@ECHO off
TITLE DiscordBot Build Script
ECHO Build Script v0.0.1 [by Nightloewe]
::call input function
GOTO INPUT

::ask what the user want to do
:INPUT
cls
echo Select one of the following options: 
echo [1] Compile DiscordBot
echo [2] Run DiscordBot 
echo [3] Compile and run DiscordBot
echo [4] Run and restart DiscordBot
echo.

SETLOCAL
SET /P OPTION=Please select the number in the brackets: 

if %OPTION%==1 CALL :COMPILE
if %OPTION%==2 CALL :RUN
if %OPTION%==3 ( 
    CALL :COMPILE
    CALL :RUN 
    )
if %OPTION%==4 GOTO AUTOSTART
ENDLOCAL

GOTO INPUT

EXIT /B 0

:COMPILE
echo Compiling...
cmd /C mvn clean package

EXIT /B 0

:RUN
echo Starting application...
cd bin/Debug
java -jar DiscordBot-0.0.1-SNAPSHOT.jar
PAUSE
cd ../

EXIT /B 0

:AUTOSTART
echo Autorunning application...
cd bin/Debug
java -jar DiscordBot-0.0.1-SNAPSHOT.jar
GOTO AUTOSTART
EXIT /B 0