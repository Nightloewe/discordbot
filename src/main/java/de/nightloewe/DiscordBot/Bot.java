package de.nightloewe.DiscordBot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.security.auth.login.LoginException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.slf4j.LoggerFactory;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.HashMultimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeSearchProvider;

import de.nightloewe.DiscordBot.command.AnnotatedCommand;
import de.nightloewe.DiscordBot.command.CommandMap;
import de.nightloewe.DiscordBot.command.ICommand;
import de.nightloewe.DiscordBot.command.ICommandMap;
import de.nightloewe.DiscordBot.command.IModule;
import de.nightloewe.DiscordBot.command.ModuleAlias;
import de.nightloewe.DiscordBot.command.annotations.Command;
import de.nightloewe.DiscordBot.config.Config;
import de.nightloewe.DiscordBot.config.DatabaseManager;
import de.nightloewe.DiscordBot.config.Language;
import de.nightloewe.DiscordBot.entities.BotPermission;
import de.nightloewe.DiscordBot.entities.ConnectedGuild;
import de.nightloewe.DiscordBot.entities.MusicPlayer;
import de.nightloewe.DiscordBot.entities.Playlist;
import de.nightloewe.DiscordBot.entities.Setting;
import de.nightloewe.DiscordBot.entities.TwitchSubscription;
import de.nightloewe.DiscordBot.events.BotEventManager;
import de.nightloewe.DiscordBot.events.ConnectionListener;
import de.nightloewe.DiscordBot.events.GuildListener;
import de.nightloewe.DiscordBot.events.MessageListener;
import de.nightloewe.DiscordBot.events.UserListener;
import de.nightloewe.DiscordBot.modules.AdministrationModule;
import de.nightloewe.DiscordBot.modules.HOI4Module;
import de.nightloewe.DiscordBot.modules.HelpModule;
import de.nightloewe.DiscordBot.modules.LevelModule;
import de.nightloewe.DiscordBot.modules.MusicModule;
import de.nightloewe.DiscordBot.modules.PermissionsModule;
import de.nightloewe.DiscordBot.modules.TwitchModule;
import de.nightloewe.DiscordBot.util.PermissionUtil;
import de.nightloewe.DiscordBot.web.WebService;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.requests.SessionReconnectQueue;

public class Bot {

	private static Bot instance;
	
	private Logger logger;
	private DatabaseManager db;
	
	private HashMap<Long, HashMap<String, Setting>> botSettings = new HashMap<Long, HashMap<String, Setting>>();

	private Config config;

	private ICommandMap commandMap;
	private HashMap<String, IModule> modules = new HashMap<String, IModule>();
	private ArrayList<JDA> instances = new ArrayList<JDA>();
	
	private HashMap<Guild, ConnectedGuild> connectedGuilds = new HashMap<Guild, ConnectedGuild>();
	private LinkedList<BotPermission> globalPermissions = new LinkedList<BotPermission>();
	
	private ArrayList<String> eventTypes = new ArrayList<String>();
	
	private Cache<Long, Message> messageCache = CacheBuilder.newBuilder().maximumSize(10000).expireAfterWrite(12, TimeUnit.HOURS).build();

	private de.nightloewe.DiscordBot.LogManager logManager;

	private HashMap<String, Language> languages = new HashMap<String, Language>();
	
	private WebService web;
	
	public Bot() {
		Bot.instance = this;
		this.logger = LogManager.getLogger();
		this.logger.info("Logging initialized - Starting DiscordBot by Nightloewe & Nightishaman (TeamNight) (Debug) (0.0.1-SNAPSHOT)");
		this.logger.info("Preparing debug mode for YoutubeSearchProvider");
		
		//TODO: @Nightishaman Set debug mode for Logger to fix youtube search bug
//		this.logger.info("Logger instance is " + LoggerFactory.getLogger(YoutubeSearchProvider.class).getClass().getName());
//		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
//		Configuration config = ctx.getConfiguration();
//		LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
//		loggerConfig.setLevel(Level.DEBUG);
//		ctx.updateLoggers();
		
		
		this.db = new DatabaseManager();
	}
	
	public void init() {
		Long startTime = System.currentTimeMillis();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		File file = new File("config.json");
		
		if(!file.exists()) {
			try {
				file.createNewFile();
				
				FileWriter fw = new FileWriter(file);
				Config config = new Config();
				config.setMaxShards(10);
				config.setToken("token");
				config.addOwnerId(0000L);
				config.setWebServerEnabled(false);
				config.setWebMinThreads(2);
				config.setWebMaxThreads(20);
				config.setWebPort(8080);
				
				fw.write(gson.toJson(config));
				fw.close();
				
				System.out.println("Config config.json was created and you should edit it now");
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		
		FileReader reader = null;
		try {
			reader = new FileReader(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		System.setProperty("http.agent", "DiscordBot");
		
		this.eventTypes.add("all");
		this.eventTypes.add("CommandExecution");
		this.eventTypes.add("UserJoin");
		this.eventTypes.add("UserLeave");
		this.eventTypes.add("UserChange");
		this.eventTypes.add("UserPresence");
		this.eventTypes.add("UserAddRole");
		this.eventTypes.add("UserRemoveRole");
		this.eventTypes.add("UserKicked");
		this.eventTypes.add("UserBanned");
		this.eventTypes.add("UserMuted");
		this.eventTypes.add("MessageDeleted");
		this.eventTypes.add("MessageEdited");
		this.eventTypes.add("MusicPlayerNotification");
		this.eventTypes.add("MusicPlayerException");
		
		this.getLogger().info("Reading config...");
		
		this.config = gson.fromJson(reader, Config.class);
		
		this.getLogger().info("Reading database...");
		
		this.botSettings = this.db.getAllSettings();
		this.commandMap = new CommandMap();
		this.logManager = new de.nightloewe.DiscordBot.LogManager(this);
		
		this.getLogger().info("Reading language files...");
		
		File langDir = new File("./lang/");
		File en_ENFile = new File("./lang/en_EN.json");
		
		if(!langDir.exists()) {
			langDir.mkdir();
		}
		
		if(!en_ENFile.exists()) {
			try {
				FileWriter fw = new FileWriter(en_ENFile);
				fw.write(gson.toJson(new Language()));
				fw.flush();
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for(File langFile : langDir.listFiles()) {
			String languageCode = langFile.getName().split("\\.")[0];
			
			this.getLogger().info("Reading \"" + langFile.getName() + "\"");
			
			FileReader langReader;
			try {
				langReader = new FileReader(langFile);
				
				Language lang = gson.fromJson(langReader, Language.class);
				
				this.languages.put(languageCode, lang);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.registerModule(new PermissionsModule(this));
		this.registerModule(new AdministrationModule(this));
		this.registerModule(new HelpModule(this));
		this.registerModule(new TwitchModule(this));
		this.registerModule(new LevelModule(this));
		this.registerModule(new MusicModule(this));
		this.registerModule(new HOI4Module(this));
		
		HashMap<String, String> moduleAliases = this.db.getAliases();
		
		//Registering module aliases if anyone needs an alias of an module
		for(String command : moduleAliases.keySet()) {
			this.addModuleAlias(command, moduleAliases.get(command));
		}
		
		this.web = new WebService(this);
		this.web.start();
		
		JDABuilder shardBuilder = new JDABuilder(AccountType.BOT).setToken(this.config.getToken()).setStatus(OnlineStatus.IDLE);
		
		BotEventManager eventManager = new BotEventManager();
		
		shardBuilder.setEventManager(eventManager);
		
		shardBuilder.addEventListener(new MessageListener(), new ConnectionListener(), new UserListener(), new GuildListener());
		
		for(int i = 0; i < this.config.getMaxShards(); i++) {
			try {
				this.instances.add(shardBuilder.useSharding(i, this.config.getMaxShards()).buildBlocking());
			} catch (LoginException | IllegalArgumentException | InterruptedException e) {
				e.printStackTrace();
			}
			
			this.getLogger().info("Started Shard " + i);
			
			try {
				Thread.sleep(5000L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		this.globalPermissions = this.getDatabase().getPermissions();
		
		for(BotPermission botPermission : this.globalPermissions) {
			Bot.getInstance().getLogger().info("BotPerm: " + botPermission.getGuildId() + ":" + botPermission.getAction());
		}
		
		for(JDA jda : this.instances) {
			for(Guild guild : jda.getGuilds()) {
				this.setupGuild(guild);
			}
			jda.getPresence().setStatus(OnlineStatus.ONLINE);
		}
		
		TwitchModule twitch = (TwitchModule) this.getModules().get("Twitch Integration");
		try {
			twitch.refreshAllSubscriptions();
		} catch(NullPointerException ex) {
			Bot.getInstance().getLogger().error("Could not init Twitch");
		}
		
		this.getLogger().info("Loadup finished! Took " + String.valueOf(System.currentTimeMillis() - startTime) + "ms to start up.");
		eventManager.setListening(true);
	}
	
	public static Bot getInstance() {
		return instance;
	}
	
	public Logger getLogger() {
		return logger;
	}
	
	public Config getConfig() {
		return config;
	}
	
	public ICommandMap getCommandMap() {
		return commandMap;
	}
	
	public HashMap<String, IModule> getModules() {
		return modules;
	}
	
	public DatabaseManager getDatabase() {
		return db;
	}
	
	public ArrayList<String> getEventTypes() {
		return eventTypes;
	}
	
	public de.nightloewe.DiscordBot.LogManager getLogManager() {
		return logManager;
	}
	
	public Cache<Long, Message> getMessageCache() {
		return messageCache;
	}
	
	public HashMap<Guild, ConnectedGuild> getConnectedGuilds() {
		return connectedGuilds;
	}
	
	public LinkedList<BotPermission> getGlobalPermissions() {
		return globalPermissions;
	}
	
	public HashMap<String, Language> getLanguages() {
		return languages;
	}
	
	public Language getLanguage(String languageCode) {
		return this.languages.get(languageCode);
	}
	
	public Language getDefaultLanguage() {
		return this.languages.get(this.getSetting(-1, "DefaultLanguage").getValue());
	}
	
	public void setupGuild(Guild guild) {
		ConnectedGuild cGuild = new ConnectedGuild(this, guild);
		
		//Language integration
		Setting langSetting = this.getSetting(guild.getIdLong(), "Language");
		
		if(langSetting == null)
			langSetting = this.getSetting(-1, "DefaultLanguage");
		
		this.getLogger().info("[Guild: " + guild.getId() + "] Set language to " + langSetting.getValue());
		
		cGuild.setLang(this.getLanguage(langSetting.getValue()));
		
		//First set mute roles for guilds
		Setting setting = this.getSetting(guild.getIdLong(), "MuteRoleId");
		Role muteRole = null;
				
		if(setting != null) {
			Role role = guild.getRoleById(setting.getValueInt());
					
			if(guild.getSelfMember().canInteract(role)) {
				muteRole = role;
			} else {
				this.getLogger().error("!!! Bot can't interact with Mute Role of " + guild.getName() + " [ID: " + guild.getId() + "] because bots roles are lower!");
			}
		} else {
			for(Role role : guild.getRolesByName("Muted", true)) {
				if(guild.getSelfMember().canInteract(role)) {
					muteRole = role;
				}
			}
		}
				
		if(muteRole == null) {
			this.getLogger().error("!!! Mute Role for server " + guild.getName() + " [ID: " + guild.getId() + "] couldn't found, please set a mute role!");
		}
		
		if(muteRole != null)
			cGuild.setMuteRole(muteRole);
		
		//Now set the log channels for this guild
		HashMap<String, Long> channelIds = this.db.getLogChannels(guild.getIdLong());
		
		for(String key : channelIds.keySet()) {
			TextChannel channel = guild.getTextChannelById(channelIds.get(key));
			
			if(key.equalsIgnoreCase("ignore")) {
				cGuild.getIgnoredChannels().add(channel);
				continue;
			}
			
			cGuild.getLogChannels().put(key, channel);
		}
		
		//Set the role emotes for this guild into local hashmap
		HashMap<Long, String> emoteNameByRoleId = this.getDatabase().getRoleEmotes(guild.getIdLong());
		HashMap<Role, Long> roleEmotes = new HashMap<Role, Long>();
		
		for(Long roleId : emoteNameByRoleId.keySet()) {
			String emoteName = emoteNameByRoleId.get(roleId);
			
			Role role = guild.getRoleById(roleId);
			if(role == null)
				continue;
			
			Long emoteId = this.getDatabase().getEmote(guild.getIdLong(), emoteName);
			
			if(emoteId == -1L)
				continue;

			roleEmotes.put(role, emoteId);
		}
		
		cGuild.setRoleEmotes(roleEmotes);
		
		//Set the allowed role assign messages
		HashMultimap<Long, Long> roleAssignMessages = this.getDatabase().getRoleAssignMessages(guild.getIdLong());
		
		cGuild.setRoleAssignMessages(roleAssignMessages);
		
		//Set the allowed self assignable roles
		ArrayList<Long> selfAssignRolesIds = this.getDatabase().getSelfAssignableRoles(guild.getIdLong());
		ArrayList<Role> selfAssignRoles = new ArrayList<Role>();
		
		for(Long roleId : selfAssignRolesIds) {
			Role role = guild.getRoleById(roleId);
			
			if(role == null) {
				this.getDatabase().removeSelfAssignableRole(guild.getIdLong(), roleId);
				continue;
			}
			
			selfAssignRoles.add(role);
		}
		
		cGuild.setSelfAssignableRoles(selfAssignRoles);
		
		MusicModule module = (MusicModule) this.getModules().get("Music");
		MusicPlayer player = new MusicPlayer(cGuild, module.getPlayerManager());
		
		cGuild.setPlayer(player);
		cGuild.getGuild().getAudioManager().setSendingHandler(player);
		
		for(Playlist playlist : this.getDatabase().getPlaylists(guild.getIdLong())) {
			cGuild.getPlaylists().put(playlist.getName(), playlist);
		}
		
		Setting defaultPlaylist = this.getSetting(cGuild.getGuild().getIdLong(), "DefaultPlaylist");
		
		Setting defaultChannel = this.getSetting(cGuild.getGuild().getIdLong(), "DefaultAudioChannel");
		
		if(defaultChannel != null) {
			VoiceChannel channel = cGuild.getGuild().getVoiceChannelById(defaultChannel.getValueInt());
			
			if(channel != null) {
				guild.getAudioManager().openAudioConnection(channel);
				
				if(defaultPlaylist != null) {
					String playlistName = defaultPlaylist.getValue();
					
					cGuild.getPlayer().setDefaultPlaylist(cGuild.getPlaylists().get(playlistName));
					cGuild.getPlayer().loadPlaylist(cGuild.getPlaylists().get(playlistName));
					cGuild.getPlayer().nextTrack();
				}
			} else {
				this.getLogger().info("Guild " + guild.getName() + " [ID: " + guild.getIdLong() + "] has no default audio channel.");
			}
		}
		
		Setting audioControlsChannel = this.getSetting(guild.getIdLong(), "AudioControlsChannel");
		Setting audioControlsMessage = this.getSetting(guild.getIdLong(), "AudioControlsMessage");
		
		if(audioControlsChannel != null && audioControlsMessage != null) {
			guild.getTextChannelById(audioControlsChannel.getValueInt()).getMessageById(audioControlsMessage.getValueInt()).queue(msg -> {
				cGuild.setAudioControlsMessage(msg);
			});
		}
		
		//Load Twitch Subscriptions
		ArrayList<TwitchSubscription> subscriptions = this.db.getTwitchSubscriptions(guild.getIdLong());
		cGuild.setTwitchSubscriptions(subscriptions);
		
		//Load permissions
		cGuild.setPermissions(this.getDatabase().getPermissions(guild.getIdLong()));
		
		if(this.getSetting(cGuild.getGuild().getIdLong(), "HOI4") == null) {
			this.setSetting(cGuild.getGuild().getIdLong(), "HOI4", 0);
		}
		
		this.connectedGuilds.put(guild, cGuild);
	}
	
	/**
	 * 
	 * @param guildId
	 * @param settingKey
	 * @return @Nullable setting from botSettings hashmap or if null from database
	 * WARNING! CAN BE NULL
	 */
	public Setting getSetting(long guildId, String settingKey) {
		HashMap<String, Setting> guildSettings = this.botSettings.get(guildId);
		
		Setting setting = null;
		if(guildSettings != null)
			setting = guildSettings.get(settingKey);
		
		if(setting == null)
			setting = this.db.getSetting(guildId, settingKey);
		
		return setting;
	}
	
	/**
	 * Updates settings - also in database!
	 * @param guildId
	 * @param settingKey
	 * @param value
	 */
	public void setSetting(long guildId, String settingKey, String value) {
		this.db.setSetting(guildId, new Setting(0, guildId, settingKey, value));
		
		Setting setting = this.db.getSetting(guildId, settingKey);
		this.botSettings.get(guildId).put(settingKey, setting);
	}
	
	/**
	 * Updates settings - also in database!
	 * @param guildId
	 * @param settingKey
	 * @param value
	 */
	public void setSetting(long guildId, String settingKey, long value) {
		this.db.setSetting(guildId, new Setting(0, guildId, settingKey, value));
		
		Setting setting = this.db.getSetting(guildId, settingKey);
		this.botSettings.get(guildId).put(settingKey, setting);
	}
	
	public void createSettingsMap(long guildId) {
		this.botSettings.put(guildId, new HashMap<String, Setting>());
	}
	
	public void setPermission(BotPermission permission) {
		for(BotPermission perm : this.globalPermissions) {
			if(PermissionUtil.isEqual(permission, perm)) {
				this.globalPermissions.remove(perm);
			}
		}
		this.globalPermissions.add(permission);
	}
	
	public void removePermission(BotPermission permission) {
		BotPermission toRemove = null;
		for(BotPermission perm : this.globalPermissions) {
			if(permission.getId() == perm.getId()) {
				toRemove = perm;
			}
		}
		
		if(toRemove != null)
			this.globalPermissions.remove(toRemove);
	}
	
	/**
	 * Registers command in given class with the @Command annotation
	 * <B>WARNING!</B> If you use modules and use super(bot) in constructor, then all methods with @Command annotation will automatically registered
	 * @param object
	 */
	public void registerCommands(Object obj) {
		for(Method method : obj.getClass().getDeclaredMethods()) {
			if(method.getAnnotation(Command.class) != null) {
				Command commandAnnotation = method.getAnnotation(Command.class);
				
				AnnotatedCommand command = new AnnotatedCommand(commandAnnotation.name(), commandAnnotation.usage(), commandAnnotation.description(), commandAnnotation.aliases(), method, obj);
				
				this.getCommandMap().register(command);
			}
		}
	}
	
	/**
	 * Registers command in CommandMap
	 * @param command
	 */
	public void registerCommand(ICommand command) {
		this.getCommandMap().register(command);
	}
	
	/**
	 * Registers module in Bot HashMap
	 * @param module
	 */
	public void registerModule(IModule module) {
		this.modules.put(module.getName(), module);
	}
	
	public ConnectedGuild getConnectedGuild(Guild guild) {
		return this.connectedGuilds.get(guild);
	}
	
	public JDA getJDA() {
		return this.instances.get(0);
	}
	
	public boolean isMuted(Member member) {
		Role muteRole = this.connectedGuilds.get(member.getGuild()).getMuteRole();
		
		if(muteRole != null && member.getRoles().contains(muteRole)) {
			return true;
		}
		
		return false;
	}
	
	public void addModuleAlias(String command, String module) {
		ICommand commandObj = this.commandMap.getCommand(command);
		
		if(commandObj == null)
			return;
		
		IModule moduleIntf = this.modules.get(module);
		if(moduleIntf != null && !(moduleIntf instanceof ModuleAlias))
			return;
		
		ModuleAlias moduleObj;
		if(moduleIntf == null) {
			moduleObj = new ModuleAlias(module);
			this.registerModule(moduleObj);
		} else {
			moduleObj = (ModuleAlias) moduleIntf;
		}
		
		moduleObj.addAlias(commandObj);
	}
	
	public void removeModuleAlias(String command, String module) {
		IModule moduleIntf = this.modules.get(module);
		
		if(moduleIntf == null || !(moduleIntf instanceof ModuleAlias))
			return;
		
		ModuleAlias moduleObj = (ModuleAlias) moduleIntf;
		
		ICommand commandObj = this.commandMap.getCommand(command);
		if(commandObj == null)
			return;
		
		moduleObj.removeAlias(commandObj);
	}

}
