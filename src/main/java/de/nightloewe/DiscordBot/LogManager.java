package de.nightloewe.DiscordBot;

import java.lang.reflect.Field;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.function.Consumer;

import de.nightloewe.DiscordBot.entities.Setting;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.MessageEmbed.Footer;
import net.dv8tion.jda.core.entities.TextChannel;

public class LogManager {

	private Bot bot;
	
	public LogManager(Bot bot) {
		this.bot = bot;
	}
	
	public void logToChannel(Guild guild, String eventType, Message message) {
		this.logToChannel(guild, eventType, message, null);
	}
	
	public void logToChannel(Guild guild, String eventType, Message message, Consumer<Message> success) {
		Setting logSetting = this.bot.getSetting(guild.getIdLong(), "Logging");
		
		if(logSetting == null || !logSetting.isTrue())
			return;
		
		TextChannel logChannel = this.bot.getConnectedGuild(guild).getLogChannels().get(eventType);
		
		if(logChannel == null)
			logChannel = this.bot.getConnectedGuild(guild).getLogChannels().get("all");
		
		if(logChannel == null)
			return;
		
		try {
			Instant time = Instant.now();
			ZoneOffset zoneOffset = ZoneOffset.from(time);
			Field f = message.getClass().getDeclaredField("timestamp");
			f.setAccessible(true);
			f.set(message, OffsetDateTime.ofInstant(time, zoneOffset));
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		logChannel.sendMessage(message).queue(success);
	}
	
	public void logToChannel(Guild guild, String eventType, MessageEmbed message) {
		this.logToChannel(guild, eventType, message, null);
	}
	
	public void logToChannel(Guild guild, String eventType, MessageEmbed message, Consumer<Message> success) {
		Setting logSetting = this.bot.getSetting(guild.getIdLong(), "Logging");
		
		if(logSetting == null || !logSetting.isTrue())
			return;
		
		TextChannel logChannel = this.bot.getConnectedGuild(guild).getLogChannels().get(eventType);
		
		if(logChannel == null)
			logChannel = this.bot.getConnectedGuild(guild).getLogChannels().get("all");
		
		if(logChannel == null)
			return;
		
		
		try {
			Instant time = Instant.now();
			ZoneOffset zoneOffset = null;
			
			try {
				zoneOffset = ZoneOffset.from(time);
			} catch(DateTimeException ignore) {
				zoneOffset = ZoneOffset.UTC;
			}
			
			Field f = message.getClass().getDeclaredField("timestamp");
			f.setAccessible(true);
			f.set(message, OffsetDateTime.ofInstant(time, zoneOffset));
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		logChannel.sendMessage(message).queue(success);
	}
	
	public void logToDefaultChannel(Guild guild, Message message) {
		Setting logSetting = this.bot.getSetting(guild.getIdLong(), "Logging");
		
		if(logSetting == null || !logSetting.isTrue())
			return;
		
		guild.getDefaultChannel().sendMessage(message).queue();
	}
	
}
