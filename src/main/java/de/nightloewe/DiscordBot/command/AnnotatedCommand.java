package de.nightloewe.DiscordBot.command;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.command.annotations.RequireBotPermission;
import de.nightloewe.DiscordBot.command.annotations.RequireBotUserPermission;
import de.nightloewe.DiscordBot.command.annotations.RequireGuild;
import de.nightloewe.DiscordBot.command.annotations.RequireOwner;
import de.nightloewe.DiscordBot.command.annotations.RequireUserPermission;
import de.nightloewe.DiscordBot.entities.BotPermission;
import de.nightloewe.DiscordBot.entities.BotPermission.PermissionValue;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import de.nightloewe.DiscordBot.util.PermissionUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.TextChannel;

public class AnnotatedCommand implements ICommand {

	private String name;
	private String[] usage;
	private String description;
	private String module;
	
	private Method method;
	
	private Object invokeObject;
	private String[] aliases;
	
	public AnnotatedCommand(String name, String[] usage, String description, Method method, Object invokeObject) {
		this.name = name;
		this.usage = usage;
		this.description = description;
		this.method = method;
		this.invokeObject = invokeObject;
		
		if(invokeObject instanceof IModule) {
			IModule module = (IModule) invokeObject;
			this.module = module.getName();
		}
	}
	
	public AnnotatedCommand(String name, String[] usage, String description, String[] aliases, Method method, Object invokeObject) {
		this(name, usage, description, method, invokeObject);
		this.aliases = aliases;
	}
	
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String[] getUsage() {
		return this.usage;
	}

	@Override
	public String getDescription() {
		return this.description;
	}
	
	@Override
	public String[] getAliases() {
		return this.aliases;
	}
	
	public String getModule() {
		return module;
	}

	@Override
	public boolean execute(CommandInfo commandInfo) {
		BotPermission.PermissionValue botPermission = PermissionUtil.canExecute(commandInfo);
		
		Bot.getInstance().getLogger().warn(botPermission.name() + ":" + botPermission.getReason());
		
		/**
		 * Requires Bot Owner
		 */
		if(method.getAnnotation(RequireOwner.class) != null) {
			if(!Bot.getInstance().getConfig().getOwnerIds().contains(commandInfo.getAuthor().getIdLong())) {
				commandInfo.getChannel().sendMessage(
						new EmbedBuilder().setDescription(commandInfo.getLang().NEED_OWNER.format(commandInfo.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
		}
		
		/**
		 * Requires permission for Bot member
		 */
		if(method.getAnnotation(RequireBotUserPermission.class) != null && botPermission == PermissionValue.UNSET) {
			RequireBotUserPermission annotation = method.getAnnotation(RequireBotUserPermission.class);
			
			if(commandInfo.getGuild().isPresent()) {
				for(Permission permission : annotation.guildPermission()) {
					if(!commandInfo.getGuild().get().getSelfMember().hasPermission(permission)) {
						commandInfo.getChannel().sendMessage(
								new EmbedBuilder().setDescription(commandInfo.getLang().BOT_NEED_SERVER_PERMISSION.format(commandInfo.getAuthor().getAsMention(), permission.getName())).setColor(Color.RED).build()
								).queue();
						return true;
					}
				}
				
				for(Permission permission : annotation.channelPermission()) {
					TextChannel textChannel = (TextChannel) commandInfo.getChannel();
					if(!commandInfo.getGuild().get().getSelfMember().hasPermission(textChannel, permission)) {
						commandInfo.getChannel().sendMessage(
								new EmbedBuilder().setDescription(commandInfo.getLang().BOT_NEED_CHANNEL_PERMISSION.format(commandInfo.getAuthor().getAsMention(), permission.getName(), textChannel.getAsMention())).setColor(Color.RED).build()
								).queue();
						return true;
					}
				}
			}
		}
		
		if(method.getAnnotation(RequireUserPermission.class) != null && botPermission == PermissionValue.UNSET) {
			RequireUserPermission annotation = method.getAnnotation(RequireUserPermission.class);
			
			if(commandInfo.getGuild().isPresent()) {
				for(Permission permission : annotation.guildPermission()) {
					if(!commandInfo.getGuildMember().get().hasPermission(permission)) {
						commandInfo.getChannel().sendMessage(
								new EmbedBuilder().setDescription(commandInfo.getLang().NEED_SERVER_PERMISSION.format(commandInfo.getAuthor().getAsMention(), permission.getName())).setColor(Color.RED).build()
								).queue();
						return true;
					}
				}
				
				for(Permission permission : annotation.channelPermission()) {
					TextChannel textChannel = (TextChannel) commandInfo.getChannel();
					if(!commandInfo.getGuildMember().get().hasPermission(textChannel, permission)) {
						commandInfo.getChannel().sendMessage(
								new EmbedBuilder().setDescription(commandInfo.getLang().NEED_CHANNEL_PERMISSION.format(commandInfo.getAuthor().getAsMention(), permission.getName(), textChannel.getAsMention())).setColor(Color.RED).build()
								).queue();
						return true;
					}
				}
			}
		}
		
		if(method.getAnnotation(RequireGuild.class) != null) {
			if(!commandInfo.getGuild().isPresent()) {
				commandInfo.getChannel().sendMessage(
						new EmbedBuilder().setDescription(commandInfo.getLang().REQUIRE_GUILD.format(commandInfo.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
		}
		
		if(botPermission == PermissionValue.DENY && method.getAnnotation(RequireOwner.class) == null) {
			new BotEmbedBuilder().setDescription(commandInfo.getAuthor().getAsMention() + " You can't execute this command because of presets by command permissions. `" + botPermission.getReason() + "`").withErrorColor().sendMessage(commandInfo.getChannel());
			return true;
		}
		
		if(method.getAnnotation(RequireBotPermission.class) != null && botPermission != PermissionValue.ALLOW) {
			new BotEmbedBuilder().setDescription(commandInfo.getAuthor().getAsMention() + " You can't execute this command because you need a permission set to use this command.").withErrorColor().sendMessage(commandInfo.getChannel());
			return true;
		}
		
		try {
			boolean returnValue = (boolean) this.method.invoke(this.invokeObject, commandInfo);
			
			return returnValue;
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
}
