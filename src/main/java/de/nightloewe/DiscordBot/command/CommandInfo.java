package de.nightloewe.DiscordBot.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.config.Language;
import de.nightloewe.DiscordBot.entities.Setting;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public class CommandInfo {

	private Optional<Guild> guild;
	private Optional<Member> guildMember;
	private Message message;
	private String[] arguments;
	private String command;
	private Language lang;
	private String botPrefix;
	
	public CommandInfo(Message message) {
		this.message = message;
		this.guild = Optional.ofNullable(message.getGuild());
		this.guildMember = Optional.ofNullable(message.getMember());
		
		Setting defaultLang = Bot.getInstance().getSetting(-1, "DefaultLanguage");
		Setting defaultBotPrefix = Bot.getInstance().getSetting(-1, "CommandPrefix");
		
		if(this.guild.isPresent()) {
			Setting lang = Bot.getInstance().getSetting(this.guild.get().getIdLong(), "Language");
			Setting botPrefix = Bot.getInstance().getSetting(this.guild.get().getIdLong(), "CommandPrefix");
			this.botPrefix = (botPrefix != null ? botPrefix.getValue() : defaultBotPrefix.getValue());
			
			this.lang = Bot.getInstance().getLanguage((lang != null ? lang.getValue() : defaultLang.getValue()));
		} else {
			this.lang = Bot.getInstance().getLanguage(defaultLang.getValue());
		}
		
		String[] params = message.getContentRaw().split(" ");
		
		ArrayList<String> paramsList = new ArrayList<String>(Arrays.asList(params));
		
		ArrayList<String> args = new ArrayList<String>();
		
		this.command = paramsList.get(0).substring(this.botPrefix.length());
		paramsList.remove(0);
		
		boolean argumentParsing = false;
		
		for(int i = 0; i < paramsList.size(); i++) {
			String param = paramsList.get(i);
			
			if(param.startsWith("\"")) {
				argumentParsing = true;
				
				ParamsLoop:
				for(int j = i + 1; j < paramsList.size(); j++) {
					String secondParam = paramsList.get(j);
					
					if(secondParam.endsWith("\"")) {
						param += " " + secondParam.substring(0, secondParam.length() - 1);
						param = param.substring(1);
						
						argumentParsing = false;
						i = j; //to set counter to another value
						break ParamsLoop;
					} else {
						param += " " + secondParam;
					}
				}
				
			}
			
			if(argumentParsing) {
				throw new IllegalArgumentException("Command argument is starting with \" but isn't ending with \"");
			}
			
			args.add(param);
		}
		
		this.arguments = args.toArray(new String[args.size()]);
	}
	
	public Message getMessage() {
		return message;
	}
	
	public Optional<Guild> getGuild() {
		return guild;
	}
	
	public Optional<Member> getGuildMember() {
		return guildMember;
	}
	
	public User getAuthor() {
		return this.message.getAuthor();
	}
	
	public String[] getArguments() {
		return arguments;
	}
	
	public String getCommand() {
		return command;
	}

	public MessageChannel getChannel() {
		return this.message.getChannel();
	}
	
	public Language getLang() {
		return lang;
	}
	
	public String getBotPrefix() {
		return botPrefix;
	}
	
	public void setGuild(Optional<Guild> guild) {
		this.guild = guild;
	}
	
	public void setGuildMember(Optional<Member> guildMember) {
		this.guildMember = guildMember;
	}
	
	public void setArguments(String[] arguments) {
		this.arguments = arguments;
	}
	
	public void setCommand(String command) {
		this.command = command;
	}
	
	public void setBotPrefix(String botPrefix) {
		this.botPrefix = botPrefix;
	}
	
}
