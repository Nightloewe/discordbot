package de.nightloewe.DiscordBot.command;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.entities.EventType;
import de.nightloewe.DiscordBot.entities.Setting;
import de.nightloewe.DiscordBot.modules.HelpModule;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import de.nightloewe.DiscordBot.util.PermissionUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageEmbed;

public class CommandMap implements ICommandMap {

	private LinkedHashMap<String, ICommand> commands = new LinkedHashMap<String, ICommand>();
	
	@Override
	public void registerAll(List<ICommand> commands) {
		for(ICommand command : commands) {
			this.commands.put(command.getName(), command);
			
			for(String alias : command.getAliases()) {
				this.commands.put(alias, command);
			}
		}
	}

	@Override
	public void register(ICommand command) {
		this.commands.put(command.getName(), command);
		
		for(String alias : command.getAliases()) {
			this.commands.put(alias, command);
		}
	}

	@Override
	public ICommand getCommand(String name) {
		return this.commands.get(name);
	}
	
	public HashMap<String, ICommand> getCommands() {
		return commands;
	}

	@Override
	public boolean dispatchCommand(CommandInfo info) {
		ICommand command = null;
		
		for(String name : this.commands.keySet()) {
			if(name.equalsIgnoreCase(info.getCommand())) {
				command = this.commands.get(name);
				
				if(PermissionUtil.isBlacklisted(info.getAuthor())) {
					new BotEmbedBuilder().setDescription(info.getAuthor().getAsMention() + " You are blacklisted by the bot. Please contact the bot owner for more information.").withErrorColor().sendMessage(info.getChannel());
					return true;
				}
				
				boolean success = command.execute(info);
				
				if(!success) {
					HelpModule helpModule = (HelpModule) Bot.getInstance().getModules().get("Help");
					MessageEmbed embed = helpModule.getCommandHelp(name, info.getGuild().get());
					info.getChannel().sendMessage(embed).queue();
				}
				
				if(info.getGuild().isPresent())
					Bot.getInstance().getLogManager().logToChannel(info.getGuild().get(), EventType.COMMAND_EXECUTION, 
							new EmbedBuilder()
							.setTitle(info.getLang().TITLE_COMMAND_EXECUTED.format())
							.setDescription(info.getAuthor().getName() + "#" + info.getAuthor().getDiscriminator())
							.addField(info.getLang().COMMAND.format(), info.getMessage().getContentRaw(), false)
							.addField("Id", info.getMessage().getId(), false)
							.build());
				Bot.getInstance().getLogger().info("Executed Command " + info.getCommand() 
					+ "\nAuthor: " + info.getAuthor().getName() + "#" + info.getAuthor().getDiscriminator()
					+ "\nArgs: " + String.join(" ", info.getArguments())
					+ (info.getGuild().isPresent() ? "\nGuild Id: " + info.getGuild().get().getId() : ""));
				return true;
			}
		}
		Bot.getInstance().getLogger().info("Command " + info .getCommand() + " is not found!"
				+ "\nAuthor: " + info.getAuthor().getName() + "#" + info.getAuthor().getDiscriminator()
				+ (info.getGuild().isPresent() ? "\nGuild Id: " + info.getGuild().get().getId() : ""));
		return false;
	}

}
