package de.nightloewe.DiscordBot.command;

import de.nightloewe.DiscordBot.Bot;

public interface IModule {

	public Bot getBot();
	
	public String getName();
	
}
