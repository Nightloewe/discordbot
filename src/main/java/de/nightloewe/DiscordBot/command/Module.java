package de.nightloewe.DiscordBot.command;

import de.nightloewe.DiscordBot.Bot;

public abstract class Module implements IModule {

	private Bot bot;

	@Override
	public Bot getBot() {
		return this.bot;
	}
	
	public Module(Bot bot) {
		this.bot = bot;
		
		this.bot.registerCommands(this);
	}
	
	public abstract String getName();

}
