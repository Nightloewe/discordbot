package de.nightloewe.DiscordBot.command;

import java.util.ArrayList;

import de.nightloewe.DiscordBot.Bot;

public class ModuleAlias implements IModule {

	private Bot bot;
	private String name = "";
	private ArrayList<ICommand> commandAliases = new ArrayList<ICommand>();
	
	@Override
	public Bot getBot() {
		return this.bot;
	}

	@Override
	public String getName() {
		return this.name;
	}
	
	public ModuleAlias(String name) {
		this.name = name;
	}
	
	public ArrayList<ICommand> getAliases() {
		return this.commandAliases;
	}
	
	public void addAlias(ICommand command) {
		this.commandAliases.add(command);
	}
	
	public void removeAlias(ICommand command) {
		this.commandAliases.remove(command);
	}

}
