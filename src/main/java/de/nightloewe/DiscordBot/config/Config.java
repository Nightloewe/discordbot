package de.nightloewe.DiscordBot.config;

import java.util.ArrayList;
import java.util.HashMap;

public class Config {

	private String token;
	private ArrayList<Long> ownerIds = new ArrayList<Long>();
	private int maxShards;
	
	private String twitchClientId;
	private String twitchClientSecret;
	
	private String mixerClientId;
	private String mixerClientSecret;
	
	//WOW FEATURES
	private HashMap<String, Long> roleEmotes = new HashMap<String, Long>();
	
	//WEB FEATURES
	private boolean webServerEnabled;
	private int webMaxThreads, webMinThreads, webPort;
	
	public String getToken() {
		return token;
	}
	
	public ArrayList<Long> getOwnerIds() {
		return ownerIds;
	}
	
	public int getMaxShards() {
		return maxShards;
	}
	
	public String getTwitchClientId() {
		return twitchClientId;
	}
	
	public String getTwitchClientSecret() {
		return twitchClientSecret;
	}
	
	public HashMap<String, Long> getRoleEmotes() {
		return roleEmotes;
	}
	
	public boolean isWebServerEnabled() {
		return webServerEnabled;
	}
	
	public int getWebMaxThreads() {
		return webMaxThreads;
	}
	
	public int getWebMinThreads() {
		return webMinThreads;
	}
	
	public int getWebPort() {
		return webPort;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public void addOwnerId(long id) {
		this.ownerIds.add(id);
	}
	
	public void removeOwnerId(long id) {
		this.ownerIds.remove(id);
	}

	public void setMaxShards(int maxShards) {
		this.maxShards = maxShards;
	}
	
	public void setTwitchClientId(String twitchClientId) {
		this.twitchClientId = twitchClientId;
	}
	
	public void setTwitchClientSecret(String twitchClientSecret) {
		this.twitchClientSecret = twitchClientSecret;
	}
	
	public String getMixerClientId() {
		return mixerClientId;
	}
	
	public String getMixerClientSecret() {
		return mixerClientSecret;
	}
	
	public void setWebServerEnabled(boolean webServerEnabled) {
		this.webServerEnabled = webServerEnabled;
	}
	
	public void setWebMaxThreads(int webMaxThreads) {
		this.webMaxThreads = webMaxThreads;
	}
	
	public void setWebMinThreads(int webMinThreads) {
		this.webMinThreads = webMinThreads;
	}
	
	public void setWebPort(int webPort) {
		this.webPort = webPort;
	}
	
}
