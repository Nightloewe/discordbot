package de.nightloewe.DiscordBot.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.sqlite.util.StringUtils;

import com.google.common.collect.HashMultimap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.entities.BotPermission;
import de.nightloewe.DiscordBot.entities.LevelData;
import de.nightloewe.DiscordBot.entities.MuteInfo;
import de.nightloewe.DiscordBot.entities.Playlist;
import de.nightloewe.DiscordBot.entities.Setting;
import de.nightloewe.DiscordBot.entities.TwitchSubscription;
import de.nightloewe.DiscordBot.entities.Warn;
import de.nightloewe.DiscordBot.entities.TwitchSubscription.SubscriptionType;
import de.nightloewe.DiscordBot.util.PermissionUtil;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;

public class DatabaseManager {

	private Connection conn;
	
	public DatabaseManager() {
		try {
			Class.forName("org.sqlite.JDBC");
			
			this.conn = DriverManager.getConnection("jdbc:sqlite:discordbot.db");
			Bot.getInstance().getLogger().info("Connection to local database discordbt.db was established!");
		} catch (ClassNotFoundException e) {
			Bot.getInstance().getLogger().error("*** JDBC Driver not found! Please reinstall this application ***");
			System.exit(1);
		} catch (SQLException e) {
			Bot.getInstance().getLogger().error("Error occured while loading local database: " + e.getLocalizedMessage());
			System.exit(1);
		}
		
		try {
			PreparedStatement ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS BotSettings(settingId INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER, settingKey TEXT NOT NULL, settingValue TEXT NOT NULL, settingValueInt INTEGER);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS Mutes(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, userId INTEGER NOT NULL, staffId INTEGER NOT NULL, expirationDate INTEGER NOT NULL, issueDate INTEGER NOT NULL, reason TEXT);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS Logging(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, channelId INTEGER NOT NULL, eventType TEXT NOT NULL);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS Emotes(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, emoteName TEXT NOT NULL, emoteId INTEGER NOT NULL);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS RoleEmotes(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, emoteName TEXT NOT NULL, roleId INTEGER NOT NULL);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS RoleAssignMessages(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, messageId INTEGER NOT NULL, roleIds TEXT NOT NULL);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS SelfAssignableRoles(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, roleId INTEGER NOT NULL);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS ModuleAliases(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, command TEXT NOT NULL, targetModule TEXT NOT NULL);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS Warns(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, userId INTEGER NOT NULL, staffId INTEGER NOT NULL, issueDate INTEGER NOT NULL, reason TEXT);");
			ps.executeUpdate();
			
//			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS Permissions(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, scope TEXT NOT NULL, affected TEXT NOT NULL, target TEXT NOT NULL, value INTEGER NOT NULL);");
//			ps.executeUpdate(); OLD IDEA
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS Level(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, userId INTEGER NOT NULL, level INTEGER, totalXP INTEGER, xp INTEGER, timestamp INTEGER);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS Playlists(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, playlistName TEXT NOT NULL, playlistObj TEXT NOT NULL);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS TwitchSubscriptions(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, name TEXT NOT NULL, userId INTEGER NOT NULL, secret TEXT NOT NULL, type TEXT NOT NULL, webhookURL TEXT NOT NULL, expiresAt TEXT NULL, mixerId TEXT NULL);");
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("CREATE TABLE IF NOT EXISTS Permissions(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, guildId INTEGER NOT NULL, channelId INTEGER, roleId INTEGER, userId INTEGER, module TEXT, command TEXT, action TEXT NOT NULL, webhookURL TEXT NOT NULL);");
			ps.executeUpdate();
			
			if(this.getSetting(-1, "AllowFromPrivateMessage") == null) {
				this.setSetting(-1, new Setting(0, -1, "AllowFromPrivateMessage", 0));
			}
			if(this.getSetting(-1, "CommandPrefix") == null) {
				this.setSetting(-1, new Setting(0, -1, "CommandPrefix", "."));
			}
			if(this.getSetting(-1, "DeleteCommandMessages") == null) {
				this.setSetting(-1, new Setting(0, -1, "DeleteCommandMessages", 0));
			}
			if(this.getSetting(-1, "DefaultLanguage") == null) {
				this.setSetting(-1, new Setting(0, -1, "DefaultLanguage", "en_EN"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Connection getConn() {
		return conn;
	}
	
	public HashMap<Long, HashMap<String, Setting>> getAllSettings() {
		HashMap<Long, HashMap<String, Setting>> settings = new HashMap<Long, HashMap<String, Setting>>();
		
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM BotSettings");
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				HashMap<String, Setting> guildSettings = settings.get(rs.getLong("guildId"));
				
				if(guildSettings == null) {
					guildSettings = new HashMap<String, Setting>();
				}
				
				Setting setting = null;
				if(rs.getString("settingValue").equalsIgnoreCase("useInt")) {
					setting = new Setting(rs.getInt("settingId"), rs.getLong("guildId"), rs.getString("settingKey"), rs.getLong("settingValueInt"));
				} else {
					setting = new Setting(rs.getInt("settingId"), rs.getLong("guildId"), rs.getString("settingKey"), rs.getString("settingValue"));
				}
				
				guildSettings.put(setting.getKey(), setting);
				settings.put(setting.getGuildId(), guildSettings);
			}
			
			return settings;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Setting> getSettings(long guildId) {
		ArrayList<Setting> settings = new ArrayList<Setting>();
		
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM BotSettings WHERE guildId = ?");
			ps.setLong(1, guildId);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Setting setting = null;
				if(rs.getString("settingValue").equalsIgnoreCase("useInt")) {
					setting = new Setting(rs.getInt("settingId"), rs.getLong("guildId"), rs.getString("settingKey"), rs.getLong("settingValueInt"));
				} else {
					setting = new Setting(rs.getInt("settingId"), rs.getLong("guildId"), rs.getString("settingKey"), rs.getString("settingValue"));
				}
				
				settings.add(setting);
			}
			return settings;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Setting getSetting(long guildId, String settingKey) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM BotSettings WHERE guildId=? AND settingKey=?");
			ps.setLong(1, guildId);
			ps.setString(2, settingKey);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				if(rs.getString("settingValue").equalsIgnoreCase("useInt")) {
					return new Setting(rs.getInt("settingId"), rs.getLong("guildId"), rs.getString("settingKey"), rs.getLong("settingValueInt"));
				} else {
					return new Setting(rs.getInt("settingId"), rs.getLong("guildId"), rs.getString("settingKey"), rs.getString("settingValue"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean deleteSetting(long guildId, String settingKey) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM BotSettings WHERE guildId=? AND settingKey=?");
			ps.setLong(1, guildId);
			ps.setString(2, settingKey);
			
			return ps.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void setSetting(long guildId, Setting setting) {
		try {
			if(this.getSetting(guildId, setting.getKey()) != null) {
				PreparedStatement ps = this.conn.prepareStatement("UPDATE BotSettings SET settingValue=?, settingValueInt=? WHERE guildId=? AND settingKey=?");
				ps.setString(1, setting.getValue());
				ps.setLong(2, setting.getValueInt());
				ps.setLong(3, guildId);
				ps.setString(4, setting.getKey());
				
				ps.executeUpdate();
				
				return;
			}
			
			PreparedStatement ps = this.conn.prepareStatement("INSERT INTO BotSettings(guildId, settingKey, settingValue, settingValueInt) VALUES (?, ?, ?, ?)");
			ps.setLong(1, guildId);
			ps.setString(2, setting.getKey());
			ps.setString(3, setting.getValue());
			ps.setLong(4, setting.getValueInt());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public MuteInfo getMute(long guildId, long userId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Mutes WHERE guildId = ? AND userId = ? LIMIT 1");
			ps.setLong(1, guildId);
			ps.setLong(2, userId);
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return new MuteInfo(guildId, userId, rs.getLong("staffId"), rs.getLong("expirationDate"), rs.getLong("issueDate"), rs.getString("reason"));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void addMute(long guildId, long userId, long staffId, long expirationDate, long issueDate, String reason) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("INSERT INTO Mutes(guildId, userId, staffId, expirationDate, issueDate, reason) VALUES (?, ?, ?, ?, ?, ?)");
			ps.setLong(1, guildId);
			ps.setLong(2, userId);
			ps.setLong(3, staffId);
			ps.setLong(4, expirationDate);
			ps.setLong(5, issueDate);
			ps.setString(6, reason);
			
			ps.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void removeMute(long guildId, long userId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM Mutes WHERE guildId = ? AND userId = ?");
			ps.setLong(1, guildId);
			ps.setLong(2, userId);
			
			ps.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public HashMap<String, Long> getLogChannels(long guildId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Logging WHERE guildId = ?");
			ps.setLong(1, guildId);
			
			ResultSet rs = ps.executeQuery();
			
			HashMap<String, Long> logChannels = new HashMap<String, Long>();
			while(rs.next()) {
				if(rs.getString("eventType").equalsIgnoreCase("ignore"))
					continue;
				
				logChannels.put(rs.getString("eventType"), rs.getLong("channelId"));
			}
			
			return logChannels;
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public long getLogChannel(long guildId, String eventType) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Logging WHERE guildId = ? AND eventType = ?");
			ps.setLong(1, guildId);
			ps.setString(2, eventType);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				return rs.getLong("channelId");
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return -1L;
	}
	
	public void setLogChannel(long guildId, long channelId, String eventType) {
		long oldId = this.getLogChannel(guildId, eventType);
		
		if(oldId != -1) {
			if(oldId == channelId) return;
			
			try {
				PreparedStatement ps = this.conn.prepareStatement("UPDATE Logging SET channelId = ? WHERE guildId = ? AND eventType = ?");
				ps.setLong(1, channelId);
				ps.setLong(2, guildId);
				ps.setString(3, eventType);
				
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			try {
				PreparedStatement ps = this.conn.prepareStatement("INSERT INTO Logging(guildId, channelId, eventType) VALUES(?, ?, ?);");
				ps.setLong(1, guildId);
				ps.setLong(2, channelId);
				ps.setString(3, eventType);
				
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void removeLogChannel(long guildId, String eventType) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM Logging WHERE guildId = ? AND eventType = ?;");
			ps.setLong(1, guildId);
			ps.setString(2, eventType);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Long> getChannelsIgnoringLog(long guildId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Logging WHERE guildId = ? AND eventType = ?");
			ps.setLong(1, guildId);
			ps.setString(2, "ignore");
			
			ResultSet rs = ps.executeQuery();
			
			ArrayList<Long> ignoringChannels = new ArrayList<Long>();
			while(rs.next()) {
				ignoringChannels.add(rs.getLong("channelId"));
			}
			
			return ignoringChannels;
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public long getEmote(long guildId, String emoteName) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Emotes WHERE guildId = ? AND emoteName = ?");
			ps.setLong(1, guildId);
			ps.setString(2, emoteName);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				return rs.getLong("emoteId");
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return -1L;
	}
	
	public void setEmote(long guildId, String emoteName, long emoteId) {
		try {
			if(this.getEmote(guildId, emoteName) != -1L) {
				PreparedStatement ps = this.conn.prepareStatement("UPDATE Emotes SET emoteId=? WHERE guildId=? AND emoteName=?");
				ps.setLong(1, emoteId);
				ps.setLong(2, guildId);
				ps.setString(3, emoteName);
				
				ps.executeUpdate();
				
				return;
			}
			
			PreparedStatement ps = this.conn.prepareStatement("INSERT INTO Emotes(guildId, emoteName, emoteId) VALUES (?, ?, ?)");
			ps.setLong(1, guildId);
			ps.setString(2, emoteName);
			ps.setLong(3, emoteId);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public HashMap<Long, String> getRoleEmotes(long guildId) {
		HashMap<Long, String> roleEmotes = new HashMap<Long, String>();
		
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM RoleEmotes WHERE guildId = ?");
			ps.setLong(1, guildId);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				roleEmotes.put(rs.getLong("roleId"), rs.getString("emoteName"));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return roleEmotes;
	}
	
	public String getRoleEmote(long guildId, long roleId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM RoleEmotes WHERE guildId = ? AND roleId = ?");
			ps.setLong(1, guildId);
			ps.setLong(2, roleId);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				return rs.getString("emoteName");
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void setRoleEmote(long guildId, long roleId, String emoteName) {
		try {
			if(this.getEmote(guildId, emoteName) != -1L) {
				if(this.getRoleEmote(guildId, roleId) != null) {
					PreparedStatement ps = this.conn.prepareStatement("UPDATE RoleEmotes SET emoteName=? WHERE guildId=? AND roleId=?");
					ps.setString(1, emoteName);
					ps.setLong(2, guildId);
					ps.setLong(3, roleId);
					
					ps.executeUpdate();
					return;
				}
				
				PreparedStatement ps = this.conn.prepareStatement("INSERT INTO RoleEmotes(guildId, emoteName, roleId) VALUES (?, ?, ?)");
				ps.setLong(1, guildId);
				ps.setString(2, emoteName);
				ps.setLong(3, roleId);
				
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void removeRoleEmote(long guildId, long roleId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM RoleEmotes WHERE guildId = ? AND roleId = ?");
			ps.setLong(1, guildId);
			ps.setLong(2, roleId);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public HashMultimap<Long, Long> getRoleAssignMessages(long guildId) {
		HashMultimap<Long, Long> messages = HashMultimap.create();
		
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM RoleAssignMessages WHERE guildId = ?");
			ps.setLong(1, guildId);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				String roleIdsString = rs.getString("roleIds");
				
				if(roleIdsString.contains(",")) {
					for(String roleIdString : roleIdsString.split(",")) {
						messages.put(rs.getLong("messageId"), Long.parseLong(roleIdString));
					}
				} else {
					messages.put(rs.getLong("messageId"), Long.parseLong(roleIdsString));
				}
			}
		} catch(NumberFormatException|SQLException e) {
			e.printStackTrace();
		}
		
		return messages;
	}
	
	public void addRoleAssignMessage(long guildId, long messageId, Long[] roleIds) {
		ArrayList<String> roleIdsList = new ArrayList<String>();
		
		for(Long roleId : roleIds) {
			roleIdsList.add(String.valueOf(roleId));
		}
		
		try {
			PreparedStatement ps = this.conn.prepareStatement("INSERT INTO RoleAssignMessages(guildId, messageId, roleIds) VALUES (?, ?, ?)");
			ps.setLong(1, guildId);
			ps.setLong(2, messageId);
			ps.setString(3, StringUtils.join(roleIdsList, ","));
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void removeRoleAssignMessage(long guildId, long messageId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM RoleAssignMessages WHERE guildId = ? AND messageId = ?");
			ps.setLong(1, guildId);
			ps.setLong(2, messageId);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Long> getSelfAssignableRoles(long guildId) {
		ArrayList<Long> selfAssignableRoles = new ArrayList<Long>();
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM SelfAssignableRoles WHERE guildId = ?");
			ps.setLong(1, guildId);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				selfAssignableRoles.add(rs.getLong("roleId"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return selfAssignableRoles;
	}
	
	public void addSelfAssignableRole(long guildId, long roleId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("INSERT INTO SelfAssignableRoles(guildId, roleId) VALUES (?, ?);");
			ps.setLong(1, guildId);
			ps.setLong(2, roleId);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void removeSelfAssignableRole(long guildId, long roleId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM SelfAssignableRoles WHERE guildId = ? AND roleId = ?");
			ps.setLong(1, guildId);
			ps.setLong(2, roleId);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public HashMap<String, String> getAliases() {
		HashMap<String, String> aliases = new HashMap<String, String>();
		
		PreparedStatement ps;
		try {
			ps = this.conn.prepareStatement("SELECT * FROM ModuleAliases");
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				aliases.put(rs.getString("command"), rs.getString("targetModule"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return aliases;
	}
	
	public void addModuleAlias(String command, String module) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("INSERT INTO ModuleAliases(command, targetModule) VALUES (?, ?);");
			ps.setString(1, command);
			ps.setString(2, module);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void removeModuleAlias(String command, String module) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM ModuleAliases WHERE command = ? AND targetModule = ?");
			ps.setString(1, command);
			ps.setString(2, module);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Warn> getWarns(Guild guild, long userId) {
		List<Warn> warns = new ArrayList<Warn>();
		
		PreparedStatement ps;
		try {
			ps = this.conn.prepareStatement("SELECT * FROM Warns WHERE guildId=? AND userId=?");
			ps.setLong(1, guild.getIdLong());
			ps.setLong(2, userId);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				int id = rs.getInt("id");
				long staffId = rs.getLong("staffId");
				long issueDate = rs.getLong("issueDate");
				String reason = rs.getString("reason");
				
				Member user = guild.getMemberById(userId);
				Member staffUser = guild.getMemberById(staffId);
				
				Warn warn = new Warn(id, guild.getIdLong(), userId, staffId, issueDate, reason, user, staffUser, guild);
				warns.add(warn);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return warns;
	}
	
	public List<Warn> getWarns(Guild guild, long userId, int offset, int limit) {
		List<Warn> warns = new ArrayList<Warn>();
		
		PreparedStatement ps;
		try {
			ps = this.conn.prepareStatement("SELECT *, (SELECT count(*) FROM Warns) AS rows FROM Warns WHERE guildId=? AND userId=? ORDER BY id LIMIT ? OFFSET ?;");
			ps.setLong(1, guild.getIdLong());
			ps.setLong(2, userId);
			ps.setInt(3, limit);
			ps.setInt(4, offset);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				int id = rs.getInt("id");
				long staffId = rs.getLong("staffId");
				long issueDate = rs.getLong("issueDate");
				String reason = rs.getString("reason");
				
				Member user = guild.getMemberById(userId);
				Member staffUser = guild.getMemberById(staffId);
				
				Warn warn = new Warn(id, guild.getIdLong(), userId, staffId, issueDate, reason, user, staffUser, guild);
				warn.setRows(rs.getInt("rows"));
				warns.add(warn);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return warns;
	}
	
	public Warn getWarn(Guild guild, int warnId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Warns WHERE guildId = ? AND id = ?");
			ps.setLong(1, guild.getIdLong());
			ps.setLong(2, warnId);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				long id = rs.getLong("id");
				long userId = rs.getLong("userId");
				long staffId = rs.getLong("staffId");
				long issueDate = rs.getLong("issueDate");
				String reason = rs.getString("reason");
				
				Member user = guild.getMemberById(userId);
				Member staffUser = guild.getMemberById(staffId);
				
				return new Warn(id, guild.getIdLong(), userId, staffId, issueDate, reason, user, staffUser, guild);
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void addWarn(long guildId, long userId, long staffId, long issueDate, String reason) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("INSERT INTO Warns(guildId, userId, staffId, issueDate, reason) VALUES (?, ?, ?, ?, ?);");
			ps.setLong(1, guildId);
			ps.setLong(2, userId);
			ps.setLong(3, staffId);
			ps.setLong(4, issueDate);
			ps.setString(5, reason);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void removeWarn(long guildId, int warnId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM Warns WHERE guildId = ? AND id = ?");
			ps.setLong(1, guildId);
			ps.setLong(2, warnId);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public LevelData getLevelData(Guild guild, long userId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Level WHERE guildId = ? AND userId = ?");
			ps.setLong(1, guild.getIdLong());
			ps.setLong(2, userId);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				int level = rs.getInt("level");
				int totalXP = rs.getInt("totalXP");
				int xp = rs.getInt("xp");
				long lastMessageId = rs.getLong("lastMessageId");
				
				Member user = guild.getMemberById(userId);
				
				return new LevelData(user, totalXP, xp, level, lastMessageId);
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void updateLevelData(Guild guild, LevelData data) {
		try {
			if(this.getLevelData(guild, data.getMember().getUser().getIdLong()) != null) {
				PreparedStatement ps = this.conn.prepareStatement("UPDATE Level SET level=? AND totalXP=? AND xp=? AND timestamp=? WHERE guildId=? AND userId=?");
				ps.setLong(1, guild.getIdLong());
				ps.setLong(2, data.getMember().getUser().getIdLong());
				ps.setInt(3, data.getLevel());
				ps.setInt(4, data.getTotalXP());
				ps.setInt(5, data.getXp());
				ps.setLong(6, data.getTimestamp());
				
				ps.executeUpdate();
				return;
			}
			
			PreparedStatement ps = this.conn.prepareStatement("INSERT INTO Level(guildId, userId, level, totalXP, xp, timestamp) VALUES (?, ?, ?, ?, ?, ?)");
			ps.setLong(1, guild.getIdLong());
			ps.setLong(2, data.getMember().getUser().getIdLong());
			ps.setInt(3, data.getLevel());
			ps.setInt(4, data.getTotalXP());
			ps.setInt(5, data.getXp());
			ps.setLong(6, data.getTimestamp());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> getPlaylistNames(long guildId) {
		ArrayList<String> playlistNames = new ArrayList<String>();
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT playlistName FROM Playlists WHERE guildId = ?");
			ps.setLong(1, guildId);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				playlistNames.add(rs.getString("playlistName"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return playlistNames;
	}
	
	public ArrayList<Playlist> getPlaylists(long guildId) {
		ArrayList<Playlist> playlists = new ArrayList<Playlist>();
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Playlists WHERE guildId = ?");
			ps.setLong(1, guildId);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String playlistName = rs.getString("playlistName");
				String json = rs.getString("playlistObj");
				
				if(json.isEmpty()) {
					this.removePlaylist(guildId, playlistName);
					return null;
				}
				
				ArrayList<String> trackURLs = new Gson().fromJson(json, new TypeToken<ArrayList<String>>(){}.getType());
				
				playlists.add(new Playlist(playlistName, trackURLs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return playlists;
	}
	
	public Playlist getPlaylist(long guildId, String name) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Playlists WHERE guildId = ? AND playlistName = ?");
			ps.setLong(1, guildId);
			ps.setString(2, name);
			
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				String playlistName = rs.getString("playlistName");
				String json = rs.getString("playlistObj");
				
				if(json.isEmpty()) {
					this.removePlaylist(guildId, playlistName);
					return null;
				}
				
				ArrayList<String> trackURLs = new Gson().fromJson(json, new TypeToken<ArrayList<String>>(){}.getType());
				
				return new Playlist(playlistName, trackURLs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void insertPlaylist(long guildId, Playlist playlist) {
		ArrayList<String> trackURLs = new ArrayList<String>();
		
		for(AudioTrack track : playlist.getTracks()) {
			trackURLs.add(track.getInfo().uri);
		}
		
		if(this.getPlaylistNames(guildId).contains(playlist.getName())) {
			try {
				PreparedStatement ps = this.conn.prepareStatement("UPDATE Playlists SET playlistObj = ? WHERE guildId = ? AND playlistName = ?;");
				ps.setString(1, new Gson().toJson(trackURLs));
				ps.setLong(2, guildId);
				ps.setString(3, playlist.getName());
				
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			try {
				PreparedStatement ps = this.conn.prepareStatement("INSERT INTO Playlists(guildId, playlistName, playlistObj) VALUES(?, ?, ?);");
				ps.setLong(1, guildId);
				ps.setString(2, playlist.getName());
				ps.setString(3, new Gson().toJson(trackURLs));
				
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void removePlaylist(long guildId, String playlistName) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM Playlists WHERE guildId = ? AND playlistName = ?;");
			ps.setLong(1, guildId);
			ps.setString(2, playlistName);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<TwitchSubscription> getTwitchSubscriptions(long guildId) {
		ArrayList<TwitchSubscription> subscriptions = new ArrayList<TwitchSubscription>();
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM TwitchSubscriptions WHERE guildId = ?");
			ps.setLong(1, guildId);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String name = rs.getString("name");
				int userId = rs.getInt("userId");
				String secret = rs.getString("secret");
				String type = rs.getString("type");
				String url = rs.getString("webhookURL");
				
				TwitchSubscription subscription = new TwitchSubscription(name, userId, secret, SubscriptionType.valueOf(type), url);
				if(subscription.getType() == SubscriptionType.MIXER && rs.getString("mixerId") != null) {
					subscription.setExpiresAt(rs.getString("expiresAt"));
					subscription.setId(rs.getString("mixerId"));
					
					java.util.Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(subscription.getExpiresAt());
					
					subscription.setInitialized(date.after(new Date()));
				}
				
				subscriptions.add(subscription);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return subscriptions;
	}
	
	public boolean existsTwitchSubscription(long guildId, String name, String type) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM TwitchSubscriptions WHERE guildId = ?");
			ps.setLong(1, guildId);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String nameStr = rs.getString("name");
				String typeStr = rs.getString("type");
				
				if(nameStr.equals(name) && typeStr.equals(type)) {
					return true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void insertTwitchSubscription(long guildId, TwitchSubscription subscription) {
		try {
			if(this.existsTwitchSubscription(guildId, subscription.getName(), subscription.getType().name())) {
				this.removeTwitchSubscriptions(guildId, subscription.getName(), subscription.getType().name());
			}
			
			if(subscription.getType() == SubscriptionType.TWITCH) {
				PreparedStatement ps = this.conn.prepareStatement("INSERT INTO TwitchSubscriptions(guildId, name, userId, secret, type, webhookURL) VALUES(?, ?, ?, ?, ?, ?);");
				ps.setLong(1, guildId);
				ps.setString(2, subscription.getName());
				ps.setInt(3, subscription.getUserId());
				ps.setString(4, subscription.getSecret());
				ps.setString(5, subscription.getType().name());
				ps.setString(6, subscription.getWebhookURL());
				
				ps.executeUpdate();
			} else {
				PreparedStatement ps = this.conn.prepareStatement("INSERT INTO TwitchSubscriptions(guildId, name, userId, secret, type, webhookURL, expiresAt, mixerId) VALUES(?, ?, ?, ?, ?, ?, ?, ?);");
				ps.setLong(1, guildId);
				ps.setString(2, subscription.getName());
				ps.setInt(3, subscription.getUserId());
				ps.setString(4, subscription.getSecret());
				ps.setString(5, subscription.getType().name());
				ps.setString(6, subscription.getWebhookURL());
				ps.setString(7, subscription.getExpiresAt());
				ps.setString(8, subscription.getId());
				
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void removeTwitchSubscriptions(long guildId, String name, String type) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM TwitchSubscriptions WHERE guildId = ? AND name = ? AND type = ?;");
			ps.setLong(1, guildId);
			ps.setString(2, name);
			ps.setString(3, type);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets global permissions, so the permissions with guildId -1.
	 */
	public LinkedList<BotPermission> getPermissions() {
		LinkedList<BotPermission> permissions = new LinkedList<BotPermission>();
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Permissions WHERE guildId = -1 OR action = \"blacklist\" OR action = \"whitelist\"");
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				BotPermission permission = null;
				String action = rs.getString("action");
				
				if(action.equalsIgnoreCase("enable") || action.equalsIgnoreCase("disable")) {
					String module = rs.getString("module");
					String command = rs.getString("command");
					
					if(module == null || module.isEmpty()) {
						permission = new BotPermission(rs.getInt("id"), command, action);
					} else {
						permission = new BotPermission(rs.getInt("id"), module, action.equalsIgnoreCase("enable") ? true : false);
					}
				} else if(action.equalsIgnoreCase("whitelist") || action.equalsIgnoreCase("blacklist")) {
					long guildId = rs.getLong("guildId");
					long userId = rs.getLong("userId");
					
					if(guildId == 0L) {
						permission = new BotPermission(rs.getInt("id"), (Long)userId, action);
					} else {
						permission = new BotPermission(rs.getInt("id"), guildId, action);
					}
				}
				
				if(permission != null)
					permissions.add(permission);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return permissions;
	}
	
	/**
	 * Gets permission for a specified guild
	 * @param guildId
	 */
	public LinkedList<BotPermission> getPermissions(long guildId) {
		LinkedList<BotPermission> permissions = new LinkedList<BotPermission>();
		
		try {
			PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM Permissions WHERE guildId = ?");
			ps.setLong(1, guildId);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				BotPermission permission = null;
				String action = rs.getString("action");
				String module = rs.getString("module");
				String command = rs.getString("command");
				
				if(action.equalsIgnoreCase("enable") || action.equalsIgnoreCase("disable")) {
					if(module != null)
						permission = new BotPermission(rs.getInt("id"), guildId, module, action.equalsIgnoreCase("enable") ? true : false);
					else
						permission = new BotPermission(rs.getInt("id"), guildId, command, action);
				} else {
					long channelId = rs.getLong("channelId");
					long roleId = rs.getLong("roleId");
					long userId = rs.getLong("userId");
					
					if(channelId != 0L) {
						if(roleId != 0L) {
							if(module != null)
								permission = new BotPermission(rs.getInt("id"), guildId, channelId, roleId, module, action.equalsIgnoreCase("allow") ? true : false);
							else
								permission = new BotPermission(rs.getInt("id"), guildId, channelId, roleId, command, action);
						} else {
							if(module != null)
								permission = new BotPermission(rs.getInt("id"), guildId, channelId, module, action.equalsIgnoreCase("allow") ? true : false);
							else
								permission = new BotPermission(rs.getInt("id"), guildId, channelId, command, action);
						}
					} else {
						if(roleId != 0L) {
							if(module != null)
								permission = new BotPermission(rs.getInt("id"), guildId, module, roleId, action.equalsIgnoreCase("allow") ? true : false);
							else
								permission = new BotPermission(rs.getInt("id"), guildId, command, roleId, action);
						} else if(userId != 0L) {
							permission = new BotPermission(rs.getInt("id"), guildId, (Long)userId, command, action);
						}
					}
				}
				
				if(permission != null)
					permissions.add(permission);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return permissions;
	}
	
	public int setPermission(BotPermission permission) {
		if(permission.getId() != 0) {
			try {
				PreparedStatement ps = this.conn.prepareStatement("UPDATE Permissions SET channelId = ?, roleId = ?, userId = ?, module = ?, command = ?, action = ? WHERE guildId = ?", Statement.RETURN_GENERATED_KEYS);
				ps.setLong(1, permission.getChannelId());
				ps.setLong(2, permission.getRoleId());
				ps.setLong(3, permission.getUserId());
				ps.setString(4, permission.getModule());
				ps.setString(5, permission.getCommand());
				ps.setString(6, permission.getAction());
				ps.setLong(7, permission.getGuildId());
				
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				rs.next();
				
				return rs.getInt(1);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			try {
				PreparedStatement ps = this.conn.prepareStatement("INSERT INTO Permissions(guildId, channelId, roleId, userId, module, command, action) VALUES (?, ?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
				ps.setLong(1, permission.getGuildId());
				ps.setLong(2, permission.getChannelId());
				ps.setLong(3, permission.getRoleId());
				ps.setLong(4, permission.getUserId());
				ps.setString(5, permission.getModule());
				ps.setString(6, permission.getCommand());
				ps.setString(7, permission.getAction());
				
				ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				rs.next();
				
				return rs.getInt(1);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}
	
	public void removePermission(BotPermission permission) {
		if(permission.getId() == 0)
			return;
		
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM Permissions WHERE id = ?");
			ps.setInt(1, permission.getId());
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void resetGuild(long guildId) {
		try {
			PreparedStatement ps = this.conn.prepareStatement("DELETE FROM BotSettings WHERE guildId = ?");
			ps.setLong(1, guildId);
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("DELETE FROM Mutes WHERE guildId = ?");
			ps.setLong(1, guildId);
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("DELETE FROM Logging WHERE guildId = ?");
			ps.setLong(1, guildId);
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("DELETE FROM Emotes WHERE guildId = ?");
			ps.setLong(1, guildId);
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("DELETE FROM RoleEmotes WHERE guildId = ?");
			ps.setLong(1, guildId);
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("DELETE FROM RoleAssignMessages WHERE guildId = ?");
			ps.setLong(1, guildId);
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("DELETE FROM SelfAssignableRoles WHERE guildId = ?");
			ps.setLong(1, guildId);
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("DELETE FROM Warns WHERE guildId = ?");
			ps.setLong(1, guildId);
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("DELETE FROM Level WHERE guildId = ?");
			ps.setLong(1, guildId);
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("DELETE FROM Playlists WHERE guildId = ?");
			ps.setLong(1, guildId);
			ps.executeUpdate();
			
			ps = this.conn.prepareStatement("DELETE FROM Permissions WHERE guildId = ? AND action != \"blacklist\" AND action != \"whitelist\";");
			ps.setLong(1, guildId);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
