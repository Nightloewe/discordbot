package de.nightloewe.DiscordBot.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.google.common.collect.HashMultimap;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.config.Language;
import de.nightloewe.DiscordBot.entities.TwitchSubscription.SubscriptionType;
import de.nightloewe.DiscordBot.util.PermissionUtil;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;

public class ConnectedGuild {

	private Bot bot;
	
	private Role muteRole;
	
	private HashMap<String, TextChannel> logChannels = new HashMap<String, TextChannel>();
	private ArrayList<TextChannel> ignoredChannels = new ArrayList<TextChannel>();
	
	private LinkedList<BotPermission> permissions = new LinkedList<BotPermission>();
	
	private List<Integer> usedRGBCodes = new ArrayList<>();
	
	private HashMap<Role, Long> roleEmotes = new HashMap<Role, Long>();
	private HashMultimap<Long, Long> roleAssignMessages = HashMultimap.create();
	
	private ArrayList<Role> selfAssignableRoles = new ArrayList<Role>();
	
	private ArrayList<TwitchSubscription> twitchSubscriptions = new ArrayList<TwitchSubscription>();
	
	private Language lang;
	
	private MusicPlayer player;
	private HashMap<String, Playlist> playlists = new HashMap<String, Playlist>();
	private Message audioControlsMessage;
	
	private Guild guild;
	
	public ConnectedGuild() {
	}
	
	public ConnectedGuild(Bot bot, Guild guild) {
		this.bot = bot;
		this.guild = guild;
	}
	
	public ConnectedGuild(Bot bot, Role muteRole, HashMap<String, TextChannel> logChannels, ArrayList<TextChannel> ignoredChannels, LinkedList<BotPermission> permissions,
			HashMap<Role, Long> roleEmotes, HashMultimap<Long, Long> roleAssignMessages, ArrayList<Role> selfAssignableRoles, Guild guild) {
		this(bot, guild);
		this.muteRole = muteRole;
		this.logChannels = logChannels;
		this.ignoredChannels = ignoredChannels;
		this.permissions = permissions;
		this.roleEmotes = roleEmotes;
		this.roleAssignMessages = roleAssignMessages;
		this.selfAssignableRoles = selfAssignableRoles;
	}
	
	public Bot getBot() {
		return bot;
	}

	public Role getMuteRole() {
		return muteRole;
	}

	public HashMap<String, TextChannel> getLogChannels() {
		return logChannels;
	}

	public ArrayList<TextChannel> getIgnoredChannels() {
		return ignoredChannels;
	}
	
	public LinkedList<BotPermission> getPermissions() {
		return permissions;
	}

	public HashMap<Role, Long> getRoleEmotes() {
		return roleEmotes;
	}
	
	/**
	 * Returns role emote for given role
	 * @param role
	 * @return Emote ID
	 */
	public Long getRoleEmote(Role role) {
		return roleEmotes.get(role);
	}
	
	/**
	 * Returns role by emote Id of role emote
	 * @param emoteId
	 * @return Role
	 */
	public Role getRoleByRoleEmoteId(long emoteId) {
		for(Entry<Role, Long> entry : this.roleEmotes.entrySet()) {
			if(((long)entry.getValue()) == emoteId) {
				return entry.getKey();
			}
		}
		return null;
	}

	public HashMultimap<Long, Long> getRoleAssignMessages() {
		return roleAssignMessages;
	}

	public ArrayList<Role> getSelfAssignableRoles() {
		return selfAssignableRoles;
	}
	
	public boolean containsSelfAssignableRole(Role role) {
		return this.selfAssignableRoles.contains(role);
	}

	public Language getLang() {
		if(lang == null)
			this.getBot().getLogger().error("LANGUAGE ERROR OCCURED: PLEASE CHECK IF LANGUAGE WAS INITIALIZED FOR " + this.getGuild().getIdLong() + "!!!");
		
		return lang;
	}
	
	public MusicPlayer getPlayer() {
		return player;
	}
	
	public HashMap<String, Playlist> getPlaylists() {
		return playlists;
	}
	
	public Message getAudioControlsMessage() {
		return audioControlsMessage;
	}

	public Guild getGuild() {
		return guild;
	}
	
	public void setBot(Bot bot) {
		this.bot = bot;
	}

	public void setMuteRole(Role muteRole) {
		this.muteRole = muteRole;
	}

	public void setLogChannels(HashMap<String, TextChannel> logChannels) {
		this.logChannels = logChannels;
	}

	public void setIgnoredChannels(ArrayList<TextChannel> ignoredChannels) {
		this.ignoredChannels = ignoredChannels;
	}
	
	public void setPermissions(LinkedList<BotPermission> permissions) {
		this.permissions = permissions;
	}
	
	public void setPermission(BotPermission permission) {
		for(BotPermission perm : this.permissions) {
			if(PermissionUtil.isEqual(permission, perm)) {
				this.permissions.remove(perm);
			}
		}
		this.permissions.add(permission);
	}
	
	public void removePermission(BotPermission permission) {
		BotPermission toRemove = null;
		for(BotPermission perm : this.permissions) {
			if(permission.getId() == perm.getId()) {
				toRemove = perm;
			}
		}
		
		if(toRemove != null)
			this.permissions.remove(toRemove);
	}

	/**
	 * Sets roleEmotes hashmap to given parameter
	 * @param roleEmotes
	 */
	public void setRoleEmotes(HashMap<Role, Long> roleEmotes) {
		this.roleEmotes = roleEmotes;
	}
	
	/**
	 * Sets role emote for the specified role in local hashmap - not in database!
	 * @param role
	 * @param emoteId
	 */
	public void setRoleEmote(Role role, Long emoteId) {
		this.roleEmotes.put(role, emoteId);
	}
	
	/**
	 * Removes role emote for the specified role in local hashmap - not in database!
	 * @param role
	 */
	public void removeRoleEmote(Role role) {
		this.roleEmotes.remove(role);
	}

	public void setRoleAssignMessages(HashMultimap<Long, Long> roleAssignMessages) {
		this.roleAssignMessages = roleAssignMessages;
	}
	
	public void addRoleAssignMessage(Long messageId, Long[] roleIds) {
		for(Long roleId : roleIds) {
			this.roleAssignMessages.put(messageId, roleId);
		}
	}
	
	public void removeRoleAssignMessage(Long messageId) {
		this.roleAssignMessages.removeAll(messageId);
	}

	public void setSelfAssignableRoles(ArrayList<Role> selfAssignableRoles) {
		this.selfAssignableRoles = selfAssignableRoles;
	}
	
	public void addSelfAssignableRole(Role role) {
		if(!this.selfAssignableRoles.contains(role)) {
			this.selfAssignableRoles.add(role);
		}
	}
	
	public void removeSelfAssignableRole(Role role) {
		if(this.selfAssignableRoles.contains(role)) {
			this.selfAssignableRoles.remove(role);
		}
	}
	
	public void addTwitchSubscription(String name, int userId, String secret, SubscriptionType type, String url) {
		TwitchSubscription subscription = new TwitchSubscription(name, userId, secret, type, url);
		
		this.twitchSubscriptions.add(subscription);
	}
	
	public void removeTwitchSubscription(String name, SubscriptionType type) {
		TwitchSubscription toRemove = null;
		
		for(TwitchSubscription subscription : this.twitchSubscriptions) {
			if(subscription.getName().equals(name) && subscription.getType() == type) {
				toRemove = subscription;
				
				break;
			}
		}
		
		this.twitchSubscriptions.remove(toRemove);
	}
	
	public TwitchSubscription getTwitchSubscription(String name, SubscriptionType type) {
		for(TwitchSubscription subscription : this.twitchSubscriptions) {
			if(subscription.getName().equals(name) && subscription.getType() == type) {
				return subscription;
			}
		}
		
		return null;
	}
	
	public TwitchSubscription getTwitchSubscription(int userId) {
		for(TwitchSubscription subscription : this.twitchSubscriptions) {
			if(subscription.getUserId() == userId) {
				return subscription;
			}
		}
		
		return null;
	}
	
	public ArrayList<TwitchSubscription> getTwitchSubscriptions() {
		return twitchSubscriptions;
	}
	
	public void setTwitchSubscriptions(ArrayList<TwitchSubscription> twitchSubscriptions) {
		this.twitchSubscriptions = twitchSubscriptions;
	}

	public void setLang(Language lang) {
		this.lang = lang;
	}
	
	public void setPlayer(MusicPlayer player) {
		this.player = player;
	}
	
	public void setAudioControlsMessage(Message audioControlsMessage) {
		this.audioControlsMessage = audioControlsMessage;
	}

	public void setGuild(Guild guild) {
		this.guild = guild;
	}
	
	public List<Integer> getUsedRGBCodes() {
		return usedRGBCodes;
	}
	
}
