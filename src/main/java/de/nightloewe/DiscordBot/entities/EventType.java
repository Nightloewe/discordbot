package de.nightloewe.DiscordBot.entities;

public class EventType {
	
	public static final String ALL = "all";
	
	public static final String COMMAND_EXECUTION = "CommandExecution";
	
	public static final String USER_JOIN = "UserJoin";
	
	public static final String USER_LEAVE = "UserLeave";
	
	public static final String USER_CHANGE = "UserChange";
	
	public static final String USER_PRESENCE = "UserPresence";
	
	public static final String USER_ADD_ROLE = "UserAddRole";
	
	public static final String USER_REMOVE_ROLE = "UserRemoveRole";
	
	public static final String USER_KICKED = "UserKicked";
	
	public static final String USER_BANNED = "UserBanned";
	
	public static final String USER_MUTED = "UserMuted";
	
	public static final String MESSAGE_DELETED = "MessageDeleted";
	
	public static final String MESSAGE_UPDATED = "MessageEdited";
	
}
