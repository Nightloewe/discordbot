package de.nightloewe.DiscordBot.entities;

import de.nightloewe.DiscordBot.Bot;
import net.dv8tion.jda.core.entities.Member;

public class LevelData {
	
	private Member member;
	
	private int level;
	private int totalXP;
	private int xp;
	
	private long timestamp;
	
	public LevelData(Member member, int level, int totalXP, int xp, long timestamp) {
		this.member = member;
		this.level = level;
		this.totalXP = totalXP;
		this.xp = xp;
		this.timestamp = timestamp;
	}

	/**
	 * Updates the level data to the database.
	 * This function is blocking so call it asynchronously
	 */
	public void update() {
		Bot.getInstance().getDatabase().updateLevelData(member.getGuild(), this);
	}
	
	public int calculateXPForNextLevel() {
		return (int) (5 * Math.pow(this.getLevel(), 2) + 50 * this.getLevel() + 100);
	}
	
	public int calculateXPForLevel(int level) {
		return level <= 0 ? 0 : (int) (5 / 3d * Math.pow(level, 3) + 45 / 2d * Math.pow(level, 2) + 455 / 6d * level);
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getTotalXP() {
		return totalXP;
	}

	public void setTotalXP(int totalXP) {
		this.totalXP = totalXP;
	}

	public int getXp() {
		return xp;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public boolean addXp(int collectedXp) {
		int neededXp = this.calculateXPForNextLevel();
		
		int newXP = this.xp + collectedXp;
		
		this.totalXP = newXP;
		
		if(newXP >= neededXp) {
			newXP -= neededXp;
			
			this.xp = newXP;
			this.level++;
			
			return true;
		}
		
		return false;
	}
	
	

}
