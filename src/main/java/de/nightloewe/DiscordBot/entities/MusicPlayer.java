package de.nightloewe.DiscordBot.entities;

import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import com.sedmelluq.discord.lavaplayer.track.playback.AudioFrame;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.audio.AudioSendHandler;
import net.dv8tion.jda.core.entities.Member;

public class MusicPlayer extends AudioEventAdapter implements AudioSendHandler {

	private ConnectedGuild guild;
	private AudioPlayer player;
	private AudioPlayerManager playerManager;
	
	private BlockingDeque<AudioTrack> queue = new LinkedBlockingDeque<>();
//	private Cache<Integer, AudioTrack> lastTracks = CacheBuilder.newBuilder().maximumSize(20).expireAfterWrite(1, TimeUnit.DAYS).removalListener(new RemovalListener<Integer, AudioTrack>() {
//
//		@Override
//		public void onRemoval(RemovalNotification<Integer, AudioTrack> removed) {
//			for(Entry<Integer, AudioTrack> entry : lastTracks.asMap().entrySet()) {
//				Integer newKey = entry.getKey() - 1;
//				lastTracks.put(newKey, entry.getValue());
//			}
//		}
//	
//	}).build();
	private Stack<AudioTrack> lastTracksStack = new Stack<>();
	
	/**
	 * ArrayList containing member's in bot's channels who voted to skip current track.
	 */
	private ArrayList<Member> voteSkips = new ArrayList<Member>();
	
	private AudioPlaylist currentPlaylist;
	private AudioPlaylist defaultPlaylist;
	
	private AudioTrack nextTrack;
	
	private AudioFrame lastFrame;
	
	public MusicPlayer(ConnectedGuild g, AudioPlayerManager manager) {
		this.guild = g;
		this.playerManager = manager;
		this.player = manager.createPlayer();
		this.player.setVolume(50);
		this.player.addListener(this);
		
		this.lastTracksStack.setSize(20);
	}

	@Override
	public boolean canProvide() {
		if(this.lastFrame == null)
			this.lastFrame = this.player.provide();
		
		return this.lastFrame != null;
	}

	@Override
	public byte[] provide20MsAudio() {
		if(lastFrame == null) {
			lastFrame = this.player.provide();
		}
		
		byte[] data = lastFrame != null ? lastFrame.data : null;
		lastFrame = null;
		
		return data;
	}
	
	@Override
	public boolean isOpus() {
		return true;
	}
	
	public AudioPlaylist getCurrentPlaylist() {
		return currentPlaylist;
	}
	
	public AudioPlaylist getDefaultPlaylist() {
		return defaultPlaylist;
	}
	
	public AudioTrack getNextTrack() {
		return nextTrack;
	}
	
	public AudioPlayer getPlayer() {
		return player;
	}
	
	public AudioPlayerManager getPlayerManager() {
		return playerManager;
	}
	
	public BlockingDeque<AudioTrack> getQueue() {
		return queue;
	}
	
//	public Cache<Integer, AudioTrack> getLastTracks() {
//		return lastTracks;
//	}
	
	public Stack<AudioTrack> getLastTracksStack() {
		return lastTracksStack;
	}
	
	public ArrayList<Member> getVoteSkips() {
		return voteSkips;
	}
	
	public String getPlayingTrackName() {
		AudioTrack track = this.getPlayer().getPlayingTrack();
		
		if(track == null)
			return null;
		
		String name = (!track.getInfo().title.equalsIgnoreCase("Unknown title") ? track.getInfo().title : track.getIdentifier());
		return name;
	}
	
	public void setQueue(BlockingDeque<AudioTrack> queue) {
		this.queue = queue;
	}
	
	public void queue(AudioTrack track) {
		if(!player.startTrack(track, true)) {
			queue.offer(track);
			
			if(this.nextTrack == null) {
				this.nextTrack = queue.poll();
			}
			
			this.updateControls(this.player.getPlayingTrack());
		}
	}
	
	public void nextTrack() {
		if(queue.isEmpty()) {
			if(this.nextTrack != null) {
				player.startTrack(this.nextTrack, false);
				this.nextTrack = null;
				this.updateControls(player.getPlayingTrack());
				
				return;
			}
			if(this.currentPlaylist != null) {
				Bot.getInstance().getLogger().info("Using current playlist!");
				if(this.currentPlaylist instanceof Playlist) {
					Playlist playlist = (Playlist) this.currentPlaylist;
					this.nextTrack = playlist.getTracks().get(playlist.getCurrentTrackNumber() + 1);
					playlist.nextTrack();
				}
			} else {
				Bot.getInstance().getLogger().info("Current Playlist null!");
				if(this.defaultPlaylist != null) {
					Bot.getInstance().getLogger().info("Using default playlist!");
					if(this.defaultPlaylist instanceof Playlist) {
						Playlist playlist = (Playlist) this.defaultPlaylist;
						this.nextTrack = playlist.getTracks().get(playlist.getCurrentTrackNumber() + 1);
						playlist.play(this);
					} else {
						for(AudioTrack track : this.defaultPlaylist.getTracks()) {
							this.queue(track);
						}
					}
				} else {
					Bot.getInstance().getLogger().info("Stopping current track!");
					player.stopTrack();
				}
			}
		} else {
			player.startTrack(this.nextTrack, false);
			
			this.nextTrack = queue.poll();
			
			this.updateControls(player.getPlayingTrack());
		}
	}
	
	public void setPlaylist(AudioPlaylist currentPlaylist) {
		this.currentPlaylist = currentPlaylist;
		
		this.updateControls(null);
	}
	
	public void setDefaultPlaylist(AudioPlaylist defaultPlaylist) {
		this.defaultPlaylist = defaultPlaylist;
	}
	
	public void setNextTrack(AudioTrack nextTrack) {
		this.nextTrack = nextTrack;
	}
	
	public void loadPlaylist(Playlist playlist) {
		playlist.setLoading(true);
		for(String trackURL : playlist.getTrackURLs()) {
			this.playerManager.loadItemOrdered(this, trackURL, new AudioLoadResultHandler() {

				@Override
				public void trackLoaded(AudioTrack track) {
					try {
						playlist.addTrack(track);
					} catch (Exception e) {
						Bot.getInstance().getLogManager().logToChannel(MusicPlayer.this.guild.getGuild(), "MusicPlayerException", new BotEmbedBuilder().withErrorColor().setDescription(":warning: Track " + trackURL + " of Playlist " + playlist.getName() + " couldnt loaded: " + e.getLocalizedMessage()).build());
						e.printStackTrace();
					}
					
					if(playlist.getTracks().size() == playlist.getTrackURLs().size()) {
						playlist.setLoading(false);
						Bot.getInstance().getLogManager().logToChannel(MusicPlayer.this.guild.getGuild(), "MusicPlayerNotification", new BotEmbedBuilder().withOkColor().setDescription(":white_check_mark: Playlist " + playlist.getName() + " was successfully loaded! It can now be played.").build());
					}
				}

				@Override
				public void playlistLoaded(AudioPlaylist audioPlaylist) {
					Bot.getInstance().getLogManager().logToChannel(MusicPlayer.this.guild.getGuild(), "MusicPlayerException", new BotEmbedBuilder().withErrorColor().setDescription(":warning: Playlist " + playlist.getName() + " tried to add a playlist as track!").build());
					playlist.getTrackURLs().remove(trackURL);
				}

				@Override
				public void noMatches() {
					Bot.getInstance().getLogManager().logToChannel(MusicPlayer.this.guild.getGuild(), "MusicPlayerException", new BotEmbedBuilder().withErrorColor().setDescription(":warning: Track " + trackURL + " of Playlist " + playlist.getName() + " wasn't found!").build());
					playlist.getTrackURLs().remove(trackURL);
				}

				@Override
				public void loadFailed(FriendlyException exception) {
					Bot.getInstance().getLogManager().logToChannel(MusicPlayer.this.guild.getGuild(), "MusicPlayerException", new BotEmbedBuilder().withErrorColor().setDescription(":warning: Track " + trackURL + " of Playlist " + playlist.getName() + " couldnt loaded: " + exception.getLocalizedMessage()).build());
				}
				
			});
		}
	}
	
	@Override
	public void onPlayerPause(AudioPlayer player) {
		super.onPlayerPause(player);
		
		Bot.getInstance().getLogManager().logToChannel(this.guild.getGuild(), "MusicPlayerPaused", new BotEmbedBuilder().setDescription("Player paused").withErrorColor().build());
	}
	
	@Override
	public void onPlayerResume(AudioPlayer player) {
		super.onPlayerResume(player);
		
		Bot.getInstance().getLogManager().logToChannel(this.guild.getGuild(), "MusicPlayerResumed", new BotEmbedBuilder().setDescription("Player resumed").withOkColor().build());
	}
	
	@Override
	public void onTrackStart(AudioPlayer player, AudioTrack track) {
		super.onTrackStart(player, track);
		
		this.updateControls(track);
		
		Bot.getInstance().getLogManager().logToChannel(this.guild.getGuild(), "MusicPlayerStart", new BotEmbedBuilder().setDescription("Now playing: " + track.getIdentifier()).withOkColor().build(), msg -> msg.delete().queueAfter(30, TimeUnit.SECONDS));
	}
	
	@Override
	public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
//		long key = this.lastTracks.size() + 1;
//		
//		if(key == 20) {
//			key = 19;
//			this.lastTracks.invalidate(0);
//		}
//		
//		this.lastTracks.put((int) key, track);
		
		this.updateControls(null);
		
		if(track.getUserData() != null && ((Boolean)track.getUserData()) == false)
			this.lastTracksStack.push(track);
		
		if(this.lastTracksStack.size() > 20) {
			this.lastTracksStack.remove(0);
		}
		
		if(endReason.mayStartNext || endReason == AudioTrackEndReason.STOPPED) {
			this.nextTrack();
		}
	}
	
	public void updateControls(AudioTrack track) {
		MessageBuilder messageBuilder = new MessageBuilder();
		EmbedBuilder embedBuilder = new EmbedBuilder();
		
		messageBuilder.append(this.guild.getLang().MUSIC_PLAYER_CONTROL_KEY.format());
		embedBuilder.setTitle("Music Player - NightBot")
			.addField(this.guild.getLang().MUSIC_PLAYER_CURRENTLY_PLAYING.format(), track != null ? (track.getInfo().title.equalsIgnoreCase("Unknown title") ? track.getIdentifier() : track.getInfo().title) : this.guild.getLang().NOTHING.format(), false);
		
		String nextTrack = "";

		if(this.nextTrack != null) {
			nextTrack = this.nextTrack.getInfo().title.equalsIgnoreCase("Unknown title") ? this.nextTrack.getIdentifier() : this.nextTrack.getInfo().title;
		}
		
		if(nextTrack.isEmpty())
			nextTrack = this.guild.getLang().NOTHING.format();
		
		embedBuilder.addField(this.guild.getLang().MUSIC_PLAYER_NEXT_TRACK.format(), nextTrack, false);
		
		messageBuilder.setEmbed(embedBuilder.build());
		
		if(this.guild.getAudioControlsMessage() == null)
			return;
		
		this.guild.getAudioControlsMessage().editMessage(messageBuilder.build()).queue();
	}
	
}
