package de.nightloewe.DiscordBot.entities;

import java.util.ArrayList;
import java.util.List;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

/**
 * Deprecated, because currently not in use
 */
@Deprecated
public class MusicQueue {
	
	public List<AudioTrack> tracks = new ArrayList<AudioTrack>();
	
	public int currentTrack = -1;
	
	public synchronized AudioTrack lastTrack() {
		if(this.currentTrack != 0) {
			this.currentTrack--;
			
			AudioTrack track = this.tracks.get(this.currentTrack);
			
			return track;
		}
		
		return null;
	}
	
	public synchronized AudioTrack nextTrack() {
		if(this.tracks.size() == this.currentTrack) {
			this.currentTrack++;
			
			AudioTrack track = this.tracks.get(this.currentTrack);
			
			if(this.currentTrack > 19) {
				this.tracks.remove(0);
			}
			
			return track;
		} 
		
		return null;
	}
	
	public AudioTrack getUpcomingTrack() {
		if(this.tracks.size() == this.currentTrack) {
			AudioTrack track = this.tracks.get(this.currentTrack + 1);
			
			return track;
		}
		
		return null;
	}
	
	public void addTrack(AudioTrack track) {
		this.tracks.add(track);
	}
	
	public void removeTrack(int i) {
		
	}

}
