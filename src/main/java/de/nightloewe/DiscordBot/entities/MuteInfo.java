package de.nightloewe.DiscordBot.entities;

public class MuteInfo {

	private Long guildId, userId, staffId, expirationDate, issueDate;
	private String reason;
	
	public MuteInfo(Long guildId, Long userId, Long staffId, Long expirationDate, Long issueDate, String reason) {
		this.guildId = guildId;
		this.userId = userId;
		this.staffId = staffId;
		this.expirationDate = expirationDate;
		this.issueDate = issueDate;
		this.reason = reason;
	}

	public Long getGuildId() {
		return guildId;
	}
	
	public Long getUserId() {
		return userId;
	}
	
	public Long getStaffId() {
		return staffId;
	}
	
	public Long getExpirationDate() {
		return expirationDate;
	}
	
	public Long getIssueDate() {
		return issueDate;
	}
	
	public String getReason() {
		return reason;
	}
	
	public void setGuildId(Long guildId) {
		this.guildId = guildId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public void setStaffId(Long staffId) {
		this.staffId = staffId;
	}
	
	public void setExpirationDate(Long expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public void setIssueDate(Long issueDate) {
		this.issueDate = issueDate;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}
}
