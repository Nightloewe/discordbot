package de.nightloewe.DiscordBot.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import de.nightloewe.DiscordBot.Bot;

public class Playlist implements AudioPlaylist, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6135318379458780176L;
	
	private String name;
	private ArrayList<AudioTrack> tracks = new ArrayList<AudioTrack>();
	private int currentTrackNumber;
	private transient MusicPlayer player;
	private boolean isLoading = false;
	
	private final ArrayList<String> trackURLs;
	
	public Playlist(String name, AudioTrack... audioTracks) {
		this.name = name;
		this.currentTrackNumber = 0;
		for(AudioTrack track : audioTracks) {
			this.tracks.add(track);
		}
		this.trackURLs = null;
	}
	
	public Playlist(String name, ArrayList<String> trackURLs) {
		this.name = name;
		this.currentTrackNumber = 0;
		this.trackURLs = trackURLs;
	}
	
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public List<AudioTrack> getTracks() {
		return this.tracks;
	}

	@Override
	public AudioTrack getSelectedTrack() {
		return this.tracks.get(this.currentTrackNumber);
	}

	@Override
	public boolean isSearchResult() {
		return false;
	}

	public int getCurrentTrackNumber() {
		return currentTrackNumber;
	}
	
	public MusicPlayer getPlayer() {
		return player;
	}
	
	public boolean isLoading() {
		return isLoading;
	}
	
	public ArrayList<String> getTrackURLs() {
		return trackURLs;
	}
	
	public void addTrack(AudioTrack track) throws Exception {
		if(this.tracks.size() == 100)
			throw new Exception("Playlists can only have 100 tracks");
			
		this.tracks.add(track);
	}
	
	public void addTracks(AudioTrack...audioTracks) {
		for(AudioTrack track : audioTracks) {
			this.tracks.add(track);
		}
	}
	
	public void removeCurrentTrack() {
		this.tracks.remove(this.currentTrackNumber);
	}
	
	public void removeTrack(AudioTrack track) {
		this.tracks.remove(track);
	}
	
	public void setPlayer(MusicPlayer player) {
		this.player = player;
	}
	
	public void setLoading(boolean isLoading) {
		this.isLoading = isLoading;
	}
	
	public void play(MusicPlayer player) {
		this.setPlayer(player);
		player.setPlaylist(this);
		
		if(!this.getTracks().isEmpty()) {
			player.getPlayer().playTrack(this.getSelectedTrack());
		} else {
			Bot.getInstance().getLogger().warn("[MusicPlayer] Playlist " + this.getName() + " is empty!");
		}
	}
	
	public void nextTrack() {
		if(this.player != null) {
			this.currentTrackNumber++;
			
			if(this.tracks.size() < this.currentTrackNumber) {
				this.player.getPlayer().playTrack(this.getSelectedTrack());
			} else {
				this.getPlayer().setPlaylist(null);
				this.getPlayer().getPlayer().stopTrack();
				this.getPlayer().nextTrack();
				this.setPlayer(null);
				this.currentTrackNumber = 0;
			}
		}
	}

}
