package de.nightloewe.DiscordBot.entities;

public class Setting {

	private int id;
	private long guildId;
	private String key, value;
	private long valueInt;
	
	public Setting(int id, long guildId, String key, String value) {
		this.id = id;
		this.guildId = guildId;
		this.key = key;
		this.value = value;
	}
	
	public Setting(int id, long guildId, String key, long value) {
		this(id, guildId, key, "useInt");
		this.valueInt = value;
	}
	
	public int getId() {
		return id;
	}
	
	public long getGuildId() {
		return guildId;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
	
	public long getValueInt() {
		return valueInt;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setGuildId(int guildId) {
		this.guildId = guildId;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public void setValueInt(long valueInt) {
		this.valueInt = valueInt;
	}
	
	public boolean isTrue() {
		if(this.valueInt == 1) {
			return true;
		}
		return false;
	}
	
}
