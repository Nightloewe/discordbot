package de.nightloewe.DiscordBot.entities;

import java.util.TimerTask;

public class TwitchSubscription {
	
	private String name;
	private int userId;
	
	private String secret;
	private String webhookURL;
	
	private SubscriptionType type;
	
	/** MIXER ONLY */
	private boolean initialized;
	private String expiresAt;
	private String id;
	/** MIXER ONLY */
	
	private TimerTask refreshTask;
	
	public TwitchSubscription(String name, int userId, String secret, SubscriptionType type, String webhookURL) {
		this.name = name;
		this.userId = userId;
		this.secret = secret;
		
		this.webhookURL = webhookURL;
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getSecret() {
		return secret;
	}
	
	public void setSecret(String secret) {
		this.secret = secret;
	}
	
	public String getWebhookURL() {
		return webhookURL;
	}
	
	public void setWebhookURL(String webhookURL) {
		this.webhookURL = webhookURL;
	}
	
	public SubscriptionType getType() {
		return type;
	}
	
	public void setType(SubscriptionType type) {
		this.type = type;
	}
	
	public TimerTask getRefreshTask() {
		return refreshTask;
	}
	
	public void setRefreshTask(TimerTask refreshTask) {
		this.refreshTask = refreshTask;
	}
	
	public boolean isInitialized() {
		return initialized;
	}
	
	public void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}
	
	public String getExpiresAt() {
		return expiresAt;
	}
	
	public void setExpiresAt(String expiresAt) {
		this.expiresAt = expiresAt;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public static enum SubscriptionType {
		TWITCH("https://api.twitch.tv/helix/streams?user_id=%d"), MIXER("");
		
		private String urlFormat;
		
		private SubscriptionType(String urlFormat) {
			this.urlFormat = urlFormat;
		}
		
		public String getUrlFormat() {
			return urlFormat;
		}
	}

}
