package de.nightloewe.DiscordBot.entities;

import java.util.Optional;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;

public class Warn {

	private long id, guildId, userId, staffId, issueDate;
	private String reason;
	
	private Optional<Member> user;
	private Optional<Member> staffUser;
	private Optional<Guild> guild;
	private int rows;
	
	public Warn(long id, long guildId, long userId, long staffId, long issueDate, String reason, Member user,
			Member staffUser, Guild guild) {
		this.id = id;
		this.guildId = guildId;
		this.userId = userId;
		this.staffId = staffId;
		this.issueDate = issueDate;
		this.reason = reason;
		this.user = Optional.ofNullable(user);
		this.staffUser = Optional.ofNullable(staffUser);
		this.guild = Optional.ofNullable(guild);
	}

	public Warn(long id, long guildId, long userId, long staffId, long issueDate, String reason) {
		this(id, guildId, userId, staffId, issueDate, reason, null, null, null);
	}
	
	public long getId() {
		return id;
	}

	public long getGuildId() {
		return guildId;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public long getStaffId() {
		return staffId;
	}
	
	public long getIssueDate() {
		return issueDate;
	}
	
	public String getReason() {
		return reason;
	}
	
	public Optional<Member> getUser() {
		return user;
	}
	
	public Optional<Member> getStaffUser() {
		return staffUser;
	}
	
	public Optional<Guild> getGuild() {
		return guild;
	}
	
	public int getRows() {
		return rows;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setGuildId(long guildId) {
		this.guildId = guildId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public void setStaffId(long staffId) {
		this.staffId = staffId;
	}
	
	public void setIssueDate(long issueDate) {
		this.issueDate = issueDate;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public void setUser(Member user) {
		this.user = Optional.ofNullable(user);
	}
	
	public void setStaffUser(Member staffUser) {
		this.staffUser = Optional.ofNullable(staffUser);
	}
	
	public void setGuild(Guild guild) {
		this.guild = Optional.ofNullable(guild);
	}
	
	public void setRows(int rows) {
		this.rows = rows;
	}
}
