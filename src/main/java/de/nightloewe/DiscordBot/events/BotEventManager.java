package de.nightloewe.DiscordBot.events;

import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.InterfacedEventManager;

public class BotEventManager extends InterfacedEventManager {

	private boolean isListening = false;
	
	@Override
	public void handle(Event event) {
		if(event instanceof ReadyEvent) {
			super.handle(event);
			return;
		}
		
		if(this.isListening)
			super.handle(event);
	}
	
	public void setListening(boolean isListening) {
		this.isListening = isListening;
	}
	
	public boolean isListening() {
		return isListening;
	}
	
}
