package de.nightloewe.DiscordBot.events;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.entities.ConnectedGuild;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import de.nightloewe.DiscordBot.util.PermissionUtil;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.ReconnectedEvent;
import net.dv8tion.jda.core.events.ResumedEvent;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.core.events.guild.GuildUnavailableEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class ConnectionListener extends ListenerAdapter {

	@Override
	public void onReady(ReadyEvent event) {
//		for(Guild guild : event.getJDA().getGuilds()) {
//			guild.getDefaultChannel().sendMessage(
//					new EmbedBuilder()
//					.setDescription("**NightBot starting!** Ready to accept commands.")
//					.setColor(guild.getSelfMember().getColor())
//					.build()
//					).queue(msg -> msg.delete().queueAfter(5, TimeUnit.SECONDS));
//		}
	}
	
	@Override
	public void onGuildUnavailable(GuildUnavailableEvent event) {
		Bot.getInstance().getLogger().warn(event.getGuild().getName() + "#" + event.getGuild().getId() + " is unavailable due to discord problems!");
	}
	
	@Override
	public void onReconnect(ReconnectedEvent event) {
		super.onReconnect(event);
	}
	
	@Override
	public void onResume(ResumedEvent event) {
		super.onResume(event);
	}
	
	@Override
	public void onGuildJoin(GuildJoinEvent event) {
		if(PermissionUtil.isBlacklisted(event.getGuild())) {
			Bot.getInstance().getLogger().warn("Blacklisted guild was found, bot is contacting owner and leaving server [ID: " + event.getGuild().getId() + "]");
			event.getGuild().getOwner().getUser().openPrivateChannel().queue(privateChannel -> {
				new BotEmbedBuilder().setDescription("Your guild is on the blacklist of the bot. Please contact the bot owner for more information.").withErrorColor().sendMessage(privateChannel);
				event.getGuild().leave().queue();
			}, failure -> event.getGuild().leave().queue());
			return;
		}
		
		if(Bot.getInstance().getConnectedGuild(event.getGuild()) == null) {
			Bot.getInstance().createSettingsMap(event.getGuild().getIdLong());
			Bot.getInstance().setSetting(event.getGuild().getIdLong(), "DeleteCommandMessages", 0);
			Bot.getInstance().setSetting(event.getGuild().getIdLong(), "Logging", 0);
			Bot.getInstance().setSetting(event.getGuild().getIdLong(), "Level", 0);
			Bot.getInstance().setSetting(event.getGuild().getIdLong(), "Language", "en_EN");
			Bot.getInstance().getDatabase().setLogChannel(event.getGuild().getIdLong(), event.getGuild().getDefaultChannel().getIdLong(), "all");
			Bot.getInstance().setupGuild(event.getGuild());
			Bot.getInstance().getConnectedGuild(event.getGuild()).getLogChannels().put("all", event.getGuild().getDefaultChannel());
			Bot.getInstance().getLogger().info("[+] Joined Server " + event.getGuild().getName() + " [ID: " + event.getGuild().getId() + "; OWNERID: " + event.getGuild().getOwner().getUser().getId() + "]");
			
			event.getGuild().getOwner().getUser().openPrivateChannel().queue(dm -> {
				new BotEmbedBuilder().withOkColor().appendDescription("Thanks for using ModBot.").sendMessage(dm);
			});
		}
	}
	
	@Override
	public void onGuildLeave(GuildLeaveEvent event) {
		Bot.getInstance().getConnectedGuilds().remove(event.getGuild());
		Bot.getInstance().getDatabase().resetGuild(event.getGuild().getIdLong());
		Bot.getInstance().getLogger().info("[-] Left Server " + event.getGuild().getName() + " [ID: " + event.getGuild().getId() + "; OWNERID: " + event.getGuild().getOwner().getUser().getId() + "]");
	}
	
}
