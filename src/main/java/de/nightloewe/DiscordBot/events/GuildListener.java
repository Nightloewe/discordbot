package de.nightloewe.DiscordBot.events;

import java.util.ArrayList;
import java.util.Map.Entry;

import de.nightloewe.DiscordBot.Bot;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.role.RoleDeleteEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class GuildListener extends ListenerAdapter  {

	@Override
	public void onRoleDelete(RoleDeleteEvent event) {
		if(Bot.getInstance().getConnectedGuild(event.getGuild()).getMuteRole() == event.getRole()) {
			Bot.getInstance().getConnectedGuild(event.getGuild()).setMuteRole(null);
			Bot.getInstance().setSetting(event.getGuild().getIdLong(), "MuteRoleId", 0);
		}
		
		ArrayList<Role> selfAssignableRoles = Bot.getInstance().getConnectedGuild(event.getGuild()).getSelfAssignableRoles();
		if(selfAssignableRoles.contains(event.getRole())) {
			Bot.getInstance().getConnectedGuild(event.getGuild()).removeSelfAssignableRole(event.getRole());
			Bot.getInstance().getDatabase().removeSelfAssignableRole(event.getGuild().getIdLong(), event.getRole().getIdLong());
		}
	}
	
}
