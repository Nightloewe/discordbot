package de.nightloewe.DiscordBot.events;

import java.util.Set;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.command.CommandInfo;
import de.nightloewe.DiscordBot.config.Language;
import de.nightloewe.DiscordBot.entities.ConnectedGuild;
import de.nightloewe.DiscordBot.entities.EventType;
import de.nightloewe.DiscordBot.entities.LevelData;
import de.nightloewe.DiscordBot.entities.MuteInfo;
import de.nightloewe.DiscordBot.entities.Setting;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageUpdateEvent;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class MessageListener extends ListenerAdapter {
	
	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		Bot.getInstance().getMessageCache().put(event.getMessageIdLong(), event.getMessage());
		
		if(event.getAuthor().isBot()) {
			return;
		}
		
		Setting receiveFromPMSetting = Bot.getInstance().getSetting(-1, "AllowFromPrivateMessage");
		Setting deleteCommandMessagesSetting = Bot.getInstance().getSetting(-1, "DeleteCommandMessages");
		
		if(event.isFromType(ChannelType.TEXT)) {
			Setting commandPrefix = Bot.getInstance().getSetting(-1, "CommandPrefix");
			deleteCommandMessagesSetting = Bot.getInstance().getSetting(event.getGuild().getIdLong(), "DeleteCommandMessages");
			
			if(event.getMessage().getContentRaw().startsWith(commandPrefix.getValue())) {
				try {
					CommandInfo info = new CommandInfo(event.getMessage());
					
					boolean success = Bot.getInstance().getCommandMap().dispatchCommand(info);
					if(deleteCommandMessagesSetting.isTrue() && success) {
						event.getMessage().delete().queue();
					}
				} catch(IllegalArgumentException e) {
					MessageEmbed embed = new EmbedBuilder().setDescription(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().COMMAND_FAILURE.format()).addField("Error", e.getMessage(), false).build();
					event.getChannel().sendMessage(embed).queue();
				}
			}
			
			if(Bot.getInstance().getSetting(event.getGuild().getIdLong(), "Level").isTrue()) {
				Setting defaultLang = Bot.getInstance().getSetting(-1, "DefaultLanguage");
				Setting lang = Bot.getInstance().getSetting(event.getGuild().getIdLong(), "Language");
				
				Language language = Bot.getInstance().getLanguage((lang != null ? lang.getValue() : defaultLang.getValue()));
				
				LevelData data = Bot.getInstance().getDatabase().getLevelData(event.getGuild(), event.getMember().getUser().getIdLong());
				
				if(data == null) {
					data = new LevelData(event.getMember(), 0, 0, 0, -1);
				}
				
				if(System.currentTimeMillis() - data.getTimestamp() > 60L) {
					int collectedXp = event.getMessage().getContentRaw().length();
					
					if(collectedXp > 25) {
						collectedXp = 25;
					}
					
					if(data.addXp(collectedXp)) {
						new BotEmbedBuilder().withOkColor().setDescription(event.getMember().getAsMention() + " " + language.LEVELUP.format()).sendMessage(event.getChannel());
					}
					
					data.setTimestamp(System.currentTimeMillis());
					data.update();
				}
			}
			
			if(Bot.getInstance().isMuted(event.getMember())) {
				MuteInfo info = Bot.getInstance().getDatabase().getMute(event.getGuild().getIdLong(), event.getAuthor().getIdLong());
				if(info != null) {
					if(info.getExpirationDate() < System.currentTimeMillis()) {
						Bot.getInstance().getDatabase().removeMute(info.getGuildId(), info.getUserId());
						Member member = event.getGuild().getMemberById(info.getUserId());
						event.getGuild().getController().setMute(member, false).queue();
						event.getGuild().getController().removeRolesFromMember(member, Bot.getInstance().getConnectedGuild(event.getGuild()).getMuteRole()).queue();
						Bot.getInstance().getLogger().info("User " + event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator() + " [ID: " + event.getAuthor().getId() + "] was unmuted");
					}
				}
			}
		} else {
			if(!receiveFromPMSetting.isTrue()) {
				event.getChannel().sendMessage(Bot.getInstance().getDefaultLanguage().ONLY_GUILD_MESSAGES.format()).queue();
			}
		}
	}
	
	@Override
	public void onGuildMessageDelete(GuildMessageDeleteEvent event) {
		Message cachedMessage = Bot.getInstance().getMessageCache().getIfPresent(event.getMessageIdLong());
		
		if(Bot.getInstance().getConnectedGuild(event.getGuild()).getRoleAssignMessages().containsKey(event.getMessageIdLong())) {
			Bot.getInstance().getConnectedGuild(event.getGuild()).removeRoleAssignMessage(event.getMessageIdLong());
			Bot.getInstance().getDatabase().removeRoleAssignMessage(event.getGuild().getIdLong(), event.getMessageIdLong());
		}
		
		EmbedBuilder builder = new EmbedBuilder()
				.setTitle(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().TITLE_MESSAGE_DELETED.format(event.getChannel().getName()));
		
		if(cachedMessage != null)
			builder.setDescription(cachedMessage.getAuthor().getName() + "#" + cachedMessage.getAuthor().getDiscriminator())
			.addField(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().MESSAGE_CONTENT.format(), cachedMessage.getContentRaw(), false);
		
		if(cachedMessage != null && cachedMessage.getAuthor().isBot())
			return;
		
		builder.addField("Id", event.getMessageId(), false);
		
		Bot.getInstance().getLogManager().logToChannel(event.getGuild(), EventType.MESSAGE_DELETED, builder.build());
	}

	@Override
	public void onGuildMessageUpdate(GuildMessageUpdateEvent event) {
		Message cachedMessage = Bot.getInstance().getMessageCache().getIfPresent(event.getMessageIdLong());
		
		EmbedBuilder builder = new EmbedBuilder()
				.setTitle(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().TITLE_MESSAGE_UPDATED.format(event.getChannel().getName()))
				.setDescription(event.getAuthor().getName() + "#" + event.getAuthor().getDiscriminator());
		
		if(cachedMessage != null)
			builder.addField(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().OLD_MESSAGE_CONTENT.format(), cachedMessage.getContentRaw(), false);
		
			builder.addField(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().NEW_MESSAGE_CONTENT.format(), event.getMessage().getContentRaw(), false)
				.addField("Id", event.getMessageId(), false);
		
		if(event.getGuild() != null)
			Bot.getInstance().getLogManager().logToChannel(event.getGuild(), EventType.MESSAGE_UPDATED, builder.build());
	}
	
	@Override
	public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
		if(event.getMember().getUser().isBot())
			return;
		
		if(Bot.getInstance().getConnectedGuild(event.getGuild()).getRoleAssignMessages().containsKey(event.getMessageIdLong())) {
			Set<Long> roleIds = Bot.getInstance().getConnectedGuild(event.getGuild()).getRoleAssignMessages().get(event.getMessageIdLong());
			
			if(event.getReaction().getReactionEmote().getEmote().getId() == null) {
				event.getReaction().removeReaction(event.getUser()).queue();
				return;
			}
			
			if(Bot.getInstance().getConnectedGuild(event.getGuild()).getRoleEmotes().containsValue(event.getReaction().getReactionEmote().getEmote().getIdLong())) {
				Role role = Bot.getInstance().getConnectedGuild(event.getGuild()).getRoleByRoleEmoteId(event.getReaction().getReactionEmote().getEmote().getIdLong());
				
				System.out.println("Role: " + role);
				
				if(roleIds.contains(role.getIdLong())) {
					event.getGuild().getController().addRolesToMember(event.getMember(), role).queue();
					return;
				}
			}
			event.getReaction().removeReaction(event.getUser()).queue();
		}
		
		ConnectedGuild cGuild = Bot.getInstance().getConnectedGuild(event.getGuild());
		
		if(cGuild.getAudioControlsMessage() != null && event.getMessageIdLong() == cGuild.getAudioControlsMessage().getIdLong()) {
			if(event.getMember().hasPermission(Permission.VOICE_MUTE_OTHERS)) {
				if(event.getReactionEmote().getName().equals("\u23EF")) {
					if(cGuild.getPlayer().getPlayer().isPaused()) {
						cGuild.getPlayer().getPlayer().setPaused(false);
					} else {
						cGuild.getPlayer().getPlayer().setPaused(true);
					}
				} else if(event.getReactionEmote().getName().equals("\u23E9")) {
					cGuild.getPlayer().nextTrack();
				} else if(event.getReactionEmote().getName().equals("\u23EA")) {
					AudioTrack track = cGuild.getPlayer().getPlayer().getPlayingTrack();
					
					AudioTrack lastTrack = cGuild.getPlayer().getLastTracksStack().pop();
					
					event.getReaction().removeReaction(event.getUser()).queue();
					if(lastTrack != null) {
						cGuild.getPlayer().getQueue().offerFirst(track.makeClone());
					} else {
						return;
					}
						
					AudioTrack newTrack = lastTrack.makeClone();
					
					cGuild.getPlayer().getPlayer().playTrack(newTrack);
				}
			} else {
				event.getMember().getUser().openPrivateChannel().queue(msg -> msg.sendMessage(new BotEmbedBuilder().withErrorColor().setDescription("You do not have permission to do that!").build()));
			}
			event.getReaction().removeReaction(event.getUser()).queue();
		}
	}
	
	@Override
	public void onGuildMessageReactionRemove(GuildMessageReactionRemoveEvent event) {
		if(event.getMember().getUser().isBot())
			return;
		
		if(event.getReaction().getReactionEmote().getId() == null) 
			return;
		
		if(Bot.getInstance().getConnectedGuild(event.getGuild()).getRoleAssignMessages().containsKey(event.getMessageIdLong())) {
			Set<Long> roleIds = Bot.getInstance().getConnectedGuild(event.getGuild()).getRoleAssignMessages().get(event.getMessageIdLong());
			
			if(Bot.getInstance().getConnectedGuild(event.getGuild()).getRoleEmotes().containsValue(event.getReaction().getReactionEmote().getEmote().getIdLong())) {
				Role role = Bot.getInstance().getConnectedGuild(event.getGuild()).getRoleByRoleEmoteId(event.getReaction().getReactionEmote().getEmote().getIdLong());
				
				if(roleIds.contains(role.getIdLong())) {
					event.getGuild().getController().removeRolesFromMember(event.getMember(), role).queue();
					return;
				}
			}
		}
	}
	
}
