package de.nightloewe.DiscordBot.events;

import java.lang.reflect.Field;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.entities.EventType;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.events.guild.GuildBanEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberNickChangeEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberRoleAddEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberRoleRemoveEvent;
import net.dv8tion.jda.core.events.user.UserAvatarUpdateEvent;
import net.dv8tion.jda.core.events.user.UserGameUpdateEvent;
import net.dv8tion.jda.core.events.user.UserNameUpdateEvent;
import net.dv8tion.jda.core.events.user.UserOnlineStatusUpdateEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class UserListener extends ListenerAdapter {

	public void onGuildMemberJoin(GuildMemberJoinEvent event) {
		Bot.getInstance().getLogManager().logToChannel(event.getGuild(), EventType.USER_LEAVE, 
				new EmbedBuilder()
				.setTitle(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().EVENT_USER_JOINED.format())
				.setDescription(event.getUser().getName() + "#" + event.getUser().getDiscriminator())
				.setImage(event.getUser().getAvatarUrl())
				.addField("Id", event.getUser().getId(), false)
				.build());
	}
	
	@Override
	public void onGuildMemberLeave(GuildMemberLeaveEvent event) {
		Bot.getInstance().getLogManager().logToChannel(event.getGuild(), EventType.USER_LEAVE, 
				new EmbedBuilder()
				.setTitle(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().EVENT_USER_LEFT.format())
				.setDescription(event.getUser().getName() + "#" + event.getUser().getDiscriminator())
				.addField("Id", event.getUser().getId(), false)
				.build());
	}
	
	@Override
	public void onGuildBan(GuildBanEvent event) {
		Bot.getInstance().getLogManager().logToChannel(event.getGuild(), EventType.USER_BANNED, 
				new EmbedBuilder()
				.setTitle(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().EVENT_USER_BANNED.format())
				.setDescription(event.getUser().getName() + "#" + event.getUser().getDiscriminator())
				.setImage(event.getUser().getAvatarUrl())
				.addField("Id", event.getUser().getId(), false)
				.build());
	}
	
	@Override
	public void onGuildMemberRoleAdd(GuildMemberRoleAddEvent event) {
		Bot.getInstance().getLogManager().logToChannel(event.getGuild(), EventType.USER_ADD_ROLE, 
				new EmbedBuilder()
				.setTitle(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().EVENT_USER_ROLE_ADDED.format())
				.setDescription(event.getUser().getName() + "#" + event.getUser().getDiscriminator())
				.addField("Rolle", event.getRoles().get(0).getName(), false)
				.build());
	}
	
	@Override
	public void onGuildMemberRoleRemove(GuildMemberRoleRemoveEvent event) {
		Bot.getInstance().getLogManager().logToChannel(event.getGuild(), EventType.USER_REMOVE_ROLE, 
				new EmbedBuilder()
				.setTitle(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().EVENT_USER_ROLE_REMOVED.format())
				.setDescription(event.getUser().getName() + "#" + event.getUser().getDiscriminator())
				.addField("Rolle", event.getRoles().get(0).getName(), false)
				.build());
	}
	
	@Override
	public void onUserAvatarUpdate(UserAvatarUpdateEvent event) {
		MessageEmbed embed = new EmbedBuilder()
				.setDescription(event.getUser().getName() + "#" + event.getUser().getDiscriminator())
				.setThumbnail(event.getUser().getAvatarUrl())
				.build();
		
		for(Guild guild : event.getUser().getMutualGuilds()) {
			Field f;
			try {
				f = embed.getClass().getDeclaredField("title");
				f.setAccessible(true);
				f.set(embed, Bot.getInstance().getConnectedGuild(guild).getLang().EVENT_AVATAR_CHANGE);
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				Bot.getInstance().getLogger().warn("embed class: " + embed.getClass().getName());
				e.printStackTrace();
			}
			Bot.getInstance().getLogManager().logToChannel(guild, EventType.USER_CHANGE, embed);
		}
	}
	
	@Override
	public void onUserNameUpdate(UserNameUpdateEvent event) {
		EmbedBuilder embed = new EmbedBuilder()
				.setTitle("Benutzername geändert")
				.setDescription(event.getUser().getName() + "#" + event.getUser().getDiscriminator());
		
		for(Guild guild : event.getUser().getMutualGuilds()) {
			embed.setTitle(Bot.getInstance().getConnectedGuild(guild).getLang().EVENT_USERNAME_CHANGE.format())
				.addField(Bot.getInstance().getConnectedGuild(guild).getLang().OLD_NAME.format(), event.getOldName() + "#" + event.getOldDiscriminator(), false)
				.addField(Bot.getInstance().getConnectedGuild(guild).getLang().NEW_NAME.format(), event.getUser().getName() + "#" + event.getUser().getDiscriminator(), false);
			Bot.getInstance().getLogManager().logToChannel(guild, EventType.USER_CHANGE, embed.build());
		}
	}
	
	@Override
	public void onGuildMemberNickChange(GuildMemberNickChangeEvent event) {
		Bot.getInstance().getLogManager().logToChannel(event.getGuild(), EventType.USER_CHANGE, new EmbedBuilder()
				.setTitle(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().EVENT_NICKNAME_CHANGE.format())
				.setDescription(event.getUser().getName() + "#" + event.getUser().getDiscriminator())
				.addField(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().OLD_NICKNAME.format(), event.getPrevNick() + "#" + event.getUser().getDiscriminator(), false)
				.addField(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().NEW_NICKNAME.format(), event.getNewNick() + "#" + event.getUser().getDiscriminator(), false)
				.build());
	}
	
	@Override
	public void onUserGameUpdate(UserGameUpdateEvent event) {
		Member member = event.getGuild().getMember(event.getUser());
		
		Bot.getInstance().getLogManager().logToChannel(event.getGuild(), EventType.USER_PRESENCE, 
				new EmbedBuilder()
				.setTitle(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().EVENT_PRESENCE.format())
				.setDescription(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().NOW_PLAYING.format(event.getUser().getName(), event.getUser().getDiscriminator(), (member.getGame() != null ? member.getGame().getName() : Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().NOTHING.format()))).build());
	}
	
	@Override
	public void onUserOnlineStatusUpdate(UserOnlineStatusUpdateEvent event) {
		EmbedBuilder builder = new EmbedBuilder()
				.setTitle(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().EVENT_PRESENCE.format());
		
		Bot.getInstance().getLogManager().logToChannel(event.getGuild(), EventType.USER_PRESENCE, 
				builder
				.setDescription(Bot.getInstance().getConnectedGuild(event.getGuild()).getLang().STATUS_UPDATE.format(event.getUser().getName(), event.getUser().getDiscriminator(), event.getGuild().getMember(event.getUser()).getOnlineStatus().name()))
				.build());
	}
	
}
