package de.nightloewe.DiscordBot.modules;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.command.CommandInfo;
import de.nightloewe.DiscordBot.command.ICommand;
import de.nightloewe.DiscordBot.command.IModule;
import de.nightloewe.DiscordBot.command.Module;
import de.nightloewe.DiscordBot.command.ModuleAlias;
import de.nightloewe.DiscordBot.command.annotations.Command;
import de.nightloewe.DiscordBot.command.annotations.RequireBotUserPermission;
import de.nightloewe.DiscordBot.command.annotations.RequireGuild;
import de.nightloewe.DiscordBot.command.annotations.RequireOwner;
import de.nightloewe.DiscordBot.command.annotations.RequireUserPermission;
import de.nightloewe.DiscordBot.config.Language;
import de.nightloewe.DiscordBot.entities.ConnectedGuild;
import de.nightloewe.DiscordBot.entities.EventType;
import de.nightloewe.DiscordBot.entities.MuteInfo;
import de.nightloewe.DiscordBot.entities.Setting;
import de.nightloewe.DiscordBot.entities.Warn;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import de.nightloewe.DiscordBot.util.DateUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Emote;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Game.GameType;
import net.dv8tion.jda.core.entities.Icon;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.requests.RestAction;

public class AdministrationModule extends Module {

	public AdministrationModule(Bot bot) {
		super(bot);
	}

	@Override
	public String getName() {
		return "Administration";
	}

	@RequireOwner
	@Command(name = "die", description = "Shutdowns the bot")
	public boolean shutdown(CommandInfo info) {
		info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getAuthor().getAsMention() + " Shutting down.").build()).complete();
		
		this.getBot().getLogger().info("Shutdown initiated by " + info.getAuthor().getName() + "#" + info.getAuthor().getDiscriminator() + " [ID: " + info.getAuthor().getId() + "]");
		info.getChannel().getJDA().shutdown();
		
		return true;
	}
	
	@RequireOwner
	@Command(name = "invite", description = "Invite command")
	public boolean invite(CommandInfo info) {
		info.getChannel().sendMessage(info.getMessage().getJDA().asBot().getInviteUrl(Permission.ADMINISTRATOR)).queue();
		return true;
	}
	
	@RequireOwner
	@Command(name = "setSetting", description = "Debug command", usage = "<guildId> <key> <value>")
	public boolean setSettingDebug(CommandInfo info) {
		if(info.getArguments().length == 3) {
			if(!info.getArguments()[0].matches("(\\d)+")) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getAuthor().getAsMention() + " First argument must be a number!").setColor(Color.RED).build()
						).queue();
				return true;
			}
			if(info.getArguments()[2].matches("(\\d)+")) {
				this.getBot().setSetting(Long.parseLong(info.getArguments()[0]), info.getArguments()[1], Integer.parseInt(info.getArguments()[2]));
			} else {
				this.getBot().setSetting(Long.parseLong(info.getArguments()[0]), info.getArguments()[1], info.getArguments()[2]);
			}
			info.getChannel().sendMessage(
					new EmbedBuilder().setDescription(info.getAuthor().getAsMention() + " DEBUG: Set " + info.getArguments()[1] + " to " + info.getArguments()[2] + " for guildId " + info.getArguments()[0]).setColor(Color.GREEN).build()
					).queue();
		}
		return false;
	}
	
	@RequireOwner
	@Command(name = "getSetting", description = "Debug command", usage = "<guildId> <key>")
	public boolean getSettingDebug(CommandInfo info) {
		if(info.getArguments().length == 2) {
			if(!info.getArguments()[0].matches("(\\d)+")) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getAuthor().getAsMention() + " First argument must be a number!").setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			Setting setting = this.getBot().getSetting(Long.parseLong(info.getArguments()[0]), info.getArguments()[1]);
			
			info.getChannel().sendMessage(
					new EmbedBuilder().setDescription(info.getAuthor().getAsMention() + " DEBUG: Setting " + info.getArguments()[1] + " for guildId " + info.getArguments()[0] + ": " + setting.getValue() + ";" + setting.getValueInt()).setColor(Color.GREEN).build()
					).queue();
		}
		return false;
	}
	
	@RequireUserPermission(channelPermission = {Permission.MESSAGE_MANAGE})
	@RequireBotUserPermission(channelPermission = {Permission.MESSAGE_MANAGE})
	@Command(name = "prune", description = "PRUNE_DESC", usage = "[amount]")
	public boolean prune(CommandInfo info) {
		int amount = 10;
		
		if(info.getArguments().length == 1) {
			try {
				amount = Integer.parseInt(info.getArguments()[0]);
			} catch(NumberFormatException ex) {
				return false;
			}
		}
		
		final int finalAmount = amount;
		
		info.getChannel().getHistory().retrievePast(amount + 1).queue(success -> {
			for(Message message : success) {
				message.delete().queue();
			}
			
			info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().PRUNED_MESSAGES.format(finalAmount)).build()).queue(message -> {
				message.delete().queueAfter(5L, TimeUnit.SECONDS);
			});
		});
		
		return true;
	}
	
	@RequireOwner
	@Command(name = "setGame", description = "SETGAME_DESC", usage = "clear/<Game>")
	public boolean setGame(CommandInfo info) {
		if(info.getArguments().length == 1) {
			if(info.getArguments()[0].equalsIgnoreCase("clear")) {
				info.getChannel().getJDA().getPresence().setGame(null);
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().GAME_SET.format(info.getAuthor().getAsMention())).build()).queue();
			} else {
				info.getChannel().getJDA().getPresence().setGame(Game.of(GameType.DEFAULT, info.getArguments()[0]));
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().GAME_CLEARED.format(info.getAuthor().getAsMention())).build()).queue();
			}
			return true;
		} else {
			return false;
		}
	}
	
	@RequireOwner
	@Command(name = "setStatus", description = "SETSTATUS_DESC", usage = ".setStatus <online/afk/dnd/invisible>")
	public boolean setStatus(CommandInfo info) {
		if(info.getArguments().length == 1) {
			EmbedBuilder builder = new EmbedBuilder();
			
			if(info.getArguments()[0].equalsIgnoreCase("online")) {
				info.getChannel().getJDA().getPresence().setStatus(OnlineStatus.ONLINE);
				builder.setDescription(info.getLang().ONLINESTATUS_ONLINE.format(info.getAuthor().getAsMention()));
			} else if(info.getArguments()[0].equalsIgnoreCase("afk")) {
				info.getChannel().getJDA().getPresence().setStatus(OnlineStatus.IDLE);
				builder.setDescription(info.getLang().ONLINESTATUS_AFK.format(info.getAuthor().getAsMention()));
			} else if(info.getArguments()[0].equalsIgnoreCase("dnd")) {
				info.getChannel().getJDA().getPresence().setStatus(OnlineStatus.DO_NOT_DISTURB);
				builder.setDescription(info.getLang().ONLINESTATUS_DND.format(info.getAuthor().getAsMention()));
			} else if(info.getArguments()[0].equalsIgnoreCase("invisible")) {
				info.getChannel().getJDA().getPresence().setStatus(OnlineStatus.INVISIBLE);
				builder.setDescription(info.getLang().ONLINESTATUS_OFFLINE.format(info.getAuthor().getAsMention()));
			} else {
				return false;
			}
			info.getChannel().sendMessage(builder.build()).queue();
			
			return true;
		}
		return false;
	}
	
	@RequireOwner
	@Command(name = "botprefix", description = "BOTPREFIX_DESC", usage = ".")
	public boolean setPrefix(CommandInfo info) {
		if(info.getArguments().length == 1) {
			this.getBot().setSetting(-1, "CommandPrefix", info.getArguments()[0]);
			
			info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().COMMAND_PREFIX_SET.format(info.getAuthor().getAsMention(), info.getArguments()[0])).build()).queue();
			return true;
		}
		return false;
	}
	
	@RequireOwner
	@Command(name = "addModuleAlias", description = "ADDMODULEALIAS_DESC", usage = "<command> <moduleName>")
	public boolean addModuleAlias(CommandInfo info) {
		if(info.getArguments().length == 2) {
			ICommand command = this.getBot().getCommandMap().getCommand(info.getArguments()[0]);
			
			if(command == null) {
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().COMMAND_NOT_FOUND.format(info.getAuthor().getAsMention(), info.getArguments()[0]))
						.setColor(Color.RED)
						.build()
						).queue();
				return true;
			}
				
			
			IModule moduleIntf = this.getBot().getModules().get(info.getArguments()[1]);
			if(moduleIntf != null && !(moduleIntf instanceof ModuleAlias)) {
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().NEED_ALIAS_MODULE.format(info.getAuthor().getAsMention(), info.getArguments()[1]))
						.setColor(Color.RED)
						.build()
						).queue();
				return true;
			}
			
			this.getBot().addModuleAlias(command.getName(), info.getArguments()[1]);
			this.getBot().getDatabase().addModuleAlias(command.getName(), info.getArguments()[1]);
			info.getChannel().sendMessage(
					new EmbedBuilder()
					.setDescription(info.getLang().ALIAS_MODULE_SUCCESS.format(command.getName(), info.getArguments()[1]))
					.setColor(Color.GREEN)
					.build()
					).queue();
			return true;
		}
		return false;
	}
	
	@RequireOwner
	@Command(name = "removeModuleAlias", description = "REMOVEMODULEALIAS_DESC", usage = "<command> <moduleName>")
	public boolean removeModuleAlias(CommandInfo info) {
		if(info.getArguments().length == 2) {
			ICommand command = this.getBot().getCommandMap().getCommand(info.getArguments()[0]);
			
			if(command == null) {
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().COMMAND_NOT_FOUND.format(info.getAuthor().getAsMention()))
						.setColor(Color.RED)
						.build()
						).queue();
				return true;
			}
			
			IModule moduleIntf = this.getBot().getModules().get(info.getArguments()[1]);
			if(moduleIntf != null && !(moduleIntf instanceof ModuleAlias)) {
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().NEED_ALIAS_MODULE.format(info.getAuthor().getAsMention(), info.getArguments()[1]))
						.setColor(Color.RED)
						.build()
						).queue();
				return true;
			}
			
			ModuleAlias moduleObj = (ModuleAlias) moduleIntf;
			if(!moduleObj.getAliases().contains(command)) {
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().COMMAND_IN_ALIAS_MODULE_NOT_FOUND.format(info.getAuthor().getAsMention(), command.getName(), info.getArguments()[1]))
						.setColor(Color.RED)
						.build()
						).queue();
				return true;
			}
			
			this.getBot().removeModuleAlias(command.getName(), info.getArguments()[1]);
			this.getBot().getDatabase().removeModuleAlias(command.getName(), info.getArguments()[1]);
			info.getChannel().sendMessage(
					new EmbedBuilder()
					.setDescription(info.getLang().ALIAS_MODULE_SUCCESS.format(command.getName(), info.getArguments()[1]))
					.setColor(Color.GREEN)
					.build()
					).queue();
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "wowclassselection", description = "Admin Command")
	public boolean classSelectionEmbed(CommandInfo info) {
		EmbedBuilder classSelectionBuilder = new EmbedBuilder()
				.setTitle("Klassenauswahl")
				.setDescription("Du kannst die Reaktionen benutzen, um deine Klassen auszuwählen!");
		
		info.getChannel().sendMessage(classSelectionBuilder.build()).queue(msg -> {
			HashMap<Role, Long> roleEmotes = this.getBot().getConnectedGuild(info.getGuild().get()).getRoleEmotes();
			
			ArrayList<Long> roleIdsList = new ArrayList<Long>();
			
			for(Role role : roleEmotes.keySet()) {
				Long emoteId = roleEmotes.get(role);
				Emote emote = info.getGuild().get().getEmoteById(emoteId);
				
				if(emote.getName().startsWith("class_")) {
					msg.addReaction(emote).queue();
					roleIdsList.add(role.getIdLong());
				}
			}
			
			Long[] roleIds = roleIdsList.toArray(new Long[roleIdsList.size()]);
			
			this.getBot().getConnectedGuild(info.getGuild().get()).addRoleAssignMessage(msg.getIdLong(), roleIds);
			this.getBot().getDatabase().addRoleAssignMessage(info.getGuild().get().getIdLong(), msg.getIdLong(), roleIds);
		});
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "wowfactionselection", description = "Admin Command")
	public boolean factionSelectionEmbed(CommandInfo info) {
		EmbedBuilder classSelectionBuilder = new EmbedBuilder()
				.setTitle("Fraktionenauswahl")
				.setDescription("Du kannst die Reaktionen benutzen, um deine Fraktion auszuwählen!");
		
		info.getChannel().sendMessage(classSelectionBuilder.build()).queue(msg -> {
			HashMap<Role, Long> roleEmotes = this.getBot().getConnectedGuild(info.getGuild().get()).getRoleEmotes();
			
			ArrayList<Long> roleIdsList = new ArrayList<Long>();
			
			for(Role role : roleEmotes.keySet()) {
				Long emoteId = roleEmotes.get(role);
				Emote emote = info.getGuild().get().getEmoteById(emoteId);
				
				if(emote.getName().startsWith("faction_")) {
					msg.addReaction(emote).queue();
					roleIdsList.add(role.getIdLong());
				}
			}
			
			Long[] roleIds = roleIdsList.toArray(new Long[roleIdsList.size()]);
			
			this.getBot().getConnectedGuild(info.getGuild().get()).addRoleAssignMessage(msg.getIdLong(), roleIds);
			this.getBot().getDatabase().addRoleAssignMessage(info.getGuild().get().getIdLong(), msg.getIdLong(), roleIds);
		});
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = Permission.ADMINISTRATOR)
	@Command(name = "listRoleEmotes", description = "LISTROLEEMOTES_DESC")
	public boolean listRoleEmotes(CommandInfo info) {
		ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());

		BotEmbedBuilder builder = new BotEmbedBuilder().setTitle(info.getLang().LIST_OF_ROLE_EMOTES.format());
		
		for(Entry<Role, Long> entry : cGuild.getRoleEmotes().entrySet()) {
			builder.appendDescription("- *" + entry.getKey().getName() + "*: " + info.getGuild().get().getEmoteById(entry.getValue()).getId() + "\n");
		}
		
		builder.sendMessage(info.getChannel());
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "setRoleEmote", description = "SETROLEEMOTE_DESC", usage = "<role name> <emote name>")
	public boolean setRoleEmote(CommandInfo info) {
		if(info.getArguments().length == 2) {
			String roleName = info.getArguments()[0];
			String emoteName = info.getArguments()[1];
			Long emoteId = -1L;
			
			for(Emote emote : info.getGuild().get().getEmotes()) {
				if(emote.getName().equalsIgnoreCase(emoteName)) {
					emoteId = emote.getIdLong();
				}
			}
			
			if(info.getGuild().get().getEmoteById(emoteId) != null) {
				Role role = info.getGuild().get().getRolesByName(roleName, true).get(0);
				
				this.getBot().getDatabase().setEmote(info.getGuild().get().getIdLong(), emoteName, emoteId);
				this.getBot().getDatabase().setRoleEmote(info.getGuild().get().getIdLong(), role.getIdLong(), emoteName);
				
				this.getBot().getConnectedGuild(info.getGuild().get()).setRoleEmote(role, emoteId);
				
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().SET_ROLE_EMOTE.format(info.getAuthor().getAsMention(), role.getAsMention(), emoteName, emoteId))
						.build()
						).queue();
			} else {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().REMOVED_ROLE_EMOTE.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()).queue();
			}
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "removeRoleEmote", description = "REMOVEROLEEMOTE_DESC", usage = "<role name>")
	public boolean removeRoleEmote(CommandInfo info) {
		if(info.getArguments().length == 1) {
			String roleName = info.getArguments()[0];
			
			Role role = info.getGuild().get().getRolesByName(roleName, true).get(0);
			
			this.getBot().getDatabase().removeRoleEmote(info.getGuild().get().getIdLong(), role.getIdLong());
			
			info.getChannel().sendMessage(
					new EmbedBuilder()
					.setDescription(info.getLang().REMOVED_ROLE_EMOTE.format(info.getAuthor().getAsMention(), role.getAsMention()))
					.build()
					).queue();
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@Command(name = "emoteid", description = "dev command", usage = ".emoteid <alias>")
	public boolean getReactionId(CommandInfo info) {
		if(info.getArguments().length == 1) {
			for(Emote emote : info.getGuild().get().getEmotes()) {
				if(emote.getName().equalsIgnoreCase(info.getArguments()[0])) {
					info.getChannel().sendMessage("Id des Emotes: " + emote.getIdLong()).queue();
					return true;
				}
			}
			info.getChannel().sendMessage("Kein Emote gefunden.").queue();
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@RequireBotUserPermission(guildPermission = {Permission.NICKNAME_CHANGE})
	@Command(name = "setNickname", description = "SETNICKNAME_DESC", usage = "<nickname>")
	public boolean setNickname(CommandInfo info) {
		if(info.getArguments().length == 1) {
			info.getGuild().get().getController().setNickname(info.getGuild().get().getSelfMember(), info.getArguments()[0]).queue(
					success -> info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().NICKNAME_CHANGED.format(info.getAuthor().getAsMention(), info.getArguments()[0])).build()).queue(),
					failure -> info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().NICKNAME_CHANGE_FAILURE.format(info.getAuthor().getAsMention())).build()).queue());
			return true;
		}
		return false;
	}
	
	@RequireOwner
	@Command(name = "setName", description = "SETNAME_DESC", usage = "<name>")
	public boolean setName(CommandInfo info) {
		if(info.getArguments().length == 1) {
			info.getChannel().getJDA().getSelfUser().getManager().setName(info.getArguments()[0]).queue(
					success -> info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().NAME_CHANGED.format(info.getAuthor().getAsMention(), info.getArguments()[0])).build()).queue(),
					failure -> info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().NAME_CHANGE_FAILURE.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()).queue());
			return true;
		}
		return false;
	}
	
	@RequireOwner
	@Command(name = "setAvatar", description = "SETAVATAR_DESC", usage = "<URL>")
	public boolean setAvatar(CommandInfo info) {
		if(info.getArguments().length == 1) {
			try {
				URL url = new URL(info.getArguments()[0]);
				
				try {
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestProperty("User-Agent", "DiscordBot");
					InputStream in = new BufferedInputStream(conn.getInputStream());
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					
					int n;
					byte[] data = new byte[4096];
					while(-1 != (n = in.read(data))) {
						baos.write(data, 0, n);
					}
					
					info.getChannel().getJDA().getSelfUser().getManager().setAvatar(Icon.from(baos.toByteArray())).queue(
							success -> info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().AVATAR_CHANGE.format()).setColor(Color.GREEN).build()).queue(),
							failure -> info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().AVATAR_CHANGE_FAILURE.format()).setColor(Color.RED).build()).queue());
				} catch (IOException e) {
					e.printStackTrace();
					info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().IOEXCEPTION_PICTURE.format()).setColor(Color.RED).build()).queue();
				}
			} catch (MalformedURLException e) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().URLEXCEPTION.format()).setColor(Color.RED).build()).queue();
			}
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "logserver", description = "LOGSERVER_DESC")
	public boolean toggleLogging(CommandInfo info) {
		Setting setting = this.getBot().getSetting(info.getGuild().get().getIdLong(), "Logging");
		if(setting != null && setting.isTrue()) {
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "Logging", 0);
			info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().LOGGING_DISABLED.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()).queue();
		} else {
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "Logging", 1);
			info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().LOGGING_ENABLED.format(info.getAuthor().getAsMention())).setColor(Color.GREEN).build()).queue();
		}
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@RequireBotUserPermission(channelPermission = {})
	@Command(name = "log", description = "LOG_DESC")
	public boolean toggleLoggingChannel(CommandInfo info) {
		if(info.getArguments().length == 1) {
			if(this.getBot().getEventTypes().contains(info.getArguments()[0])) {
				TextChannel channel = this.getBot().getConnectedGuild(info.getGuild().get()).getLogChannels().get(info.getArguments()[0]);
				
				if(channel != null && info.getChannel().getName().equals(channel.getName())) {
					this.getBot().getConnectedGuild(info.getGuild().get()).getLogChannels().remove(info.getArguments()[0]);
					this.getBot().getDatabase().removeLogChannel(info.getGuild().get().getIdLong(), info.getArguments()[0]);
					info.getChannel().sendMessage(
							new EmbedBuilder()
							.setDescription(info.getLang().LOGGING_EVENT_DISABLED.format(info.getAuthor().getAsMention(), info.getArguments()[0]))
							.setColor(Color.RED)
							.build()).queue();
					return true;
				}
				
				this.getBot().getConnectedGuild(info.getGuild().get()).getLogChannels().put(info.getArguments()[0], ((TextChannel)info.getChannel()));
				this.getBot().getDatabase().setLogChannel(info.getGuild().get().getIdLong(), info.getChannel().getIdLong(), info.getArguments()[0]);
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().LOGGING_EVENT_SET.format(info.getAuthor().getAsMention(), info.getArguments()[0], ((TextChannel) info.getChannel()).getAsMention()))
						.build()).queue();
				return true;
			}
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@RequireBotUserPermission(channelPermission = {})
	@Command(name = "logevents", description = "LOGEVENTS_DESC")
	public boolean printLogEvents(CommandInfo info) {
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < Bot.getInstance().getEventTypes().size(); i++) {
			if(i == 0) {
				builder.append(Bot.getInstance().getEventTypes().get(i));
				continue;
			}
			
			builder.append(", " + Bot.getInstance().getEventTypes().get(i));
		}
		info.getChannel().sendMessage(info.getLang().LOGGING_EVENTS.format(builder.toString())).queue();
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@RequireBotUserPermission(guildPermission = {Permission.MESSAGE_MANAGE})
	@Command(name = "delmsgoncmd", description = "DELMSGONCMD_DESC")
	public boolean delMsgOnCmd(CommandInfo info) {
		Setting setting = this.getBot().getSetting(info.getGuild().get().getIdLong(), "DeleteCommandMessages");
		
		if(setting.isTrue()) {
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "DeleteCommandMessages", 0);
			info.getChannel().sendMessage(
					new EmbedBuilder().setDescription(info.getLang().DEL_CMDS_AFTER_EXEC_DIS.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
					).queue();
		} else {
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "DeleteCommandMessages", 1);
			info.getChannel().sendMessage(
					new EmbedBuilder().setDescription(info.getLang().DEL_CMDS_AFTER_EXEC_EN.format(info.getAuthor().getAsMention())).setColor(Color.GREEN).build()
					).queue();
		}
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.MANAGE_ROLES})
	@RequireBotUserPermission(guildPermission = {Permission.MANAGE_ROLES})
	@Command(name = "setMuteRole", description = "SETMUTEROLE_DESC", usage = "Muted")
	public boolean setMuteRole(CommandInfo info) {
		if(info.getArguments().length == 1) {
			Role role = info.getGuild().get().getRolesByName(info.getArguments()[0], true).get(0);
			
			if(role == null) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().ROLE_NOT_FOUND.format(info.getAuthor().getAsMention(), info.getArguments()[0])).setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			if(info.getGuild().get().getSelfMember().canInteract(role)) {
				this.getBot().getConnectedGuild(info.getGuild().get()).setMuteRole(role);
				this.getBot().setSetting(info.getGuild().get().getIdLong(), "MuteRoleId", role.getIdLong());
				
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().MUTE_ROLE_SET.format(info.getAuthor().getAsMention(), role.getAsMention())).build()).queue();
			} else {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().BOT_CANNOT_INTERACT_ROLE.format(info.getAuthor().getAsMention(), info.getArguments()[0])).setColor(Color.RED).build()
						).queue();
			}
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = Permission.ADMINISTRATOR)
	@Command(name = "lang", description = "LANG_DESC", usage = "en_EN")
	public boolean setLang(CommandInfo info) {
		if(info.getArguments().length == 1) {
			String langCode = info.getArguments()[0];
			
			Language lang = this.getBot().getLanguage(langCode);
			
			if(lang == null) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().LANG_NOT_FOUND.format(info.getAuthor().getAsMention())).build()).queue();
				return true;
			}
			
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "Language", langCode);
			this.getBot().getConnectedGuild(info.getGuild().get()).setLang(lang);
			info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().LANG_SET.format(info.getAuthor().getAsMention(), langCode)).build()).queue();
		} else {
			EmbedBuilder builder = new EmbedBuilder();
			builder.setTitle(info.getLang().TITLE_LANG.format());
			
			for(String langCode : this.getBot().getLanguages().keySet()) {
				builder.addField(langCode, "`.lang " + langCode + "`", true);
			}
			
			info.getChannel().sendMessage(builder.build()).queue();
		}
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.BAN_MEMBERS})
	@RequireBotUserPermission(guildPermission = {Permission.BAN_MEMBERS})
	@Command(name = "ban", description = "BAN_DESC", usage = "@User [\"Reason\"]")
	public boolean ban(CommandInfo info) {
		if(info.getArguments().length > 0) {
			if(info.getMessage().getMentionedUsers().size() > 0) {
				User bannedUser = info.getMessage().getMentionedUsers().get(0);
				Member bannedMember = info.getGuild().get().getMember(bannedUser);
				
				RestAction<Message> action = info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().CMD_BANNED_USER.format(info.getAuthor().getAsMention(), bannedUser.getName(), bannedUser.getDiscriminator())).build());
				
				if(info.getGuild().get().getSelfMember().canInteract(bannedMember)) {
					if(info.getArguments().length == 2) {
						info.getGuild().get().getController().ban(bannedMember, 0, info.getArguments()[1]).queue(success -> action.queue(msg -> msg.delete().queueAfter(5L, TimeUnit.SECONDS)));
					} else {
						info.getGuild().get().getController().ban(bannedMember, 0).queue(success -> action.queue(msg -> msg.delete().queueAfter(5L, TimeUnit.SECONDS)));
					}
				} else {
					info.getChannel().sendMessage(
							new EmbedBuilder().setDescription(info.getLang().BOT_LOWER_CANNOT_BAN.format(info.getAuthor().getAsMention(), bannedMember.getAsMention())).setColor(Color.RED).build()
							).queue();
					return true;
				}
				
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.BAN_MEMBERS})
	@RequireBotUserPermission(guildPermission = {Permission.BAN_MEMBERS})
	@Command(name = "unban", description = "UNBAN_DESC", usage = ".unban 205798890014900224")
	public boolean unban(CommandInfo info) {
		if(info.getArguments().length > 0) {
			if(info.getArguments()[0].matches("(\\d)+")) {
				info.getGuild().get().getController().unban(info.getArguments()[0]).queue(success -> info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().CMD_UNBANNED_USER.format(info.getAuthor().getAsMention())).setColor(Color.GREEN).build()
						).queue()
				, failure -> info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().BOT_CANNOT_UNBAN.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue());
				return true;
			} else {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().NEED_ID.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.KICK_MEMBERS, Permission.VOICE_MUTE_OTHERS})
	@RequireBotUserPermission(guildPermission = {Permission.MANAGE_ROLES, Permission.VOICE_MUTE_OTHERS})
	@Command(name = "mute", description = "MUTE_DESC", usage = {"[TimeDiff in eg. 30h] @User", "205798890014900224"})
	public boolean mute(CommandInfo info) {
		//check if time is specified
		if(info.getArguments().length == 2) {
			//get time and convert it to date by string
			String timeDiff = info.getArguments()[0];
			Date muteTime = DateUtil.stringToDate(timeDiff);
			
			//check if given time isnt false
			if(muteTime.getTime() == 0L) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().TIMEDIFF_FALSE.format(info.getAuthor().getAsMention())).addField("Format", "1h30m", false).setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			//getting user by mention or id
			User muteUser = null;
			if(info.getMessage().getMentionedUsers().size() == 1) {
				muteUser = info.getMessage().getMentionedUsers().get(0);
			} else {
				muteUser = info.getMessage().getJDA().getUserById(info.getArguments()[0]);
			}
			
			//check if user couldnt be found
			if(muteUser == null) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().USER_NOT_FOUND.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			//get guild member by user
			Member member = info.getGuild().get().getMember(muteUser);
			
			//check if member couldnt be found
			if(member == null) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().USER_NOT_FOUND.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			//check if bot can interact with member
			if(!info.getGuild().get().getSelfMember().canInteract(member)) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().BOT_LOWER_CANNOT_MUTE.format(info.getAuthor().getAsMention(), member.getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			//get mute role to add user to role
			Role muteRole = this.getBot().getConnectedGuild(info.getGuild().get()).getMuteRole();
			
			//check if mute role is set
			if(muteRole != null) {
				//add member to role
				info.getGuild().get().getController().addRolesToMember(member, muteRole).queue(success -> {
					//mute member from voice chat
					info.getGuild().get().getController().setMute(member, true).queue();
					//send successful message
					info.getChannel().sendMessage(
							new EmbedBuilder().setDescription(info.getLang().CMD_USER_MUTED.format(info.getAuthor().getAsMention(), member.getAsMention()))
							.setColor(Color.GREEN).build()
							).queue(msg -> msg.delete().queueAfter(5L, TimeUnit.SECONDS));
					
					//log to logging channel
					Bot.getInstance().getLogManager().logToChannel(info.getGuild().get(), EventType.USER_MUTED, 
							new EmbedBuilder()
							.setTitle(info.getLang().EVENT_USER_MUTED.format())
							.setDescription(member.getUser().getName() + "#" + member.getUser().getDiscriminator() + " | " + member.getUser().getId())
							.addField("Moderator", info.getAuthor().getName() + "#" + info.getAuthor().getDiscriminator(), false)
							.build());
					
					//add mute to database
					this.getBot().getDatabase().addMute(info.getGuild().get().getIdLong(), member.getUser().getIdLong(), info.getAuthor().getIdLong(), muteTime.getTime() + System.currentTimeMillis(), System.currentTimeMillis(), null);
				}, failure -> {
					info.getChannel().sendMessage(
							new EmbedBuilder().setDescription(info.getLang().MUTE_FAILURE.format(info.getAuthor().getAsMention(), member.getAsMention())).setColor(Color.RED).build()
							).queue();
					failure.printStackTrace();
				});
			} else {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().NO_MUTE_ROLE_SET.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
			}
			return true;
		} else if(info.getArguments().length == 1) {
			//getting user by mention or id
			User muteUser = null;
			if(info.getMessage().getMentionedUsers().size() == 1) {
				muteUser = info.getMessage().getMentionedUsers().get(0);
			} else {
				muteUser = info.getMessage().getJDA().getUserById(info.getArguments()[0]);
			}
			
			//check if user couldnt be found
			if(muteUser == null) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().USER_NOT_FOUND.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			//get member by user
			Member member = info.getGuild().get().getMember(muteUser);
			
			//check if member couldnt be found
			if(member == null) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().USER_NOT_FOUND.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			//check if bot can interact with member
			if(!info.getGuild().get().getSelfMember().canInteract(member)) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().BOT_LOWER_CANNOT_MUTE.format(info.getAuthor().getAsMention(), member.getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			Role muteRole = this.getBot().getConnectedGuild(info.getGuild().get()).getMuteRole();
			
			if(muteRole != null) {
				info.getGuild().get().getController().addRolesToMember(member, muteRole).queue(success -> {
					info.getGuild().get().getController().setMute(member, true).queue();
					
					info.getChannel().sendMessage(
							new EmbedBuilder().setDescription(info.getLang().CMD_USER_MUTED.format(info.getAuthor().getAsMention(), member.getAsMention()))
							.setColor(Color.GREEN).build()
							).queue(msg -> msg.delete().queueAfter(5L, TimeUnit.SECONDS));
					
					Bot.getInstance().getLogManager().logToChannel(info.getGuild().get(), EventType.USER_MUTED, 
							new EmbedBuilder()
							.setTitle(info.getLang().EVENT_USER_MUTED.format())
							.setDescription(member.getUser().getName() + "#" + member.getUser().getDiscriminator() + " | " + member.getUser().getId())
							.addField("Moderator", info.getAuthor().getName() + "#" + info.getAuthor().getDiscriminator(), false)
							.build());
					
					
				}, failure -> {
					info.getChannel().sendMessage(
							new EmbedBuilder().setDescription(info.getLang().MUTE_FAILURE.format(info.getAuthor().getAsMention(), member.getAsMention())).setColor(Color.RED).build()
							).queue();
					failure.printStackTrace();
				});
			} else {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().NO_MUTE_ROLE_SET.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
			}
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.KICK_MEMBERS, Permission.VOICE_MUTE_OTHERS})
	@RequireBotUserPermission(guildPermission = {Permission.MANAGE_ROLES, Permission.VOICE_MUTE_OTHERS})
	@Command(name = "unmute", description = "UNMUTE_DESC", usage = {".unmute @User", "205798890014900224"})
	public boolean unmute(CommandInfo info) {
		if(info.getArguments().length == 1) {
			User muteUser = null;
			if(info.getMessage().getMentionedUsers().size() == 1) {
				muteUser = info.getMessage().getMentionedUsers().get(0);
			} else {
				muteUser = info.getMessage().getJDA().getUserById(info.getArguments()[0]);
			}
			
			if(muteUser == null) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().USER_NOT_FOUND.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			Member member = info.getGuild().get().getMember(muteUser);
			
			if(!info.getGuild().get().getSelfMember().canInteract(member)) {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().BOT_LOWER_CANNOT_UNMUTE.format(info.getAuthor().getAsMention(), member.getAsMention())).setColor(Color.RED).build()
						).queue();
				return true;
			}
			
			info.getGuild().get().getController().setMute(member, false).queue(success -> {/*nothing to do */}, failure -> {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().UNMUTE_VOICE_FAILURE.format(info.getAuthor().getAsMention(), member.getAsMention())).setColor(Color.RED).build()
						).queue();
				failure.printStackTrace();
			});
			
			Role muteRole = this.getBot().getConnectedGuild(info.getGuild().get()).getMuteRole();
			if(muteRole != null) {
				info.getGuild().get().getController().removeRolesFromMember(member, muteRole).queue(success -> {
					info.getChannel().sendMessage(
							new EmbedBuilder().setDescription(info.getLang().CMD_USER_UNMUTED.format(info.getAuthor().getAsMention()))
							.setColor(Color.GREEN).build()
							).queue(msg -> msg.delete().queueAfter(5L, TimeUnit.SECONDS));
					
					Bot.getInstance().getLogManager().logToChannel(info.getGuild().get(), EventType.USER_MUTED, 
							new EmbedBuilder()
							.setTitle(info.getLang().EVENT_USER_UNMUTED.format())
							.setDescription(member.getUser().getName() + "#" + member.getUser().getDiscriminator() + " | " + member.getUser().getId())
							.addField("Moderator", info.getAuthor().getName() + "#" + info.getAuthor().getDiscriminator(), false)
							.build());
					
					Bot.getInstance().getDatabase().removeMute(info.getGuild().get().getIdLong(), member.getUser().getIdLong());
				}, failure -> {
					info.getChannel().sendMessage(
							new EmbedBuilder().setDescription(info.getLang().UNMUTE_TEXT_FAILURE.format(info.getAuthor().getAsMention(), member.getAsMention())).setColor(Color.RED).build()
							).queue();
					failure.printStackTrace();
				});
			} else {
				info.getChannel().sendMessage(
						new EmbedBuilder().setDescription(info.getLang().NO_MUTE_ROLE_SET.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()
						).queue();
			}
		}
		return false;
	}
	
	@RequireGuild
	@Command(name = "mutetime", description = "MUTETIME_DESC")
	public boolean muteTime(CommandInfo info) {
		MuteInfo muteInfo = Bot.getInstance().getDatabase().getMute(info.getGuild().get().getIdLong(), info.getAuthor().getIdLong());
		Role muteRole = this.getBot().getConnectedGuild(info.getGuild().get()).getMuteRole();
		
		if(muteInfo == null) {
			if(muteRole != null) {
				if(info.getGuildMember().get().getRoles().contains(muteRole)) {
					info.getChannel().sendMessage(new EmbedBuilder()
							.setDescription(info.getLang().MUTETIME_PERMANENT.format(info.getAuthor().getAsMention())).build()).queue();
				} else {
					info.getChannel().sendMessage(new EmbedBuilder()
							.setDescription(info.getLang().MUTETIME_NOT_MUTED.format(info.getAuthor().getAsMention())).build()).queue();
				}
			}
			return true;
		}
		
		long current = System.currentTimeMillis();
		
		if(muteInfo.getExpirationDate() < current) {
			Bot.getInstance().getDatabase().removeMute(muteInfo.getGuildId(), muteInfo.getUserId());
			Member member = info.getGuild().get().getMemberById(muteInfo.getUserId());
			info.getGuild().get().getController().setMute(member, false).queue();
			info.getGuild().get().getController().removeRolesFromMember(member, this.getBot().getConnectedGuild(info.getGuild().get()).getMuteRole()).queue();
			Bot.getInstance().getLogger().info("User " + info.getAuthor().getName() + "#" + info.getAuthor().getDiscriminator() + " [ID: " + info.getAuthor().getId() + "] was unmuted");
			return true;
		}
		
		long remainingTime = muteInfo.getExpirationDate() - current;
		
		info.getChannel().sendMessage(new EmbedBuilder()
				.setDescription(info.getLang().MUTETIME_TEMP.format(info.getAuthor().getAsMention(), DateUtil.getRemainingTimeString(remainingTime))).build()).queue();
		
		return true;
	}
	
	@RequireGuild
	@Command(name = "warnlist", description = "WARNLIST_DESC", usage = {"@User", "205798890014900224"}, aliases = {"warns", "warnlog"})
	public boolean getWarnlist(CommandInfo info) {
		User user = null;
		if(info.getArguments().length == 1) {
			if(!info.getGuildMember().get().hasPermission(Permission.KICK_MEMBERS)) {
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().NEED_SERVER_PERMISSION.format(Permission.KICK_MEMBERS))
						.setColor(Color.RED)
						.build()
						).queue();
				return true;
			}
			
			if(info.getMessage().getMentionedUsers().size() == 1) {
				user = info.getMessage().getMentionedUsers().get(0);
			} else if(info.getArguments()[0].matches("(\\d){18}")) {
				user = info.getChannel().getJDA().getUserById(info.getArguments()[0]);
			} else {
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().NEED_USER.format())
						.setColor(Color.RED)
						.build()
						).queue();
				return true;
			}
		} else {
			user = info.getAuthor();
		}
		
		int page = 0;
		if(info.getArguments().length == 2) {
			try {
				page = Integer.parseInt(info.getArguments()[1]) - 1;
			} catch(NumberFormatException e) {
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().NEED_NATURAL_NUMBER.format())
						.setColor(Color.RED)
						.build()
						).queue();
				return true;
			}
		}
		
		int offset = page * 10;
		
		if(user == null) {
			info.getChannel().sendMessage(
					new EmbedBuilder()
					.setDescription(info.getLang().USER_NOT_FOUND.format(info.getAuthor().getAsMention()))
					.setColor(Color.RED)
					.build()
					).queue();
			return true;
		}
		
		List<Warn> warns = this.getBot().getDatabase().getWarns(info.getGuild().get(), user.getIdLong(), offset, 10);
		
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setTitle(info.getLang().TITLE_WARNLOG.format(user.getName(), user.getDiscriminator()));
		
		Format format = new SimpleDateFormat("dd.MM.yyyy");
		Format timeFormat = new SimpleDateFormat("HH:mm");
		
		for(Warn warn : warns) {
			this.getBot().getLogger().info("WARN: " + warn.getReason());
			Date date = new Date(warn.getIssueDate());
			embedBuilder.addField(info.getLang().WARNLOG_FIELD.format(warn.getId(), format.format(date), timeFormat.format(date), (warn.getStaffUser().isPresent() ? warn.getStaffUser().get().getUser().getName() + "#" + warn.getStaffUser().get().getUser().getDiscriminator() : info.getLang().UNKNOWN.format(warn.getStaffId()))), warn.getReason(), false);
		}
		
		int rows = 0;
		
		if(warns.size() == 0) {
			embedBuilder.setDescription(info.getLang().NO_WARNS.format());
		} else {
			rows = warns.get(0).getRows();
		}
		
		embedBuilder.setFooter((page + 1) + "/" + (rows < 10 ? 1 : (rows/10)), null);
		info.getChannel().sendMessage(embedBuilder.build()).queue();
		
		return true;
	}
	
	public boolean getWarn(CommandInfo info) {
		return false;
	}
	
	@Command(name = "warn", description = "WARN_DESC")
	public boolean warnUser(CommandInfo info) {
		if(info.getArguments().length == 2) {
			
			User warnUser = null;
			if(info.getMessage().getMentionedUsers().size() == 1) {
				warnUser = info.getMessage().getMentionedUsers().get(0);
			} else {
				warnUser = info.getChannel().getJDA().getUserById(info.getArguments()[0]);
			}
			
			if(warnUser == null) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().USER_NOT_FOUND.format(info.getAuthor().getAsMention())).build()).queue();
				return true;
			}
			
			this.getBot().getDatabase().addWarn(info.getGuild().get().getIdLong(), warnUser.getIdLong(), info.getAuthor().getIdLong(), System.currentTimeMillis(), info.getArguments()[1]);
			warnUser.openPrivateChannel().queue(textChannel -> {
				textChannel.sendMessage(new EmbedBuilder()
						.setDescription(info.getLang().WARN_NOTIFICATION.format(info.getGuild().get().getName()))
						.addField("Moderator", info.getAuthor().getName() + "#" + info.getAuthor().getDiscriminator(), false)
						.addField(info.getLang().REASON.format(), info.getArguments()[1], false)
						.setColor(Color.RED)
						.build()
						).queue();
			});
			info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().WARN_SUCCESS.format(info.getAuthor().getAsMention(), warnUser.getAsMention())).build()).queue();
			return true;
		}
		info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().WARN_NEED_ARGS.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()).queue();
		return true;
	}
	
	@Command(name = "rmwarn", description = "REMOVEWARN_DESC")
	public boolean removeWarn(CommandInfo info) {
		if(info.getArguments().length == 1) {
			int id = 0;
			
			try {
				id = Integer.parseInt(info.getArguments()[0]);
			} catch(NumberFormatException e) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().NEED_NATURAL_NUMBER.format(info.getAuthor().getAsMention())).build()).queue();
			}
			
			this.getBot().getDatabase().removeWarn(info.getGuild().get().getIdLong(), id);
			info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().WARN_REMOVED.format(info.getAuthor().getAsMention(), id)).build()).queue();
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@Command(name = "lsar", description = "LSAR_DESC")
	public boolean listSelfAssignableRoles(CommandInfo info) {
		StringBuilder messageBuilder = new StringBuilder();
		
		int i = 0;
		for(Role role : Bot.getInstance().getConnectedGuild(info.getGuild().get()).getSelfAssignableRoles()) {
			messageBuilder.append((i != 0 ? ", " : "") + role.getName());
			i++;
		}
		
		info.getChannel().sendMessage(info.getLang().LIST_SELFASSIGNABLE_ROLES_TITLE.format(Bot.getInstance().getConnectedGuild(info.getGuild().get()).getSelfAssignableRoles().size())).queue();
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.MANAGE_ROLES})
	@RequireBotUserPermission(guildPermission = {Permission.MANAGE_ROLES})
	@Command(name = "asar", description = "ASAR_DESC", usage = {"@Role", "Role"})
	public boolean addSelfAssignableRole(CommandInfo info) {
		if(info.getArguments().length == 1) {
			Role role = null;
			
			if(info.getMessage().getMentionedRoles().size() == 1) {
				role = info.getMessage().getMentionedRoles().get(0);
			} else {
				role = info.getGuild().get().getRolesByName(info.getArguments()[0], true).get(0);
			}
			
			if(role == null) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().ROLE_NOT_FOUND.format(info.getArguments()[0])).setColor(Color.RED).build()).queue();
				return true;
			}
			
			if(!info.getGuild().get().getSelfMember().canInteract(role)) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().BOT_CANNOT_INTERACT_ROLE.format()).setColor(Color.RED).build()).queue();
				return true;
			}
			if(!info.getGuildMember().get().canInteract(role)) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().ASAR_UNASSIGNABLE.format()).setColor(Color.RED).build()).queue();
				return true;
			}
			
			if(!Bot.getInstance().getConnectedGuild(info.getGuild().get()).containsSelfAssignableRole(role)) {
				Bot.getInstance().getDatabase().addSelfAssignableRole(info.getGuild().get().getIdLong(), role.getIdLong());
				Bot.getInstance().getConnectedGuild(info.getGuild().get()).addSelfAssignableRole(role);
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().ASAR_NOW_ASSIGNABLE.format()).setColor(Color.GREEN).build()).queue();
			} else {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().ASAR_ALREADY_ASSIGNABLE.format()).setColor(Color.RED).build()).queue();
			}
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.MANAGE_ROLES})
	@RequireBotUserPermission(guildPermission = {Permission.MANAGE_ROLES})
	@Command(name = "rsar", description = "RSAR_DESC", usage = {"@Role", "Role"})
	public boolean removeSelfAssignableRole(CommandInfo info) {
		if(info.getArguments().length == 1) {
			Role role = null;
			
			if(info.getMessage().getMentionedRoles().size() == 1) {
				role = info.getMessage().getMentionedRoles().get(0);
			} else {
				role = info.getGuild().get().getRolesByName(info.getArguments()[0], true).get(0);
			}
			
			if(role == null) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().ROLE_NOT_FOUND.format(info.getArguments()[0])).setColor(Color.RED).build()).queue();
				return true;
			}
			
			if(!info.getGuild().get().getSelfMember().canInteract(role)) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().BOT_CANNOT_INTERACT_ROLE.format()).setColor(Color.RED).build()).queue();
				return true;
			}
			if(!info.getGuildMember().get().canInteract(role)) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().ASAR_UNASSIGNABLE.format()).setColor(Color.RED).build()).queue();
				return true;
			}
			
			if(Bot.getInstance().getConnectedGuild(info.getGuild().get()).containsSelfAssignableRole(role)) {
				Bot.getInstance().getConnectedGuild(info.getGuild().get()).removeSelfAssignableRole(role);
				Bot.getInstance().getDatabase().removeSelfAssignableRole(info.getGuild().get().getIdLong(), role.getIdLong());
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().ASAR_NOW_UNASSIGNABLE.format(role.getName())).setColor(Color.RED).build()).queue();
			} else {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().ASAR_ALREADY_UNASSIGNABLE.format(role.getName())).setColor(Color.RED).build()).queue();
			}
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@Command(name = "iam", description = "IAM_DESC", usage = "Role")
	public boolean addSelfAssignableRoleToSelf(CommandInfo info) {
		if(info.getArguments().length == 1) {
			Role role = null;
			
			role = info.getGuild().get().getRolesByName(info.getArguments()[0], true).get(0);
			
			if(role == null) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().ROLE_NOT_FOUND.format(info.getArguments()[0])).setColor(Color.RED).build()).queue();
				return true;
			}
			
			String roleName = role.getName();
			
			if(Bot.getInstance().getConnectedGuild(info.getGuild().get()).containsSelfAssignableRole(role)) {
				info.getGuild().get().getController().addSingleRoleToMember(info.getGuildMember().get(), role).queue(
						success -> info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().IAM_ROLE_ASSIGNED.format(info.getAuthor().getAsMention(), roleName)).setColor(Color.GREEN).build()).queue(),
						failure -> info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().IAM_ASSIGN_FAILURE.format()).setColor(Color.RED).build()).queue());
			} else {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().IAM_UNASSIGNABLE.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()).queue();
			}
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@Command(name = "iamnot", description = "IAMNOT_DESC", usage = "Role")
	public boolean removeSelfAssignableRoleToSelf(CommandInfo info) {
		if(info.getArguments().length == 1) {
			Role role = null;
			
			role = info.getGuild().get().getRolesByName(info.getArguments()[0], true).get(0);
			
			if(role == null) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().ROLE_NOT_FOUND.format(info.getArguments()[0])).setColor(Color.RED).build()).queue();
				return true;
			}
			
			String roleName = role.getName();
			
			if(Bot.getInstance().getConnectedGuild(info.getGuild().get()).containsSelfAssignableRole(role)) {
				info.getGuild().get().getController().removeSingleRoleFromMember(info.getGuildMember().get(), role).queue(
						success -> info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().IAM_ROLE_UNASSIGNED.format(info.getAuthor().getAsMention(), roleName)).setColor(Color.RED).build()).queue(),
						failure -> info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().IAM_UNASSIGN_FAILURE.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()).queue());
			} else {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().IAM_UNASSIGNABLE.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()).queue();
			}
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@Command(name = "serverinfo", description = "SERVERINFO_DESC")
	public boolean serverinfoCommand(CommandInfo info) {
		StringBuilder emojiBuilder = new StringBuilder();
		
		for(Emote emote : info.getGuild().get().getEmotes()) {
			if(emojiBuilder.length() + (emote.getName() + emote.getAsMention()).length() > 1024) {
				String toAppend = ", ...";
				
				if(emojiBuilder.length() + toAppend.length() > 1024) {
					emojiBuilder.append(toAppend);
				}
				
				break;
			}
			
			emojiBuilder.append(emote.getName() + emote.getAsMention());
		}
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
		
		info.getChannel().sendMessage(
				new EmbedBuilder()
				.setAuthor("Server Info", null, null)
				.setTitle(info.getGuild().get().getName())
				.addField("ID", info.getGuild().get().getId(), true)
				.addField(info.getLang().OWNER.format(), info.getGuild().get().getOwner().getUser().getName() + "#" + info.getGuild().get().getOwner().getUser().getDiscriminator(), true)
				.addField(info.getLang().MEMBERS.format(), String.valueOf(info.getGuild().get().getMembers().size()), true)
				.addField(info.getLang().TEXT_CHANNELS.format(), String.valueOf(info.getGuild().get().getTextChannels().size()), true)
				.addField(info.getLang().VOICE_CHANNELS.format(), String.valueOf(info.getGuild().get().getVoiceChannels().size()), true)
				.addField(info.getLang().CREATED_AT.format(), info.getGuild().get().getCreationTime().format(formatter), true)
				.addField(info.getLang().REGION.format(), info.getGuild().get().getRegion().getName(), true)
				.addField(info.getLang().ROLES.format(), String.valueOf(info.getGuild().get().getRoles().size()), true)
				.addField(info.getLang().CUSTOM_EMOJIS.format(info.getGuild().get().getEmotes().size()), emojiBuilder.toString(), false)
				.setThumbnail(info.getGuild().get().getIconUrl())
				.build()
				).queue();
		return true;
	}
	
	@RequireGuild
	@Command(name = "userinfo", description = "USERINFO_DESC", usage = {"@User", "23234234234234"})
	public boolean userinfoCommand(CommandInfo info) {
		Member member = null;
		if(info.getMessage().getMentionedUsers().size() == 1) {
			member = info.getGuild().get().getMember(info.getMessage().getMentionedUsers().get(0));
		} else if(info.getArguments().length == 1) {
			if(info.getArguments()[0].matches("(.){0,32}#(\\d){4}")) {
				List<Member> members = info.getGuild().get().getMembersByName(info.getArguments()[0], true);
				member = (members.size() > 0 ? members.get(0) : null);
			} else if(info.getArguments()[0].matches("(\\d){18}")) {
				member = info.getGuild().get().getMemberById(info.getArguments()[0]);
			}
		} else {
			member = info.getGuildMember().get();
		}
		
		if(member == null) {
			info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().USER_NOT_FOUND.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()).queue();
			return true;
		}
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
		
		StringBuilder roleString = new StringBuilder();
		
		for(Role role : member.getRoles()) {
			roleString.append(role.getName() + "\n");
		}
		
		info.getChannel().sendMessage(
				new EmbedBuilder()
				.addField("Name", member.getUser().getName() + "#" + member.getUser().getDiscriminator(), true)
				.addField("Nickname", member.getEffectiveName(), true)
				.addField("ID", member.getUser().getId(), true)
				.addField(info.getLang().SERVER_JOINDATE.format(), member.getJoinDate().format(formatter), true)
				.addField(info.getLang().DISCORD_JOINDATE.format(), member.getUser().getCreationTime().format(formatter), true)
				.addField(info.getLang().ROLES.format(), "**(" + member.getRoles().size() + ")** " + roleString.toString(), true)
				.setThumbnail(member.getUser().getAvatarUrl())
				.build()
				).queue();
		return true;
	}
	
	@Command(name = "profile", description = "Opens profile image")
	public boolean profileCommand(CommandInfo info) {
		String avatarURL = info.getAuthor().getAvatarUrl();
		
		try {
			BufferedImage profileCard = ImageIO.read(new File("./profile-card.png"));
			BufferedImage profileBG = ImageIO.read(new File("./profile-bg-anime.png"));
			BufferedImage ownerIMG = ImageIO.read(new File("profile-badge-owner.png"));
			
			HttpURLConnection conn = (HttpURLConnection) new URL(avatarURL).openConnection();
			conn.setRequestProperty("User-Agent", "DiscordBot");
			
			BufferedImage avatar = ImageIO.read(conn.getInputStream());
			
			URL notebookURL = new URL("https://discordapp.com/assets/908f9e562fb536d4f05c0e1d651ca6fe.svg");
			conn = (HttpURLConnection) notebookURL.openConnection();
			conn.setRequestProperty("User-Agent", "DiscordBot");
			
			BufferedImage notebook = ImageIO.read(conn.getInputStream());
			
			int w = profileCard.getWidth() / 2;
			int h = profileCard.getHeight() / 2;
			
			BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2d = img.createGraphics();
			
			g2d.drawImage(profileBG, 0, 0, w, h, null);
			g2d.drawImage(profileCard, 0, 0, w, h, null);
			g2d.drawImage(avatar, 15, 82, 64, 64, null);
			g2d.drawImage(notebook, 97, 233, 40, 40, null); //97, 233
			
			if(this.getBot().getConfig().getOwnerIds().contains(info.getAuthor().getIdLong())) {
				g2d.drawImage(ownerIMG, 8, 174, 22, 22, null);
			}
			
			Font levelFont = new Font("Whitney", Font.BOLD, 20);
			Font nameFont = new Font("Whitney", Font.BOLD, 16);
			Font titleFont = new Font("Whitney", Font.BOLD, 12);
			Font progessBarFont = new Font("Whitney", Font.BOLD, 10);
			
			//Name and Title
			g2d.setPaint(Color.WHITE);
			g2d.setFont(nameFont);
			FontMetrics fm = g2d.getFontMetrics();
			
			float nameX = 92.50f;
			float nameY = 108f + fm.getHeight();
			
			g2d.drawString(info.getAuthor().getName(), nameX, nameY);
			
			float badgesX = 76 - fm.stringWidth("Badges");
			float badgesY = 142 + fm.getHeight();
			
			g2d.drawString("Badges", badgesX, badgesY);
			
			float levelX = 96;
			float levelY = 172 + fm.getHeight();
			int levelWidth = fm.stringWidth("LEVEL");
			
			g2d.setColor(Color.darkGray);
			g2d.drawString("LEVEL", levelX, levelY);
			
			g2d.setFont(titleFont);
			g2d.setColor(Color.WHITE);
			fm = g2d.getFontMetrics();
			
			float titleX = nameX;
			float titleY = 124f + fm.getHeight(); //122.5
			
			String title = (getBot().getConfig().getOwnerIds().contains(info.getAuthor().getIdLong()) ?  "Bot loyal" : "Bot illoyal - Must die");
			
			g2d.drawString(title, titleX, titleY);
			
			//XP
			
			float[] hsb = Color.RGBtoHSB(114, 137, 218, null);
			g2d.setColor(Color.getHSBColor(hsb[0], hsb[1], hsb[2]));
			g2d.drawRect(98, 151, 187, 11); //286, 163
			
			for(int i = 151; i < 162; i++) {
				g2d.drawLine(98, i, 285, i);
			}
			
			g2d.setFont(progessBarFont);
			g2d.setColor(Color.darkGray);
			fm = g2d.getFontMetrics(); //99, 308
			
			float progressBarX = 99;
			float progressBarY = 148 + fm.getHeight();
			
			progressBarX = progressBarX + ((187 - fm.stringWidth("XP: 88 / 666")) / 2);
			
			g2d.drawString("XP: 88 / 666", progressBarX, progressBarY);
			
			g2d.setFont(levelFont);
			fm = g2d.getFontMetrics();
			
			float levelNumberX = 96 + ((levelWidth - fm.stringWidth("13")) / 2);
			float levelNumberY = levelY + fm.getHeight();
			
			g2d.drawString("13", levelNumberX, levelNumberY);
			
			g2d.setFont(titleFont);
			
			float totalxpX = levelX + levelWidth + 20;
			float totalxpY = 165 + fm.getHeight();
			
			g2d.drawString("Total XP     2017", totalxpX, totalxpY);
			
			float rankxpX = totalxpX;
			float rankxpY = totalxpY + fm.getHeight() - 10;
			
			g2d.drawString("Rank           #1", rankxpX, rankxpY);
			
			float moneyX = totalxpX;
			float moneyY = rankxpY + fm.getHeight() - 10;
			
			g2d.drawString("Money        2000€", moneyX, moneyY);
			
			//Badges: 10, 151 - 86, 152
			
			g2d.dispose();
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(img, "png", baos);
			baos.flush();
			byte[] imageBytes = baos.toByteArray();
			baos.close();
			
			info.getChannel().sendFile(imageBytes, "profile-" + info.getAuthor().getIdLong() + ".png", new MessageBuilder().append("**Profile for " + info.getAuthor().getAsMention() + "**").build()).queue();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Command(name = "stats", description = "STATS_DESC")
	public boolean statsCommand(CommandInfo info) {
		StringBuilder ownerBuilder = new StringBuilder();
		
		for(Long ownerId : Bot.getInstance().getConfig().getOwnerIds()) {
			ownerBuilder.append(ownerId + "\n");
		}
		
		info.getChannel().sendMessage(
				new EmbedBuilder()
				.setTitle("NightBot v0.0.1-SNAPSHOT")
				.addField(info.getLang().AUTHORS.format(), "Nightloewe#7706, Nightishaman#5678", true)
				.addField(info.getLang().ID_OF_BOT.format(), info.getChannel().getJDA().getSelfUser().getId(), true)
				.addField("Shard", "#" + info.getGuild().get().getJDA().getShardInfo().getShardId() + " / " + info.getGuild().get().getJDA().getShardInfo().getShardTotal(), true)
				.addField(info.getLang().OWNER_IDS.format(), ownerBuilder.toString(), true)
				.addField(info.getLang().PRESENCE.format(), info.getGuild().get().getJDA().getGuilds().size() + " Server\n"
						+ info.getGuild().get().getJDA().getTextChannels().size() + " " + info.getLang().TEXT_CHANNELS.format() + "\n"
						+ info.getGuild().get().getJDA().getVoiceChannels().size() + " " + info.getLang().VOICE_CHANNELS.format(), true)
				.setThumbnail(info.getGuild().get().getJDA().getSelfUser().getAvatarUrl())
				.build()
				).queue();
		return true;
	}
}
