package de.nightloewe.DiscordBot.modules;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import com.auth0.jwt.internal.org.apache.commons.io.output.ByteArrayOutputStream;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.command.CommandInfo;
import de.nightloewe.DiscordBot.command.Module;
import de.nightloewe.DiscordBot.command.annotations.Command;
import de.nightloewe.DiscordBot.command.annotations.RequireOwner;
import de.nightloewe.DiscordBot.command.annotations.RequireUserPermission;
import de.nightloewe.DiscordBot.entities.ConnectedGuild;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.Permission;

public class HOI4Module extends Module {

	private Bot bot;
	
	public HOI4Module(Bot bot) {
		super(bot);
		
		this.bot = bot;
	}

	@Override
	public String getName() {
		return "HOI4";
	}
	
	public boolean isEnabled(CommandInfo info) {
		return this.bot.getSetting(info.getGuild().get().getIdLong(), "HOI4").isTrue();
	}
	
	
	@Command(name = "updatehoi4map", description = "UPDATEHOI4MAP_DESC", usage = "[image url] <- without that argument you need to attach an image")
	public boolean updateMapCommand(CommandInfo info) {
		if(this.isEnabled(info)) {
			if(info.getArguments().length == 1) {
				String url = info.getArguments()[0];
				
				try {
					File file = new File("temp.bmp");
					
					URL website = new URL(url);
					
					ReadableByteChannel rbc = Channels.newChannel(website.openStream());
					FileOutputStream fos = new FileOutputStream(file);
					fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
					
					BufferedImage image = ImageIO.read(file);
					this.processImage(info, image);

					//info.getChannel().sendFile(file, new MessageBuilder("Processed image.").build());
					new BotEmbedBuilder().withOkColor().appendDescription("Processed image.").sendMessage(info.getChannel());
					file.delete();
					
					return true;
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				if(info.getMessage().getAttachments().size() > 0) {
					File file = new File("temp.bmp");
					
					if(info.getMessage().getAttachments().get(0).download(file)) {
						try {
							BufferedImage image = ImageIO.read(file);
							this.processImage(info, image);
							
							//info.getChannel().sendFile(file, new MessageBuilder("Processed image.").build());
							new BotEmbedBuilder().withOkColor().appendDescription("Processed image.").sendMessage(info.getChannel());
							file.delete();
							
							return true;
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				} else {
					return false;
				}
			}
		} else {
			new BotEmbedBuilder().appendDescription("This module needs to be enabled with the command enablehoi4").sendMessage(info.getChannel());
		}
		return false;
	}
	
	@RequireUserPermission(guildPermission = {Permission.BAN_MEMBERS})
	@Command(name = "randomrgb", description = "RANDOMRGB_DESC")
	public boolean randomrgbCommand(CommandInfo info) {
		ConnectedGuild guild = this.bot.getConnectedGuild(info.getGuild().get());
		
		if(this.bot.getSetting(info.getGuild().get().getIdLong(), "HOI4").isTrue()) {
			if(guild.getUsedRGBCodes().size() == 0) {
				new BotEmbedBuilder().appendDescription("Please initialize generator with updatehoi4map command.").sendMessage(info.getChannel());
				
				return true;
			}
			
			if(info.getArguments().length == 1) {
				try {
					int i = Integer.parseInt(info.getArguments()[0]);
					
					if(i > 20 || i < 1) {
						new BotEmbedBuilder().withErrorColor().appendDescription("Max 20 Min 1");
					} else {
						Random r = new Random();
						
						List<Integer> randomRGBCodes = new ArrayList<>();
						
						for(int x = 0; x < i; x++) {
							loop:
								for(int y = 0; y < 100; y++) {
									int rgb = r.nextInt(16777215);
									
									if(!guild.getUsedRGBCodes().contains(rgb)) {
										guild.getUsedRGBCodes().add(rgb);
										randomRGBCodes.add(rgb);
										break loop;
									}
								}
						}
						
						this.sendSample(info, randomRGBCodes);
					}
				} catch(NumberFormatException ex) {
					new BotEmbedBuilder().withErrorColor().appendDescription(ex.getLocalizedMessage());
				}
			} else {
				Random r = new Random();
				
				for(int y = 0; y < 100; y++) {
					int rgb = r.nextInt(16777215);
					
					if(!guild.getUsedRGBCodes().contains(rgb)) {
						guild.getUsedRGBCodes().add(rgb);
						this.sendSample(info, Arrays.asList(rgb));
						break;
					}
				}
			}
		} else {
			new BotEmbedBuilder().appendDescription("This module needs to be enabled with the command togglehoi4").sendMessage(info.getChannel());
		}
		
		return true;
	}
	
	@RequireOwner
	@Command(name = "togglehoi4", description = "ENABLEHOI4_DESC")
	public boolean togglehoi4Command(CommandInfo info) {
		long enabled = this.getBot().getSetting(info.getGuild().get().getIdLong(), "HOI4").getValueInt();
		
		if(enabled == 1) {
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "HOI4", 0);
			
			new BotEmbedBuilder().withErrorColor().appendDescription("Disabled module.").sendMessage(info.getChannel());
		} else {
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "HOI4", 1);
			
			new BotEmbedBuilder().withOkColor().appendDescription("Enabled module.").sendMessage(info.getChannel());
		}
		
		return true;
	}
	
	public void sendSample(CommandInfo info, List<Integer> rgbCodes) {
		StringBuilder codes = new StringBuilder();
		for(int i = 0; i < rgbCodes.size(); i++) {
			int rgb = rgbCodes.get(i);
			
			int red = (rgb >> 16) & 0xFF;
			int green = (rgb >> 8) & 0xFF;
			int blue = (rgb) & 0xFF;
			
			codes.append(i + " ID," + red +"," + green + "," + blue + "\n");
		}
		
		new BotEmbedBuilder().withOkColor().appendDescription("Found " + rgbCodes.size() + " codes\n" + codes).sendMessage(info.getChannel());
		
		int width = (rgbCodes.size() > 5 ? 500 : rgbCodes.size() * 100);
		int height = (rgbCodes.size() > 5 ? (rgbCodes.size() / 5) * 100 : 100);
		
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics g2d = image.getGraphics();
		
		for(int i = 0, x = 1, y = 0; i < rgbCodes.size(); i++) {
			g2d.setColor(new Color(rgbCodes.get(i)));
			g2d.fillRect(x, y, x + 100, y + 100);
			
			x += 100;
			
			if(x == 501) {
				x = 1;
				y += 100;
			}
			
			if((i + 1) == rgbCodes.size()) {
				g2d.dispose();
			}
		}
		
		try {
			ImageIO.write(image, "jpg", new File("temp.png"));
			
			info.getChannel().sendFile(new File("temp.png"), "samples.png").queue(m -> {
				new File("temp.png").delete();
			}, m -> {
				new File("temp.png").delete();
			});;
		} catch(IOException e) {
			new BotEmbedBuilder().withErrorColor().appendDescription(e.getLocalizedMessage()).sendMessage(info.getChannel());
		}
	}
	
	public void processImage(CommandInfo info, BufferedImage image) {
		ConnectedGuild guild = bot.getConnectedGuild(info.getGuild().get());
		final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		final int width = image.getWidth();
		final int height = image.getHeight();
		final boolean hasAlphaChannel = image.getAlphaRaster() != null;
		
		guild.getUsedRGBCodes().clear();
		
		final int pixelLength = 3;
		for(int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
			int rgb = 0;
			rgb += -16777216;
			rgb += ((int) pixels[pixel] & 0xFF);
			rgb += ((int) pixels[pixel + 1] & 0xFF) << 8;
			rgb += ((int) pixels[pixel + 2] & 0xFF) << 16;
			
			guild.getUsedRGBCodes().add(rgb);
			col++;
			
			if(col == width) {
				col = 0;
				row++;
			}
		}
	}

}
