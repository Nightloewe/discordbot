package de.nightloewe.DiscordBot.modules;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.command.AnnotatedCommand;
import de.nightloewe.DiscordBot.command.CommandInfo;
import de.nightloewe.DiscordBot.command.ICommand;
import de.nightloewe.DiscordBot.command.IModule;
import de.nightloewe.DiscordBot.command.Module;
import de.nightloewe.DiscordBot.command.ModuleAlias;
import de.nightloewe.DiscordBot.command.annotations.Command;
import de.nightloewe.DiscordBot.command.annotations.RequireBotUserPermission;
import de.nightloewe.DiscordBot.command.annotations.RequireOwner;
import de.nightloewe.DiscordBot.config.Language;
import de.nightloewe.DiscordBot.config.Language.LangString;
import de.nightloewe.DiscordBot.entities.Setting;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageEmbed;

public class HelpModule extends Module {

	public HelpModule(Bot bot) {
		super(bot);
	}

	@Override
	public String getName() {
		return "Help";
	}
	
	@Command(name = "help", description = "HELP_DESC", usage = "[command]", aliases = {"h"})
	public boolean helpCommand(CommandInfo info) {
		if(info.getAuthor().getIdLong() == 166610782782619649L) {
			info.getChannel().sendMessage(info.getAuthor().getAsMention() + " nix!").queue();
		}
		if(info.getArguments().length > 0) {
			MessageEmbed embed = this.getCommandHelp(info.getArguments()[0].substring(1), info.getGuild().get());
			
			if(embed != null) {
				info.getChannel().sendMessage(embed).queue();
			} else {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(
						info.getLang().COMMAND_NOT_FOUND.format(info.getAuthor().getAsMention(), info.getArguments()[0])).build()
						).queue();
			}
		} else {
			info.getAuthor().openPrivateChannel().queue(channel -> {
				channel.sendMessage(info.getLang().HELP_INFO.format()).queue();
			});
		}
		return true;
	}
	
	@Command(name = "modules", description = "MODULES_DESC")
	public boolean modulesCommand(CommandInfo info) {
		EmbedBuilder embedBuilder = new EmbedBuilder();
		StringBuilder stringBuilder = new StringBuilder();
		
		embedBuilder.setTitle(info.getLang().LIST_OF_MODULES.format());
		
		for(String name : this.getBot().getModules().keySet()) {
			stringBuilder.append("\u2022 " + name + "\n");
		}
		
		embedBuilder.setDescription(stringBuilder.toString());
		embedBuilder.setFooter(info.getLang().LIST_OF_MODULES_FOOTER.format(), null);
		
		info.getChannel().sendMessage(embedBuilder.build()).queue();
		return true;
	}
	
	@Command(name = "commands", description = "COMMANDS_DESC", usage = "<moduleName>", aliases = {"cmds"})
	public boolean commandsCommand(CommandInfo info) {
		if(info.getArguments().length > 0) {
			IModule module = this.getBot().getModules().get(info.getArguments()[0]);
			
			if(module == null) {
				info.getChannel().sendMessage(new EmbedBuilder().setDescription(
						info.getLang().MODULE_NOT_FOUND.format(info.getAuthor().getAsMention(), info.getArguments()[0])).build()
						).queue();
				return false;
			}
			
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("```");
			
			Setting prefixSet = Bot.getInstance().getSetting(-1, "CommandPrefix");
			
			if(module instanceof ModuleAlias) {
				ModuleAlias moduleAlias = (ModuleAlias) module;
				
				for(ICommand command : moduleAlias.getAliases()) {
					stringBuilder.append(prefixSet.getValue() + command.getName() + "\n");
				}
			} else {
				for(Method method : module.getClass().getDeclaredMethods()) {
					if(method.getAnnotation(Command.class) != null) {
						Command commandAnnotation = method.getAnnotation(Command.class);
						
						stringBuilder.append(prefixSet.getValue() + commandAnnotation.name() + "\n");
					}
				}
			}
			
			stringBuilder.append("```");
			info.getChannel().sendMessage(stringBuilder.toString()).queue();
			return true;
		}
		return false;
	}
	
	public MessageEmbed getCommandHelp(String command, Guild guild) {
		EmbedBuilder builder = new EmbedBuilder();
		Language lang = Bot.getInstance().getConnectedGuild(guild).getLang();
		
		if(lang == null)
			lang = Bot.getInstance().getDefaultLanguage();
		
		for(String name : this.getBot().getCommandMap().getCommands().keySet()) {
			if(name.equalsIgnoreCase(command)) {
				ICommand cmd = this.getBot().getCommandMap().getCommand(name);
				
				Setting prefixSet = Bot.getInstance().getSetting(-1, "CommandPrefix");
				
				StringBuilder titleBuilder = new StringBuilder();
				titleBuilder.append(prefixSet.getValue() + cmd.getName());
				
				for(String alias : cmd.getAliases()) {
					titleBuilder.append(" / " + prefixSet.getValue() + alias);
				}
				
				StringBuilder usageBuilder = new StringBuilder();
				
				int usageInt = 0;
				for(String usage : cmd.getUsage()) {
					usageBuilder.append((usageInt != 0 ? lang.OR.format() : "") + "`" + prefixSet.getValue() + cmd.getName() + " " + usage + "`");
					usageInt++;
				}
				
				if(usageBuilder.length() == 0) {
					usageBuilder.append("`" + prefixSet.getValue() + cmd.getName() + "`");
				}
				
				String description = "";
				
				Object descObj = null;
				try {
					Field langField = lang.getClass().getDeclaredField(cmd.getDescription());
					langField.setAccessible(true);
					descObj = langField.get(lang);
				} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				if(descObj instanceof LangString) {
					LangString desc = (LangString) descObj;
					description = desc.format();
				} else {
					description = cmd.getDescription();
				}
				
				builder.setTitle(titleBuilder.toString());
				builder.setDescription(description);
				builder.addField(lang.USAGE.format(), usageBuilder.toString(), false);
				
				if(cmd instanceof AnnotatedCommand) {
					AnnotatedCommand annotatedCommand = (AnnotatedCommand) cmd;
					try {
						Field f = annotatedCommand.getClass().getDeclaredField("invokeObject");
						f.setAccessible(true);
						Object obj = f.get(annotatedCommand);
						
						if(obj instanceof IModule) {
							IModule module = (IModule) obj;
							builder.setFooter(lang.MODULE.format() + module.getName(), null);
						}
						
						Field f2 = annotatedCommand.getClass().getDeclaredField("method");
						f2.setAccessible(true);
						Object obj2 = f2.get(annotatedCommand);
						
						if(obj2 instanceof Method) {
							Method method = (Method) obj2;
							
							if(method.getAnnotation(RequireOwner.class) != null) {
								builder.getDescriptionBuilder().append(lang.ONLY_OWNER.format());
							}
							
							if(method.getAnnotation(RequireBotUserPermission.class) != null) {
								RequireBotUserPermission annotation = method.getAnnotation(RequireBotUserPermission.class);
								StringBuilder guildPermBuilder = new StringBuilder();
								
								for(Permission perm : annotation.guildPermission()) {
									guildPermBuilder.append(perm.getName() + "\n");
								}
								if(guildPermBuilder.length() != 0)
									builder.addField("Bot Server Permissions", guildPermBuilder.toString(), false);
								
								StringBuilder channelPermBuilder = new StringBuilder();
								
								for(Permission perm : annotation.channelPermission()) {
									channelPermBuilder.append(perm.getName() + "\n");
								}
								if(channelPermBuilder.length() != 0)
									builder.addField("Bot Channel Permissions", channelPermBuilder.toString(), false);
							}
						}
					} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				}
				
				return builder.build();
			}
		}
		return null;
	}
}
