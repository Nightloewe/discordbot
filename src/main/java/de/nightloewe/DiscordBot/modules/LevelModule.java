package de.nightloewe.DiscordBot.modules;

import java.awt.Color;
import java.util.List;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.command.CommandInfo;
import de.nightloewe.DiscordBot.command.Module;
import de.nightloewe.DiscordBot.command.annotations.Command;
import de.nightloewe.DiscordBot.command.annotations.RequireBotUserPermission;
import de.nightloewe.DiscordBot.command.annotations.RequireGuild;
import de.nightloewe.DiscordBot.command.annotations.RequireUserPermission;
import de.nightloewe.DiscordBot.entities.LevelData;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Member;

public class LevelModule extends Module {

	public LevelModule(Bot bot) {
		super(bot);
	}
	
	public boolean isEnabled(CommandInfo info) {
		return this.getBot().getSetting(info.getGuild().get().getIdLong(), "Level").isTrue();
	}
	
	@RequireGuild
	@Command(name = "rank", description = "RANK_DESC", usage = "")
	public boolean rankCommand(CommandInfo info) {
		if(this.isEnabled(info)) {
			LevelData data = Bot.getInstance().getDatabase().getLevelData(info.getGuild().get(), info.getGuildMember().get().getUser().getIdLong());
			
			if(data == null) {
				data = new LevelData(info.getGuildMember().get(), 0, 0, 0, -1);
				
				Bot.getInstance().getDatabase().updateLevelData(info.getGuild().get(), data);
			}
			
			new BotEmbedBuilder().withOkColor().setDescription(info.getGuildMember().get().getAsMention() + ": LEVEL " + data.getLevel() + " | XP " + data.getXp() + "/" + data.calculateXPForNextLevel() + " | TOTAL XP " + data.getTotalXP() + " | RANK 1/1");
		} else {
			new BotEmbedBuilder().withErrorColor().setDescription("Level module is disabled!").sendMessage(info.getChannel());
		}
		
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "setXp", description = "SETXP_DESC", usage = {"@User newxp", "23234234234234 newxp"})
	public boolean setXpCommand(CommandInfo info) {
		if(this.isEnabled(info)) {
			if(info.getArguments().length == 2) {
				Member member = null;
				if(info.getMessage().getMentionedUsers().size() == 1) {
					member = info.getGuild().get().getMember(info.getMessage().getMentionedUsers().get(0));
				} else if(info.getArguments().length == 2) {
					if(info.getArguments()[0].matches("(.){0,32}#(\\d){4}")) {
						List<Member> members = info.getGuild().get().getMembersByName(info.getArguments()[0], true);
						member = (members.size() > 0 ? members.get(0) : null);
					} else if(info.getArguments()[0].matches("(\\d){18}")) {
						member = info.getGuild().get().getMemberById(info.getArguments()[0]);
					}
				} else {
					member = info.getGuildMember().get();
				}
				
				if(member == null) {
					try {
						LevelData data = Bot.getInstance().getDatabase().getLevelData(info.getGuild().get(), Long.parseLong(info.getArguments()[0]));
						
						if(data == null) {
							info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().USER_NOT_FOUND.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()).queue();
							
							return true;
						}
						
						int xp = Integer.parseInt(info.getArguments()[1]);
						
						data.setXp(xp);
						
						new BotEmbedBuilder().withOkColor().setDescription("Set " + info.getArguments()[0] + "'s xp to " + xp).sendMessage(info.getChannel());
					} catch(NumberFormatException ex) {
						new BotEmbedBuilder().withErrorColor().setDescription(info.getLang().NEED_NATURAL_NUMBER.format()).sendMessage(info.getChannel());
					}
					
					
					
					return true;
				}
				
				LevelData data = Bot.getInstance().getDatabase().getLevelData(info.getGuild().get(), member.getUser().getIdLong());
				
				if(data == null) {
					data = new LevelData(info.getGuildMember().get(), 0, 0, 0, -1);
					
					Bot.getInstance().getDatabase().updateLevelData(info.getGuild().get(), data);
				}
				
				try {
					int xp = Integer.parseInt(info.getArguments()[1]);
					
					data.setXp(xp);
					
					new BotEmbedBuilder().withOkColor().setDescription("Set " + member.getEffectiveName() + "'s xp to " + xp).sendMessage(info.getChannel());
				} catch(NumberFormatException ex) {
					info.getChannel().sendMessage(new EmbedBuilder().setDescription(info.getLang().NEED_NATURAL_NUMBER.format(info.getAuthor().getAsMention())).setColor(Color.RED).build()).queue();
				}
			}
		}
		
		return true;
	}
	
	@RequireGuild
	@RequireBotUserPermission(guildPermission = {Permission.MANAGE_ROLES, Permission.MESSAGE_MANAGE})
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "toggleLevel", description = "LEVEL_DESC", usage = "")
	public boolean toggleLevel(CommandInfo info) {
		if(this.getBot().getSetting(info.getGuild().get().getIdLong(), "Level").isTrue()) {
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "Level", 0);
			
			new BotEmbedBuilder().withErrorColor().setDescription("Disabled level module").sendMessage(info.getChannel());
		} else {
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "Level", 1);
			
			new BotEmbedBuilder().withOkColor().setDescription("Enabled level module").sendMessage(info.getChannel());
		}
		
		return false;
	}

	@Override
	public String getName() {
		return "Level";
	}

}
