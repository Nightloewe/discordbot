package de.nightloewe.DiscordBot.modules;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.command.CommandInfo;
import de.nightloewe.DiscordBot.command.Module;
import de.nightloewe.DiscordBot.command.annotations.Command;
import de.nightloewe.DiscordBot.command.annotations.RequireGuild;
import de.nightloewe.DiscordBot.command.annotations.RequireUserPermission;
import de.nightloewe.DiscordBot.entities.ConnectedGuild;
import de.nightloewe.DiscordBot.entities.MusicPlayer;
import de.nightloewe.DiscordBot.entities.Playlist;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.GuildVoiceState;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.VoiceChannel;

public class MusicModule extends Module {

	private DefaultAudioPlayerManager playerManager;

	public MusicModule(Bot bot) {
		super(bot);
		
		this.playerManager = new DefaultAudioPlayerManager();
		AudioSourceManagers.registerRemoteSources(this.playerManager);
		AudioSourceManagers.registerLocalSource(this.playerManager);
	}

	@Override
	public String getName() {
		return "Music";
	}
	
	public DefaultAudioPlayerManager getPlayerManager() {
		return playerManager;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.VOICE_MUTE_OTHERS})
	@Command(name = "next", description = "NEXT_TRACK_DESC")
	public boolean next(CommandInfo info) {
		ConnectedGuild cGuild = Bot.getInstance().getConnectedGuild(info.getGuild().get());
		MusicPlayer player = cGuild.getPlayer();
		
		player.nextTrack();
		
		new BotEmbedBuilder().withOkColor().setDescription(info.getLang().MUSIC_PLAYER_SKIPPED_TRACK.format()).sendMessage(info.getChannel());
		return true;
	}
	
	@RequireGuild
	@Command(name = "voteskip", description = "VOTESKIP_DESC")
	public boolean voteSkip(CommandInfo info) {
		if(!info.getGuildMember().get().getVoiceState().inVoiceChannel()) {
			new BotEmbedBuilder().setDescription(info.getLang().MUST_BE_IN_VOICECHANNEL.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			return true;
		}
		if(info.getGuildMember().get().getVoiceState().getAudioChannel().getIdLong() != info.getGuild().get().getAudioManager().getConnectedChannel().getIdLong()) {
			new BotEmbedBuilder().setDescription(info.getLang().MUST_BE_IN_BOT_VOICECHANNEL.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			return true;
		}
		
		ConnectedGuild cGuild = Bot.getInstance().getConnectedGuild(info.getGuild().get());
		MusicPlayer player = cGuild.getPlayer();
		
		ArrayList<Member> voteSkippable = new ArrayList<Member>();
		
		for(Member member : info.getGuild().get().getAudioManager().getConnectedChannel().getMembers()) {
			GuildVoiceState gvs = member.getVoiceState();
			if(!gvs.isDeafened() && !gvs.isMuted() && member != info.getGuild().get().getSelfMember()) {
				voteSkippable.add(member);
			}
		}
		
		if(!player.getVoteSkips().contains(info.getGuildMember().get())) {
			player.getVoteSkips().add(info.getGuildMember().get());
		}
		
		if(player.getVoteSkips().size() >= voteSkippable.size()) {
			player.nextTrack();
			player.getVoteSkips().clear();
			new BotEmbedBuilder().withOkColor().setDescription(info.getLang().MUSIC_PLAYER_SKIPPED_TRACK.format()).sendMessage(info.getChannel());
			return true;
		}
		
		new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYER_VOTESKIP_TRACK.format(player.getVoteSkips().size(), voteSkippable.size())).sendMessage(info.getChannel());
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.VOICE_MUTE_OTHERS})
	@Command(name = "stop", description = "STOP_TRACK_DESC")
	public boolean stop(CommandInfo info) {
		ConnectedGuild cGuild = Bot.getInstance().getConnectedGuild(info.getGuild().get());
		MusicPlayer player = cGuild.getPlayer();
		
		player.getPlayer().stopTrack();
		
		new BotEmbedBuilder().withErrorColor().setDescription("Stopped Player").sendMessage(info.getChannel());
		return true;
	}
	
	
	@RequireGuild
	@Command(name = "volume", description = "VOLUME_DESC")
	public boolean volume(CommandInfo info) {
		if(!info.getGuildMember().get().getVoiceState().inVoiceChannel() || info.getGuildMember().get().getVoiceState().getAudioChannel() != info.getGuild().get().getAudioManager().getConnectedChannel()) {
			new BotEmbedBuilder().setDescription(info.getLang().MUST_BE_IN_VOICECHANNEL.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			return true;
		}
		
		if(info.getArguments().length == 1) {
			ConnectedGuild cGuild = Bot.getInstance().getConnectedGuild(info.getGuild().get());
			MusicPlayer player = cGuild.getPlayer();
			
			player.getPlayer().setVolume(Integer.parseInt(info.getArguments()[0]));
			
			new BotEmbedBuilder().setDescription("Set volume to " + info.getArguments()[0]).sendMessage(info.getChannel());
		}
		return true;
	}
	
	@RequireGuild
	@Command(name = "queue", description = "QUEUE_DESC", aliases = {"q", "addtoqueue", "addsong", "songrequest", "sr"}, usage = "<link>")
	public boolean addToQueue(CommandInfo info) {
		if(!info.getGuildMember().get().getVoiceState().inVoiceChannel() || info.getGuildMember().get().getVoiceState().getAudioChannel() != info.getGuild().get().getAudioManager().getConnectedChannel()) {
			new BotEmbedBuilder().setDescription(info.getLang().MUST_BE_IN_VOICECHANNEL.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			return true;
		}
		
		ConnectedGuild cGuild = Bot.getInstance().getConnectedGuild(info.getGuild().get());
		MusicPlayer player = cGuild.getPlayer();
		
		StringBuilder builder = new StringBuilder();
		
		for(String s : info.getArguments()) {
			builder.append(s);
		}
		
		this.playerManager.loadItemOrdered(player, builder.toString(), new AudioLoadResultHandler() {

			@Override
			public void loadFailed(FriendlyException exc) {
				new BotEmbedBuilder().withErrorColor().setDescription(info.getLang().COULD_NOT_LOAD_MUSIC.format(info.getGuildMember().get().getAsMention())).sendMessage(info.getChannel());
			}

			@Override
			public void noMatches() {
				new BotEmbedBuilder().withErrorColor().setDescription(info.getLang().NO_MATCHES.format(info.getGuildMember().get().getAsMention())).sendMessage(info.getChannel());
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {
				new BotEmbedBuilder().withOkColor().setDescription(info.getLang().QUEUED_PLAYLIST.format(info.getGuildMember().get().getAsMention(), playlist.getName())).sendMessage(info.getChannel());
				
				for(AudioTrack track : playlist.getTracks()) {
					player.queue(track);
				}
			}

			@Override
			public void trackLoaded(AudioTrack track) {
				new BotEmbedBuilder().withOkColor().setDescription(info.getLang().QUEUED.format(info.getGuildMember().get().getAsMention(), track.getInfo().title.equalsIgnoreCase("Unknown title") ? track.getInfo().identifier : track.getInfo().title)).sendMessage(info.getChannel());
				
				player.queue(track);
			}
			
		});
		
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.VOICE_MUTE_OTHERS})
	@Command(name = "connect", description = "CONNECT_DESC")
	public boolean connect(CommandInfo info) {
		if(!info.getGuildMember().get().getVoiceState().inVoiceChannel()) {
			new BotEmbedBuilder().setDescription(info.getLang().MUST_BE_IN_VOICECHANNEL.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			return true;
		}
		
		info.getGuild().get().getAudioManager().openAudioConnection(info.getGuildMember().get().getVoiceState().getChannel());
		
		new BotEmbedBuilder().withOkColor().setDescription(info.getLang().CONNECTED.format(info.getGuildMember().get().getAsMention(), info.getGuildMember().get().getVoiceState().getChannel().getName())).sendMessage(info.getChannel());
		
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "setDefaultChannel", description = "SETDEFAULTCHANNEL_DESC")
	public boolean setDefaultChannel(CommandInfo info) {
		if(!info.getGuildMember().get().getVoiceState().inVoiceChannel()) {
			new BotEmbedBuilder().setDescription(info.getLang().MUST_BE_IN_VOICECHANNEL.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			return true;
		}
		
		VoiceChannel channel = info.getGuildMember().get().getVoiceState().getChannel();
		this.getBot().setSetting(info.getGuild().get().getIdLong(), "DefaultAudioChannel", channel.getIdLong());
		
		info.getGuild().get().getAudioManager().openAudioConnection(channel);
		
		return true;
	}
	
	@RequireGuild
	@Command(name = "listqueue", description = "LISTQUEUE_DESC")
	public boolean listQueue(CommandInfo info) {
		ConnectedGuild g = this.getBot().getConnectedGuild(info.getGuild().get());
		MusicPlayer player = g.getPlayer();
		
		EmbedBuilder builder = new EmbedBuilder();
		builder.setTitle(info.getLang().MUSIC_QUEUE.format());
		
		AudioTrack[] tracks = {};
		tracks = player.getQueue().toArray(tracks);
		
		int i = 0;
		
		if(player.getNextTrack() != null) {
			builder.addField("#0", info.getLang().MUSIC_QUEUE_ENTRY.format(player.getNextTrack().getInfo().title, player.getNextTrack().getInfo().author, player.getNextTrack().getInfo().identifier), false);
			i = 1;
		}
		
		int size = i + player.getQueue().size();
		
		if(tracks.length > 0) {
			for(; i < (size > 25 ? 25 : size); i++) {
				AudioTrack track = tracks[i - 1];
				builder.addField("#" + i, info.getLang().MUSIC_QUEUE_ENTRY.format(track.getInfo().title, track.getInfo().author, track.getInfo().identifier), false);
			}
		}
		
		if(builder.getFields().size() == 0)
			builder.setDescription(info.getLang().MUSIC_QUEUE_EMPTY.format());
		
		info.getChannel().sendMessage(builder.build()).queue();
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.VOICE_MUTE_OTHERS})
	@Command(name = "qremove", description = "QREMOVE_DESC")
	public boolean removeFromQueue(CommandInfo info) {
		if(info.getArguments().length == 1) {
			int position = 0;
			
			try {
				position = Integer.parseInt(info.getArguments()[0]);
			} catch(NumberFormatException e) {
				new BotEmbedBuilder().setDescription(info.getLang().NEED_NATURAL_NUMBER.format()).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			ConnectedGuild g = this.getBot().getConnectedGuild(info.getGuild().get());
			MusicPlayer player = g.getPlayer();
			
			if(position == 0 && player.getNextTrack() != null && player.getCurrentPlaylist() == null) {
				player.setNextTrack(player.getQueue().poll());
				new BotEmbedBuilder().setDescription(info.getLang().CMD_QCLEAR_CLEARED_EXACT.format(position)).withOkColor().sendMessage(info.getChannel());
				return true;
			}
			
			int i = 0;
			Iterator<AudioTrack> it = player.getQueue().iterator();
			
			while(it.hasNext()) {
				i++;
				AudioTrack t = it.next();
				
				if(position == i) {
					player.getQueue().remove(t);
					new BotEmbedBuilder().setDescription(info.getLang().CMD_QCLEAR_CLEARED_EXACT.format(position)).withOkColor().sendMessage(info.getChannel());
					return true;
				}
			}
			
			new BotEmbedBuilder().setDescription(info.getLang().CMD_QCLEAR_COULDNT_FIND.format(position)).withErrorColor().sendMessage(info.getChannel());
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(channelPermission = {Permission.VOICE_MUTE_OTHERS})
	@Command(name = "qclear", description = "QCLEAR_DESC", usage = {"", "3"})
	public boolean clearQueue(CommandInfo info) {
		ConnectedGuild g = this.getBot().getConnectedGuild(info.getGuild().get());
		MusicPlayer player = g.getPlayer();
		
		BlockingQueue<AudioTrack> queue = player.getQueue();
		
		if(info.getArguments().length == 1) {
			try {
				int i = Integer.parseInt(info.getArguments()[0]);
				
				for(int j = 0; j < i; j++) {
					queue.poll();
				}
				
				new BotEmbedBuilder().withOkColor().setDescription(info.getLang().CMD_QCLEAR_CLEARED_NUMBER.format(i));
				
				return true;
			} catch(NumberFormatException ex) {
				new BotEmbedBuilder().withErrorColor().setDescription(info.getLang().NEED_NATURAL_NUMBER.format()).sendMessage(info.getChannel());;
			}
		} else {
			queue.clear();
			
			new BotEmbedBuilder().withOkColor().setDescription(info.getLang().CMD_QCLEAR_CLEARED.format()).sendMessage(info.getChannel());
			
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@Command(name = "playlists", description = "PLAYLISTS_DESC", usage = "<page>")
	public boolean listPlaylists(CommandInfo info) {
		int start = 0;
		
		if(info.getArguments().length == 1) {
			try {
				start = (Integer.parseInt(info.getArguments()[0]) - 1) * 10;
			} catch(NumberFormatException e) {
				new BotEmbedBuilder().setDescription(info.getLang().NEED_NATURAL_NUMBER.format()).withErrorColor().sendMessage(info.getChannel());
				return false;
			}
		}
		
		List<Playlist> playlists = new ArrayList<Playlist>(this.getBot().getConnectedGuild(info.getGuild().get()).getPlaylists().values());
		
		int max = start + 10;
		
		if(max > playlists.size())
			max = playlists.size();
		
		EmbedBuilder builder = new EmbedBuilder();
		builder.setTitle(info.getLang().MUSIC_PLAYLISTS_TITLE.format(playlists.size()));
		
		for(int i = start; i < max; i++) {
			builder.appendDescription(info.getLang().MUSIC_PLAYLISTS_ENTRY.format(i, playlists.get(i).getName(), playlists.get(i).getTrackURLs().size(), playlists.get(i).getTracks().size() != 0 ? "Loaded" : "Unloaded"));
		}
		
		info.getChannel().sendMessage(builder.build()).queue();
		return true;
	}
	
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "addtoplaylist", description = "ADDTOPLAYLIST_DESC", usage = "name trackurl")
	public boolean addToPlaylist(CommandInfo info) {
		//NOTICE: Please block tracks with the length LONG.MAX_VALUE because they are streams
		
		if(info.getArguments().length == 2) {
			String playlistName = info.getArguments()[0];
			String URL = info.getArguments()[1];
			
			if(Bot.getInstance().getDatabase().getPlaylist(info.getGuild().get().getIdLong(), playlistName) != null) {
				Playlist playlist = MusicModule.this.getBot().getConnectedGuild(info.getGuild().get()).getPlaylists().get(playlistName);
				
				this.getPlayerManager().loadItem(URL, new AudioLoadResultHandler() {
					
					@Override
					public void trackLoaded(AudioTrack track) {
						try {
							playlist.addTrack(track);
							playlist.getTrackURLs().add(URL);
							
							Bot.getInstance().getDatabase().insertPlaylist(info.getGuild().get().getIdLong(), playlist);
							new BotEmbedBuilder()
								.setDescription(info.getLang().MUSIC_PLAYLIST_ADDED.format(
										track.getInfo().title.equalsIgnoreCase("Unknown title") ? track.getInfo().title : track.getInfo().identifier,
										playlist.getName()
										))
								.sendMessage(info.getChannel());
						} catch (Exception e) {
							new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_ONLY_100_TRACKS.format()).withErrorColor().sendMessage(info.getChannel());
						}
					}
					
					@Override
					public void playlistLoaded(AudioPlaylist playlist) {
						new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_ADD_ERROR.format("Playlists are not allowed as track!")).withErrorColor().sendMessage(info.getChannel());
					}
					
					@Override
					public void noMatches() {
						new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_ADD_ERROR.format("Track wasn't found!")).withErrorColor().sendMessage(info.getChannel());
					}
					
					@Override
					public void loadFailed(FriendlyException exception) {
						new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_ADD_ERROR.format(exception.getLocalizedMessage())).withErrorColor().sendMessage(info.getChannel());
					}
				});
			} else {
				this.getPlayerManager().loadItem(URL, new AudioLoadResultHandler() {
					
					@Override
					public void trackLoaded(AudioTrack track) {
						try {
							Playlist playlist = new Playlist(playlistName, track);
							MusicModule.this.getBot().getConnectedGuild(info.getGuild().get()).getPlaylists().put(playlistName, playlist);
							
							Bot.getInstance().getDatabase().insertPlaylist(info.getGuild().get().getIdLong(), playlist);
							new BotEmbedBuilder()
							.setDescription(info.getLang().MUSIC_PLAYLIST_ADDED.format(
									track.getInfo().title.equalsIgnoreCase("Unknown title") ? track.getInfo().title : track.getInfo().identifier,
									playlist.getName()
									))
							.sendMessage(info.getChannel());
						} catch (Exception e) {
							new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_ONLY_100_TRACKS.format()).withErrorColor().sendMessage(info.getChannel());
						}
					}
					
					@Override
					public void playlistLoaded(AudioPlaylist playlist) {
						new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_ADD_ERROR.format("Playlists are not allowed as track!")).withErrorColor().sendMessage(info.getChannel());
					}
					
					@Override
					public void noMatches() {
						new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_ADD_ERROR.format("Track wasn't found!")).withErrorColor().sendMessage(info.getChannel());
					}
					
					@Override
					public void loadFailed(FriendlyException exception) {
						new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_ADD_ERROR.format(exception.getLocalizedMessage())).withErrorColor().sendMessage(info.getChannel());
					}
				});
			}
			
			return true;
		}
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(channelPermission = {Permission.VOICE_MUTE_OTHERS})
	@Command(name = "loadplaylist", description = "LOADPLAYLIST_DESC", usage = "playlistName")
	public boolean loadPlaylist(CommandInfo info) {
		if(info.getArguments().length == 1) {
			if(Bot.getInstance().getDatabase().getPlaylist(info.getGuild().get().getIdLong(), info.getArguments()[0]) != null) {
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				Playlist playlist = cGuild.getPlaylists().get(info.getArguments()[0]);
				
				if(playlist.getTracks().size() != 0) {
					new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_ALREADY_LOADED.format(playlist.getName())).withOkColor().sendMessage(info.getChannel());
					return true;
				}
				
				MusicPlayer player = cGuild.getPlayer();
				
				if(!playlist.isLoading()) {
					player.loadPlaylist(playlist);
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_CURRENTLY_LOADING.format(playlist.getName())).withErrorColor().sendMessage(info.getChannel());
					return true;
				}
				
				new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_NOW_LOADING.format(playlist.getName())).sendMessage(info.getChannel());
			}
		}
		
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(channelPermission = {Permission.VOICE_MUTE_OTHERS})
	@Command(name = "startplaylist", description = "STARTPLAYLIST_DESC", usage = "playlistName")
	public boolean playPlaylist(CommandInfo info) {
		if(info.getArguments().length == 1) {
			if(Bot.getInstance().getDatabase().getPlaylist(info.getGuild().get().getIdLong(), info.getArguments()[0]) != null) {
				ConnectedGuild cGuild = Bot.getInstance().getConnectedGuild(info.getGuild().get());
				
				Playlist playlist = cGuild.getPlaylists().get(info.getArguments()[0]);
				MusicPlayer player = cGuild.getPlayer();
				
				if(playlist.getTracks().size() == 0 && !playlist.isLoading()) {
					player.loadPlaylist(playlist);
					new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_NOW_LOADING.format(playlist.getName())).sendMessage(info.getChannel());
				}
				
				if(playlist.isLoading()) {
					new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_CURRENTLY_LOADING.format(playlist.getName())).withErrorColor().sendMessage(info.getChannel());
					return true;
				}
				
				if(playlist.getPlayer() == null) {
					player.getPlayer().stopTrack();
					
					if(player.getPlayer().getPlayingTrack() != null)
						player.getQueue().push(player.getPlayer().getPlayingTrack());
					
					playlist.play(player);
					
					new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_NOW_PLAYING.format(playlist.getName())).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_CURRENTLY_PLAYING.format(playlist.getName())).withErrorColor().sendMessage(info.getChannel());
				}
			}
		}
		
		return true;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "playlistremove", description = "PLAYLISTREMOVE_DESC", usage = "playlistName trackURL")
	public boolean removePlaylist(CommandInfo info) {
		if(info.getArguments().length == 2) {
			String playlistName = info.getArguments()[0];
			String URL = info.getArguments()[1];
			
			ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
			
			if(cGuild.getPlaylists().containsKey(playlistName)) {
				Playlist playlist = cGuild.getPlaylists().get(playlistName);
				
				for(AudioTrack track : playlist.getTracks()) {
					if(track.getInfo().uri.equalsIgnoreCase(URL)) {
						playlist.removeTrack(track);
					}
				}
				
				playlist.getTrackURLs().remove(URL);
				
				if(playlist.getTracks().size() == 0 && playlist.getTrackURLs().size() == 0) {
					cGuild.getPlaylists().remove(playlist.getName());
					Bot.getInstance().getDatabase().removePlaylist(cGuild.getGuild().getIdLong(), playlist.getName());
				}
				
				this.getBot().getDatabase().insertPlaylist(cGuild.getGuild().getIdLong(), playlist);
				
				new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_REMOVED_TRACK.format(URL, playlist.getName())).withErrorColor().sendMessage(info.getChannel());
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().MUSIC_PLAYLIST_NOT_FOUND.format(playlistName)).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		
		return false;
	}
	
	@RequireGuild
	@RequireUserPermission(guildPermission = {Permission.ADMINISTRATOR})
	@Command(name = "controls", description = "CONTROLS_DESC")
	public boolean initPlayerControls(CommandInfo info) {
		MessageBuilder messageBuilder = new MessageBuilder();
		EmbedBuilder embedBuilder = new EmbedBuilder();
		
		ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
		
		messageBuilder.append(info.getLang().MUSIC_PLAYER_CONTROL_KEY.format());
		embedBuilder.setTitle("Music Player - NightBot")
			.addField(info.getLang().MUSIC_PLAYER_CURRENTLY_PLAYING.format(), cGuild.getPlayer().getPlayingTrackName() != null ? cGuild.getPlayer().getPlayingTrackName() : info.getLang().NOTHING.format(), false)
			.addField(info.getLang().MUSIC_PLAYER_NEXT_TRACK.format(), 
					cGuild.getPlayer().getNextTrack() != null 
					? (cGuild.getPlayer().getNextTrack().getInfo().title.equalsIgnoreCase("Unknown title") 
							? cGuild.getPlayer().getNextTrack().getInfo().title 
							: cGuild.getPlayer().getNextTrack().getIdentifier()) 
					: info.getLang().NOTHING.format(), false);
		
		messageBuilder.setEmbed(embedBuilder.build());
		
		info.getChannel().sendMessage(messageBuilder.build()).queue(msg -> {
			msg.addReaction("\u23EA").queue();
			msg.addReaction("\u23EF").queue();
			msg.addReaction("\u23E9").queue();
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "AudioControlsChannel", info.getChannel().getIdLong());
			this.getBot().setSetting(info.getGuild().get().getIdLong(), "AudioControlsMessage", msg.getIdLong());
			this.getBot().getConnectedGuild(info.getGuild().get()).setAudioControlsMessage(msg);
		});
		return true;
	}
	
}
