package de.nightloewe.DiscordBot.modules;

import java.awt.Color;
import java.util.LinkedList;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.command.CommandInfo;
import de.nightloewe.DiscordBot.command.Module;
import de.nightloewe.DiscordBot.command.annotations.Command;
import de.nightloewe.DiscordBot.command.annotations.RequireBotPermission;
import de.nightloewe.DiscordBot.command.annotations.RequireOwner;
import de.nightloewe.DiscordBot.entities.BotPermission;
import de.nightloewe.DiscordBot.entities.ConnectedGuild;
import de.nightloewe.DiscordBot.entities.Setting;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import de.nightloewe.DiscordBot.util.PermissionUtil;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;

public class PermissionsModule extends Module {

	public PermissionsModule(Bot bot) {
		super(bot);
	}

	@Override
	public String getName() {
		return "Permissions";
	}
	
	@RequireOwner
	@Command(name = "listGlobalPermissions", description = "LISTGLOBALPERMISSIONS_DESC", aliases = "lgp", usage = {"", "<page>"})
	public boolean listGlobalPermissions(CommandInfo info) {
		Setting botPrefix = this.getBot().getSetting(-1, "CommandPrefix");
		int page = 0;
		
		if(info.getArguments().length == 1) {
			try {
				page = Integer.parseInt(info.getArguments()[0]) - 1;
			} catch(NumberFormatException e) {
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().NEED_NATURAL_NUMBER.format())
						.setColor(Color.RED)
						.build()
						).queue();
				return true;
			}
		}

		
		LinkedList<BotPermission> globalPerms = this.getBot().getGlobalPermissions();
		
		int offset = page * 10;
		int max = (globalPerms.size() > offset + 10 ? offset + 10 : globalPerms.size());
		
		EmbedBuilder builder = new EmbedBuilder().setTitle("Global Permissions");
		
		for(int i = offset; i < max; i++) {
			BotPermission perm = globalPerms.get(i);
			
			StringBuilder permString = new StringBuilder();
			
			permString.append(botPrefix.getValue());
			
			if(perm.getAction().equalsIgnoreCase("enable") || perm.getAction().equalsIgnoreCase("disable")) {
				if(perm.getModule() != null) {
					permString.append("globalModulePerm " + perm.getModule() + " " + perm.getAction());
				} else {
					permString.append("globalCommandPerm " + perm.getCommand() + " " + perm.getAction());
				}
			} else {
				if(perm.getGuildId() != 0L) {
					permString.append("blacklistGuild " + perm.getGuildId() + " " + perm.getAction());
				} else {
					permString.append("blacklistUser " + perm.getUserId() + " " + perm.getAction());
				}
			}
			
			builder.appendDescription("**#" + perm.getId() + "** `" + permString.toString() + "` \n");
		}
		
		if(builder.getDescriptionBuilder().length() == 0) {
			builder.appendDescription("No permissions to display.");
		}
		
		info.getChannel().sendMessage(builder.setFooter((page + 1) + "/" + (globalPerms.size() < 10 ? 1 : (globalPerms.size()/10)), null).build()).queue();
		
		return true;
	}
	
	@RequireOwner
	@Command(name = "globalCommandPerm", description = "ADDGLOBALCOMMANDPERMISSION_DESC", usage = {"\"command\" enable", "\"command\" disable"}, aliases = "gcp")
	public boolean addGlobalCommandPermission(CommandInfo info) {
		if(info.getArguments().length == 2) {
			String command = info.getArguments()[0];
			String action = info.getArguments()[1];
			
			if(this.getBot().getCommandMap().getCommand(command) == null) {
				new BotEmbedBuilder().setDescription(info.getLang().COMMAND_NOT_FOUND.format(info.getAuthor().getAsMention(), command)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(action.equalsIgnoreCase("enable") || action.equalsIgnoreCase("disable")) {
				BotPermission botPermission = new BotPermission(0, command, action);
				int id = this.getBot().getDatabase().setPermission(botPermission);
				
				if(id != -1) {
					botPermission.setId(id);
					this.getBot().setPermission(botPermission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GLOBAL_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + command + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ENABLE_DISABLE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireOwner
	@Command(name = "globalModulePerm", description = "ADDGLOBALMODULEPERMISSION_DESC", usage = {"\"module\" enable", "\"module\" disable"}, aliases = "gmp")
	public boolean addGlobalModulePermission(CommandInfo info) {
		if(info.getArguments().length == 2) {
			String module = info.getArguments()[0];
			String action = info.getArguments()[1];
			
			if(!this.getBot().getModules().containsKey(module)) {
				new BotEmbedBuilder().setDescription(info.getLang().MODULE_NOT_FOUND.format(info.getAuthor().getAsMention(), module)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(!action.equalsIgnoreCase("enable") || !action.equalsIgnoreCase("disable")) {
				BotPermission botPermission = new BotPermission(0, module, (action.equalsIgnoreCase("enable") ? true : false));
				int id = this.getBot().getDatabase().setPermission(botPermission);
				
				if(id != -1) {
					botPermission.setId(id);
					this.getBot().setPermission(botPermission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GLOBAL_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + module + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ENABLE_DISABLE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			
			return true;
		}
		return false;
	}
	
	@RequireOwner
	@Command(name = "blacklistGuild", description = "ADDGUILDTOBLACKLIST_DESC", usage = {"GuildID", "@Owner"})
	public boolean addGuildToBlacklist(CommandInfo info) {
		if(info.getArguments().length == 1) {
			long guildId = -1L;
			
			if(info.getGuild().isPresent() && info.getMessage().getMentionedMembers().size() == 1) {
				Member member = info.getMessage().getMentionedMembers().get(0);
				
				if(member.isOwner()) {
					guildId = member.getGuild().getIdLong();
				}
			} else if(info.getArguments()[0].matches("(\\d){18}")) {
				try {
					guildId = Long.parseLong(info.getArguments()[0]);
				} catch(NumberFormatException e) {
					new BotEmbedBuilder().setDescription(info.getLang().NEED_NATURAL_NUMBER.format()).withErrorColor().sendMessage(info.getChannel());
					return true;
				}
			}
			
			if(guildId != -1L) {
				BotPermission permission = new BotPermission(0, guildId, "blacklist");
				
				if(PermissionUtil.isPermissionExisting(this.getBot().getGlobalPermissions(), permission))
					return true;
				
				int permissionId = this.getBot().getDatabase().setPermission(permission);
				
				permission.setId(permissionId);
				this.getBot().setPermission(permission);
				
				final long guildIdFinal = guildId;
				
				new BotEmbedBuilder().setDescription(info.getLang().ADDED_TO_BLACKLIST_GUILD.format(info.getAuthor().getAsMention(), String.valueOf(guildId).replaceAll("\\.", ""))).withOkColor().sendMessage(info.getChannel(), success -> {
					for(ConnectedGuild cGuild : this.getBot().getConnectedGuilds().values()) {
						if(cGuild.getGuild().getIdLong() == guildIdFinal) {
							cGuild.getGuild().leave().queue();
						}
					}
				}, failure -> {
					for(ConnectedGuild cGuild : this.getBot().getConnectedGuilds().values()) {
						if(cGuild.getGuild().getIdLong() == guildIdFinal) {
							cGuild.getGuild().leave().queue();
						}
					}
				});
				
				
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().GUILD_NOT_FOUND.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireOwner
	@Command(name = "blacklistUser", description = "ADDUSERTOBLACKLIST_DESC", usage = {"UserID", "@User"})
	public boolean addUserToBlacklist(CommandInfo info) {
		if(info.getArguments().length == 1) {
			long userId = -1L;
			
			if(info.getMessage().getMentionedUsers().size() == 1) {
				User user = info.getMessage().getMentionedUsers().get(0);
				userId = user.getIdLong();
			} else if(info.getArguments()[0].matches("(\\d){18}")) {
				try {
					userId = Long.parseLong(info.getArguments()[0]);
				} catch(NumberFormatException e) {
					new BotEmbedBuilder().setDescription(info.getLang().NEED_NATURAL_NUMBER.format()).withErrorColor().sendMessage(info.getChannel());
					return true;
				}
			}
			
			if(userId != -1L) {
				BotPermission permission = new BotPermission(0, userId, "blacklist");
				
				if(PermissionUtil.isPermissionExisting(this.getBot().getGlobalPermissions(), permission))
					return true;
				
				int permissionId = this.getBot().getDatabase().setPermission(permission);
				
				permission.setId(permissionId);
				this.getBot().setPermission(permission);
				
				new BotEmbedBuilder().setDescription(info.getLang().ADDED_TO_BLACKLIST_USER.format(info.getAuthor().getAsMention(), String.valueOf(userId).replaceAll("\\.", ""))).withOkColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireOwner
	@Command(name = "removeGlobalPerm", description = "REMOVEGLOBALPERM_DESC", usage = {"<id>"})
	public boolean removeGlobalPermission(CommandInfo info) {
		if(info.getArguments().length == 1) {
			int id = -1;
			
			try {
				id = Integer.parseInt(info.getArguments()[0]);
			} catch(NumberFormatException e) {
				new BotEmbedBuilder().setDescription(info.getLang().NEED_NATURAL_NUMBER.format()).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(id != -1) {
				BotPermission perm = null;
				for(BotPermission permission : this.getBot().getGlobalPermissions()) {
					if(permission.getId() == id) {
						perm = permission;
					}
				}
				
				if(perm != null) {
					this.getBot().getDatabase().removePermission(perm);
					this.getBot().removePermission(perm);
					new BotEmbedBuilder().setDescription(info.getLang().REMOVED_GLOBAL_PERMISSION.format(info.getAuthor().getAsMention(), id)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NOT_FOUND.format(info.getAuthor().getAsMention(), id)).withErrorColor().sendMessage(info.getChannel());
				}
			}
			return true;
		}
		return false;
	}
	
	@RequireBotPermission
	@Command(name = "permissions", description = "LISTPERMISSIONS_DESC", usage = {"<page>"})
	public boolean listPermissions(CommandInfo info) {
		Setting botPrefix = this.getBot().getSetting(-1, "CommandPrefix");
		int page = 0;
		
		if(info.getArguments().length == 1) {
			try {
				page = Integer.parseInt(info.getArguments()[0]) - 1;
			} catch(NumberFormatException e) {
				info.getChannel().sendMessage(
						new EmbedBuilder()
						.setDescription(info.getLang().NEED_NATURAL_NUMBER.format())
						.setColor(Color.RED)
						.build()
						).queue();
				return true;
			}
		}
		
		LinkedList<BotPermission> perms = this.getBot().getConnectedGuild(info.getGuild().get()).getPermissions();
		
		int offset = page * 10;
		int max = (perms.size() > offset + 10 ? offset + 10 : perms.size());
		
		EmbedBuilder builder = new EmbedBuilder().setTitle("Permissions");
		
		for(int i = offset; i < max; i++) {
			BotPermission permission = perms.get(i);
			StringBuilder permString = new StringBuilder();
			
			String module = permission.getModule();
			String command = permission.getCommand();
			String action = permission.getAction();
			
			permString.append(botPrefix.getValue());
			
			if(action.equalsIgnoreCase("enable") || action.equalsIgnoreCase("disable")) {
				if(module != null) {
					permString.append("modulePerm " + module + " " + action);
				} else {
					permString.append("commandPerm " + command + " " + action);
				}
			} else if(action.equalsIgnoreCase("allow") || action.equalsIgnoreCase("deny")) {
				if(permission.getChannelId() != 0L) {
					if(permission.getRoleId() != 0L) {
						Role role = info.getGuild().get().getRoleById(permission.getRoleId());
						TextChannel channel = info.getGuild().get().getTextChannelById(permission.getChannelId());
						if(module != null) {
							permString.append("roleChannelModulePerm " + (channel != null ? channel.getAsMention() : permission.getChannelId()) + " " + (role != null ? role.getName() : permission.getRoleId()) + " " + action);
						} else {
							permString.append("roleChannelCommandPerm " + (channel != null ? channel.getAsMention() : permission.getChannelId()) + " " + (role != null ? role.getName() : permission.getRoleId()) + " " + action);
						}
					} else {
						TextChannel channel = info.getGuild().get().getTextChannelById(permission.getChannelId());
						if(module != null) {
							permString.append("channelModulePerm " + (channel != null ? channel.getAsMention() : permission.getChannelId()) + " " + action);
						} else {
							permString.append("channelCommandPerm " + (channel != null ? channel.getAsMention() : permission.getChannelId()) + " " + action);
						}
					}
				} else {
					if(permission.getRoleId() != 0L) {
						Role role = info.getGuild().get().getRoleById(permission.getRoleId());
						if(module != null) {
							permString.append("roleModulePerm " + (role != null ? role.getName() : permission.getRoleId()) + " " + action);
						} else {
							permString.append("roleCommandPerm " + (role != null ? role.getName() : permission.getRoleId()) + " " + action);
						}
					} else {
						permString.append("userCommandPerm " + permission.getUserId() + " " + permission.getCommand() + " " + action);
					}
				}
			}
			
			builder.appendDescription("**#" + permission.getId() + "** `" + permString.toString() + "`");
		}
		
		info.getChannel().sendMessage(builder.build()).queue();
		
		return true;
	}
	
	@RequireBotPermission
	@Command(name = "commandPerm", description = "GUILDCOMMANDPERM_DESC", aliases = "gcommand", usage = {"<Command> enable", "<Command> disable"})
	public boolean addGuildCommandPermission(CommandInfo info) {
		if(info.getArguments().length == 2) {
			String command = info.getArguments()[0];
			String action = info.getArguments()[1];
			
			if(this.getBot().getCommandMap().getCommand(command) == null) {
				new BotEmbedBuilder().setDescription(info.getLang().COMMAND_NOT_FOUND.format(info.getAuthor().getAsMention(), info.getBotPrefix() + command)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(action.equalsIgnoreCase("enable") || action.equalsIgnoreCase("disable")) {
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				BotPermission permission = new BotPermission(0, cGuild.getGuild().getIdLong(), command, action);
				
				if(PermissionUtil.isPermissionExisting(cGuild.getPermissions(), permission))
					return true;
				
				int id = this.getBot().getDatabase().setPermission(permission);
				if(id != -1) {
					permission.setId(id);
					cGuild.setPermission(permission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GUILD_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + command + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
				
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ENABLE_DISABLE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireBotPermission
	@Command(name = "modulePerm", description = "GUILDNODULEPERM_DESC", aliases = "gmodule", usage = {"<Module> enable", "<Module> disable"})
	public boolean addGuildModulePermission(CommandInfo info) {
		if(info.getArguments().length == 2) {
			String module = info.getArguments()[0];
			String action = info.getArguments()[1];
			
			if(!this.getBot().getModules().containsKey(module)) {
				new BotEmbedBuilder().setDescription(info.getLang().MODULE_NOT_FOUND.format(info.getAuthor().getAsMention(), module)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(action.equalsIgnoreCase("enable") || action.equalsIgnoreCase("disable")) {
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				BotPermission permission = new BotPermission(0, cGuild.getGuild().getIdLong(), module, (action.equalsIgnoreCase("enable") ? true : false));
				
				if(PermissionUtil.isPermissionExisting(cGuild.getPermissions(), permission))
					return true;
				
				int id = this.getBot().getDatabase().setPermission(permission);
				if(id != -1) {
					permission.setId(id);
					cGuild.setPermission(permission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GUILD_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + module + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
				
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ENABLE_DISABLE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireBotPermission
	@Command(name = "channelCommand", description = "CHANNELCOMMANDPERM_DESC", aliases = "gcc", usage = {"<Command> allow", "<Command> deny"})
	public boolean addChannelCommandPermission(CommandInfo info) {
		if(info.getArguments().length == 2) {
			String command = info.getArguments()[0];
			String action = info.getArguments()[1];
			
			if(this.getBot().getCommandMap().getCommand(command) == null) {
				new BotEmbedBuilder().setDescription(info.getLang().COMMAND_NOT_FOUND.format(info.getAuthor().getAsMention(), info.getBotPrefix() + command)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(action.equalsIgnoreCase("allow") || action.equalsIgnoreCase("deny")) {
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				BotPermission permission = new BotPermission(0, cGuild.getGuild().getIdLong(), info.getChannel().getIdLong(), command, action);
				
				if(PermissionUtil.isPermissionExisting(cGuild.getPermissions(), permission))
					return true;
				
				int id = this.getBot().getDatabase().setPermission(permission);
				if(id != -1) {
					permission.setId(id);
					cGuild.setPermission(permission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GUILD_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + info.getChannel().getIdLong() + " " + command + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ALLOW_DENY.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireBotPermission
	@Command(name = "channelModule", description = "CHANNELMODULEPERM_DESC", aliases = "gcm", usage = {"<Module> allow", "<Module> deny"})
	public boolean addChannelModulePermission(CommandInfo info) {
		if(info.getArguments().length == 2) {
			String module = info.getArguments()[0];
			String action = info.getArguments()[1];
			
			if(!this.getBot().getModules().containsKey(module)) {
				new BotEmbedBuilder().setDescription(info.getLang().MODULE_NOT_FOUND.format(info.getAuthor().getAsMention(), module)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(action.equalsIgnoreCase("allow") || action.equalsIgnoreCase("deny")) {
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				BotPermission permission = new BotPermission(0, cGuild.getGuild().getIdLong(), info.getChannel().getIdLong(), module, (action.equalsIgnoreCase("allow") ? true : false));
				
				if(PermissionUtil.isPermissionExisting(cGuild.getPermissions(), permission))
					return true;
				
				int id = this.getBot().getDatabase().setPermission(permission);
				if(id != -1) {
					permission.setId(id);
					cGuild.setPermission(permission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GUILD_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + info.getChannel().getIdLong() + " " + module + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ALLOW_DENY.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireBotPermission
	@Command(name = "roleCommand", description = "ROLECOMMANDPERM_DESC", aliases = "rolecmd", usage = {"<RoleName> <Command> allow", "<RoleName> <Command> deny"})
	public boolean addRoleCommandPermission(CommandInfo info) {
		if(info.getArguments().length == 3) {
			String roleName = info.getArguments()[0];
			String command = info.getArguments()[1];
			String action = info.getArguments()[2];
			
			Role role = info.getGuild().get().getRolesByName(roleName, true).get(0);
			if(role == null) {
				new BotEmbedBuilder().setDescription(info.getLang().ROLE_NOT_FOUND.format(info.getAuthor().getAsMention(), roleName)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(this.getBot().getCommandMap().getCommand(command) == null) {
				new BotEmbedBuilder().setDescription(info.getLang().COMMAND_NOT_FOUND.format(info.getAuthor().getAsMention(), info.getBotPrefix() + command)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(action.equalsIgnoreCase("allow") || action.equalsIgnoreCase("deny")) {
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				BotPermission permission = new BotPermission(0, cGuild.getGuild().getIdLong(), command, role.getIdLong(), action);
				
				if(PermissionUtil.isPermissionExisting(cGuild.getPermissions(), permission))
					return true;
				
				int id = this.getBot().getDatabase().setPermission(permission);
				if(id != -1) {
					permission.setId(id);
					cGuild.setPermission(permission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GUILD_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + role.getName() + " " + command + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ALLOW_DENY.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireBotPermission
	@Command(name = "roleModule", description = "ROLEMODULEPERM_DESC", aliases = "rolemod", usage = {"<RoleName> <Module> allow", "<RoleName> <Module> allow"})
	public boolean addRoleModulePermission(CommandInfo info) {
		if(info.getArguments().length == 3) {
			String roleName = info.getArguments()[0];
			String module = info.getArguments()[1];
			String action = info.getArguments()[2];
			
			Role role = info.getGuild().get().getRolesByName(roleName, true).get(0);
			if(role == null) {
				new BotEmbedBuilder().setDescription(info.getLang().ROLE_NOT_FOUND.format(info.getAuthor().getAsMention(), roleName)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(!this.getBot().getModules().containsKey(module)) {
				new BotEmbedBuilder().setDescription(info.getLang().MODULE_NOT_FOUND.format(info.getAuthor().getAsMention(), module)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(action.equalsIgnoreCase("allow") || action.equalsIgnoreCase("deny")) {
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				BotPermission permission = new BotPermission(0, cGuild.getGuild().getIdLong(), module, role.getIdLong(), (action.equalsIgnoreCase("allow") ? true : false));
				
				if(PermissionUtil.isPermissionExisting(cGuild.getPermissions(), permission))
					return true;
				
				int id = this.getBot().getDatabase().setPermission(permission);
				if(id != -1) {
					permission.setId(id);
					cGuild.setPermission(permission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GUILD_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + role.getName() + " " + module + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ALLOW_DENY.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireBotPermission
	@Command(name = "roleChannelCommand", description = "ROLECHANNELCOMMANDPERM_DESC", aliases = "rcc", usage = {"<RoleName> <Command> allow", "<RoleName> <Command> deny"})
	public boolean addRoleChannelCommandPermission(CommandInfo info) {
		if(info.getArguments().length == 3) {
			String roleName = info.getArguments()[0];
			String command = info.getArguments()[1];
			String action = info.getArguments()[2];
			
			Role role = info.getGuild().get().getRolesByName(roleName, true).get(0);
			if(role == null) {
				new BotEmbedBuilder().setDescription(info.getLang().ROLE_NOT_FOUND.format(info.getAuthor().getAsMention(), roleName)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(this.getBot().getCommandMap().getCommand(command) == null) {
				new BotEmbedBuilder().setDescription(info.getLang().COMMAND_NOT_FOUND.format(info.getAuthor().getAsMention(), info.getBotPrefix() + command)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(action.equalsIgnoreCase("allow") || action.equalsIgnoreCase("deny")) {
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				BotPermission permission = new BotPermission(0, cGuild.getGuild().getIdLong(), info.getChannel().getIdLong(), role.getIdLong(), command, action);
				
				if(PermissionUtil.isPermissionExisting(cGuild.getPermissions(), permission))
					return true;
				
				int id = this.getBot().getDatabase().setPermission(permission);
				if(id != -1) {
					permission.setId(id);
					cGuild.setPermission(permission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GUILD_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + role.getName() + " " + command + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ALLOW_DENY.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireBotPermission
	@Command(name = "roleChannelModule", description = "ROLECHANNELMODULEPERM_DESC", aliases = "rcm", usage = {"<RoleName> <Command> allow", "<RoleName> <Command> deny"})
	public boolean addRoleChannelModulePermission(CommandInfo info) {
		if(info.getArguments().length == 3) {
			String roleName = info.getArguments()[0];
			String module = info.getArguments()[1];
			String action = info.getArguments()[2];
			
			Role role = info.getGuild().get().getRolesByName(roleName, true).get(0);
			if(role == null) {
				new BotEmbedBuilder().setDescription(info.getLang().ROLE_NOT_FOUND.format(info.getAuthor().getAsMention(), roleName)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(!this.getBot().getModules().containsKey(module)) {
				new BotEmbedBuilder().setDescription(info.getLang().MODULE_NOT_FOUND.format(info.getAuthor().getAsMention(), module)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(action.equalsIgnoreCase("allow") || action.equalsIgnoreCase("deny")) {
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				BotPermission permission = new BotPermission(0, cGuild.getGuild().getIdLong(), info.getChannel().getIdLong(), role.getIdLong(), module, (action.equalsIgnoreCase("allow") ? true : false));
				
				if(PermissionUtil.isPermissionExisting(cGuild.getPermissions(), permission))
					return true;
				
				int id = this.getBot().getDatabase().setPermission(permission);
				if(id != -1) {
					permission.setId(id);
					cGuild.setPermission(permission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GUILD_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + role.getName() + " " + module + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ALLOW_DENY.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireBotPermission
	@Command(name = "userCommandPerm", description = "USERCOMMANDPERM_DESC", aliases = "usercmd", usage = {"<UserID> <Command> allow/deny", "@User <Command> allow/deny"})
	public boolean addUserCommandPermission(CommandInfo info) {
		if(info.getArguments().length == 3) {
			User user = null;
			String command = info.getArguments()[1];
			String action = info.getArguments()[2];
			
			if(info.getMessage().getMentionedUsers().size() == 1) {
				user = info.getMessage().getMentionedUsers().get(0);
			} else if(info.getArguments()[1].matches("(\\d){18}")) {
				user = info.getChannel().getJDA().getUserById(info.getArguments()[0]);
			}
			
			if(user == null) {
				new BotEmbedBuilder().setDescription(info.getLang().NEED_USER.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(this.getBot().getCommandMap().getCommand(command) == null) {
				new BotEmbedBuilder().setDescription(info.getLang().COMMAND_NOT_FOUND.format(info.getAuthor().getAsMention(), info.getBotPrefix() + command)).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(action.equalsIgnoreCase("allow") || action.equalsIgnoreCase("deny")) {
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				BotPermission permission = new BotPermission(0, cGuild.getGuild().getIdLong(), (Long)user.getIdLong(), command, action);
				
				if(PermissionUtil.isPermissionExisting(cGuild.getPermissions(), permission))
					return true;
				
				int id = this.getBot().getDatabase().setPermission(permission);
				if(id != -1) {
					permission.setId(id);
					cGuild.setPermission(permission);
					new BotEmbedBuilder().setDescription(info.getLang().SET_GUILD_PERMISSION.format(info.getAuthor().getAsMention(), info.getBotPrefix() + info.getCommand() + " " + user.getName() + " " + command + " " + action)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().SET_PERMISSION_DATABASE_FAILURE.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
				}
			} else {
				new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NEED_ALLOW_DENY.format(info.getAuthor().getAsMention())).withErrorColor().sendMessage(info.getChannel());
			}
			return true;
		}
		return false;
	}
	
	@RequireBotPermission
	@Command(name = "removePerm", description = "REMOVEPERM_DESC", aliases = "rmperm", usage = "<id>")
	public boolean removePermission(CommandInfo info) {
		if(info.getArguments().length == 1) {
			int id = -1;
			
			try {
				id = Integer.parseInt(info.getArguments()[0]);
			} catch(NumberFormatException e) {
				new BotEmbedBuilder().setDescription(info.getLang().NEED_NATURAL_NUMBER.format()).withErrorColor().sendMessage(info.getChannel());
				return true;
			}
			
			if(id != -1) {
				BotPermission perm = null;
				ConnectedGuild cGuild = this.getBot().getConnectedGuild(info.getGuild().get());
				for(BotPermission permission : cGuild.getPermissions()) {
					if(permission.getId() == id) {
						perm = permission;
					}
				}
				
				if(perm != null) {
					this.getBot().getDatabase().removePermission(perm);
					cGuild.removePermission(perm);
					new BotEmbedBuilder().setDescription(info.getLang().REMOVED_GUILD_PERMISSION.format(info.getAuthor().getAsMention(), id)).withOkColor().sendMessage(info.getChannel());
				} else {
					new BotEmbedBuilder().setDescription(info.getLang().PERMISSION_NOT_FOUND.format(info.getAuthor().getAsMention(), id)).withErrorColor().sendMessage(info.getChannel());
				}
			}
			return true;
		}
		return false;
	}

}
