package de.nightloewe.DiscordBot.modules;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mixer.api.MixerAPI;
import com.mixer.api.resource.MixerUser;
import com.mixer.api.resource.channel.MixerChannel;
import com.mixer.api.resource.interactive.InteractiveGame;
import com.mixer.api.response.users.UserSearchResponse;
import com.mixer.api.services.impl.ChannelsService;
import com.mixer.api.services.impl.InteractiveService;
import com.mixer.api.services.impl.UsersService;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.command.CommandInfo;
import de.nightloewe.DiscordBot.command.Module;
import de.nightloewe.DiscordBot.command.annotations.Command;
import de.nightloewe.DiscordBot.command.annotations.RequireGuild;
import de.nightloewe.DiscordBot.command.annotations.RequireOwner;
import de.nightloewe.DiscordBot.command.annotations.RequireUserPermission;
import de.nightloewe.DiscordBot.entities.ConnectedGuild;
import de.nightloewe.DiscordBot.entities.TwitchSubscription;
import de.nightloewe.DiscordBot.entities.TwitchSubscription.SubscriptionType;
import de.nightloewe.DiscordBot.util.BotEmbedBuilder;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.webhook.WebhookClient;
import net.dv8tion.jda.webhook.WebhookClientBuilder;

public class TwitchModule extends Module {

	/** Twitch Secret: mrp4vbmicrfdonqazic3ejlcxybvwe */
	
	private String twitchToken;
	private int expirationSeconds;
	
	private TimerTask twitchRefreshTask;
	private Timer timer;
	
	private MixerAPI mixer = new MixerAPI(Bot.getInstance().getConfig().getMixerClientSecret());
	
	public TwitchModule(Bot bot) {
		super(bot);
		
		this.retrieveToken();
		//this.refreshAllSubscriptions();
	}
	
	private void retrieveToken() {
		try {
			URL url = new URL("https://api.twitch.tv/kraken/oauth2/token");
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("User-Agent", "DiscordBot");
			conn.setRequestMethod("POST");
			
			String urlParameters = "client_id=" + this.getBot().getConfig().getTwitchClientId() + "&client_secret=" + this.getBot().getConfig().getTwitchClientSecret() + "&grant_type=client_credentials&scope=user:edit+user:read:email";
			byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
			
			conn.setRequestProperty("Content-Length", Integer.toString(postData.length));
			conn.setDoOutput(true);
			
			try(DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(postData);
			}
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			StringBuilder builder = new StringBuilder();
			
			String line = "";
			
			while((line = reader.readLine()) != null) {
				builder.append(line);
			}
			
			String response = builder.toString();
			
			JsonElement jElement = new JsonParser().parse(response);
			JsonObject jObject = jElement.getAsJsonObject();
			
			this.twitchToken = jObject.get("access_token").getAsString();
			this.expirationSeconds = jObject.get("expires_in").getAsInt();
			
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {

				@Override
				public void run() {
					TwitchModule.this.retrieveToken();
				}
				
			}, this.expirationSeconds * 1000);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void refreshAllSubscriptions() {
		for(ConnectedGuild guild : Bot.getInstance().getConnectedGuilds().values()) {
			this.unsubscribe(guild);
			this.refreshSubscriptions(guild);
		}
		
		this.timer = new Timer();
		timer.schedule(this.twitchRefreshTask = new TimerTask() {
			
			@Override
			public void run() {
				TwitchModule.this.refreshAllSubscriptions();
			}
		}, 864000 * 1000);
	}
	
	private void refreshSubscriptions(ConnectedGuild guild) {
		for(TwitchSubscription subscription : guild.getTwitchSubscriptions()) {
			if(subscription.getType() == SubscriptionType.TWITCH) {
				try {
					URL url = new URL("https://api.twitch.tv/helix/webhooks/hub");
					
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestProperty("User-Agent", "DiscordBot");
					conn.setRequestProperty("Content-Type", "application/json");
					conn.setRequestProperty("Client-ID", this.getBot().getConfig().getTwitchClientId());
					conn.setRequestMethod("POST");
					
					JSONObject object = new JSONObject();
					object.put("hub.callback", "http://209.250.232.210/twitch_callback/" + guild.getGuild().getIdLong());
					object.put("hub.mode", "subscribe");
					object.put("hub.topic", String.format(subscription.getType().getUrlFormat(), subscription.getUserId()));
					object.put("hub.secret", subscription.getSecret());
					object.put("hub.lease_seconds", 864000);
					
					Bot.getInstance().getLogger().error(object.toString());
					
					byte[] postData = object.toString().getBytes(StandardCharsets.UTF_8);
					
					conn.setRequestProperty("Content-Length", Integer.toString(postData.length));
					conn.setDoOutput(true);
					
					try(DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
						wr.write(postData);
					}
					
					if(conn.getResponseCode() != 202) {
						int responseCode = conn.getResponseCode();
						guild.getGuild().getOwner().getUser().openPrivateChannel().queue(success -> {
							success.sendMessage(new BotEmbedBuilder().withErrorColor().addField("Exception", "Could not verify", false).appendDescription("Twitch declined subscription. Response code: " + responseCode).build()).queue();
						});
					} else {
						guild.getGuild().getOwner().getUser().openPrivateChannel().queue(success -> {
							success.sendMessage(new BotEmbedBuilder().withErrorColor().appendDescription("Twitch accepted subscription").build()).queue();
						});
					}
				} catch (IOException e) {
					e.printStackTrace();
					
					guild.getGuild().getOwner().getUser().openPrivateChannel().queue(success -> {
						success.sendMessage(new BotEmbedBuilder().withErrorColor().addField("Exception", e.toString(), false).appendDescription(e.getLocalizedMessage()).build()).queue();
					});
				}
			} else {
				//Mixer
				if(!subscription.isInitialized()) {
					Bot.getInstance().getLogger().error("TEST3");
					
					this.refreshSubscriptionMixer(subscription, guild);
				}
			}
		}
	}
	
	private void refreshSubscriptionMixer(TwitchSubscription subscription, ConnectedGuild guild) {
		try {
			URL tokenUrl = new URL("https://mixer.com/api/v1/hooks");
			
			HttpsURLConnection tokenConn = (HttpsURLConnection) tokenUrl.openConnection();
			tokenConn.setRequestProperty("User-Agent", "DiscordBot");
			tokenConn.setRequestProperty("Content-Type", "application/json");
			tokenConn.setRequestProperty("Client-ID", this.getBot().getConfig().getMixerClientId());
			tokenConn.setRequestProperty("Authorization", "Secret " + this.getBot().getConfig().getMixerClientSecret());
			tokenConn.connect();
			
			String csrfToken = tokenConn.getHeaderField("X-CSRF-Token");
			
			URL url = new URL("https://mixer.com/api/v1/hooks");
			
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestProperty("User-Agent", "DiscordBot");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Client-ID", this.getBot().getConfig().getMixerClientId());
			conn.setRequestProperty("Authorization", "Secret " + this.getBot().getConfig().getMixerClientSecret());
			//conn.setRequestProperty("X-CSRF-Token", csrfToken);
			conn.setRequestMethod("POST");
			
			JSONObject object = new JSONObject();
			object.put("url", "http://209.250.232.210/mixer_callback/" + guild.getGuild().getIdLong());
			object.put("kind", "web");
			JSONArray array = new JSONArray();
			array.put("channel:" + subscription.getUserId() + ":broadcast");
			object.put("events", array);
			
			Bot.getInstance().getLogger().error(object.toString());
			
			byte[] postData = object.toString().getBytes(StandardCharsets.UTF_8);
			
			conn.setRequestProperty("Content-Length", Integer.toString(postData.length));
			conn.setDoOutput(true);
			
			try(DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
				wr.write(postData);
			}
			
			subscription.setInitialized(true);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			StringBuilder builder = new StringBuilder();
			
			String line = "";
			
			while((line = reader.readLine()) != null) {
				builder.append(line);
			}
			
			String response = builder.toString();
			
			Bot.getInstance().getLogger().error(response);
			
			JsonElement jElement = new JsonParser().parse(response);
			JsonObject jObject = jElement.getAsJsonObject();
			
			String dateStr = jObject.get("expiresAt").getAsString();
			String id = jObject.get("id").getAsString();
			java.util.Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dateStr);
			
			subscription.setExpiresAt(dateStr);
			subscription.setId(id);
			
			Bot.getInstance().getDatabase().insertTwitchSubscription(guild.getGuild().getIdLong(), subscription);
			
			this.timer.schedule(new TimerTask() {
				
				@Override
				public void run() {
					TwitchModule.this.refreshSubscriptionMixer(subscription, guild);
				}
			}, date);
		} catch (IOException e) {
			e.printStackTrace();
			
			guild.getGuild().getOwner().getUser().openPrivateChannel().queue(success -> {
				success.sendMessage(new BotEmbedBuilder().withErrorColor().addField("Exception", e.toString(), false).appendDescription(e.getLocalizedMessage()).build()).queue();
			});
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	private void unsubscribe(ConnectedGuild guild) {
		for(TwitchSubscription subscription : guild.getTwitchSubscriptions()) {
			if(subscription.getType() == SubscriptionType.TWITCH) {
				try {
					URL url = new URL("https://api.twitch.tv/helix/webhooks/hub");
					
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestProperty("User-Agent", "DiscordBot");
					conn.setRequestProperty("Content-Type", "application/json");
					conn.setRequestProperty("Client-ID", this.getBot().getConfig().getTwitchClientId());
					conn.setRequestMethod("POST");
					
					JSONObject object = new JSONObject();
					object.put("hub.callback", "http://209.250.232.210/twitch_callback/" + guild.getGuild().getIdLong());
					object.put("hub.mode", "unsubscribe");
					object.put("hub.topic", String.format(subscription.getType().getUrlFormat(), subscription.getUserId()));
					object.put("hub.secret", subscription.getSecret());
					
					byte[] postData = object.toString().getBytes(StandardCharsets.UTF_8);
					
					conn.setRequestProperty("Content-Length", Integer.toString(postData.length));
					conn.setDoOutput(true);
					
					try(DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
						wr.write(postData);
					}
					
					if(conn.getResponseCode() != 202) {
//						guild.getGuild().getOwner().getUser().openPrivateChannel().queue(success -> {
//							success.sendMessage(new BotEmbedBuilder().withErrorColor().addField("Exception", "Could not verify", false).appendDescription("Twitch declined unsubscribe").build()).queue();
//						});
						
						Bot.getInstance().getLogger().error("Response code:" + conn.getResponseCode());
					}
				} catch (IOException e) {
					e.printStackTrace();
					
					guild.getGuild().getOwner().getUser().openPrivateChannel().queue(success -> {
						success.sendMessage(new BotEmbedBuilder().withErrorColor().addField("Exception", e.toString(), false).appendDescription(e.getLocalizedMessage()).build()).queue();
					});
				}
			}
		}
		
		if(this.twitchRefreshTask != null) {
			this.twitchRefreshTask.cancel();
		}
	}

	@Override
	public String getName() {
		return "Twitch Integration";
	}
	
	@RequireUserPermission(guildPermission = Permission.KICK_MEMBERS)
	@Command(name = "twitchid", description = "TWITCHID_DESC", usage = "username")
	public boolean lookupId(CommandInfo info) {
		if(info.getArguments().length == 1) {
			String loginName = info.getArguments()[0];
			try {
				URL url = new URL("https://api.twitch.tv/helix/users?login=" + loginName);
				
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestProperty("User-Agent", "DiscordBot");
				conn.setRequestProperty("Client-ID", this.getBot().getConfig().getTwitchClientId());
				conn.setRequestProperty("Authorization", "Bearer " + this.twitchToken);
				
				BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				
				StringBuilder builder = new StringBuilder();
				
				String line = "";
				
				while((line = reader.readLine()) != null) {
					builder.append(line);
				}
				
				String response = builder.toString();
				
				JsonElement jElement = new JsonParser().parse(response);
				JsonObject jObject = jElement.getAsJsonObject();
				
				JsonArray dataArray = jObject.get("data").getAsJsonArray();
				JsonObject dataObject = dataArray.get(0).getAsJsonObject();
				
				String id = dataObject.get("id").getAsString();
				
				info.getChannel().sendMessage(new EmbedBuilder().setDescription("Die Twitch-ID von " + info.getArguments()[0] + " ist: " + id).setColor(Color.getHSBColor(259, 55, 47)).build()).queue();
			} catch (IOException e) {
				e.printStackTrace();
				info.getChannel().sendMessage(new EmbedBuilder().setDescription("Es ist ein Fehler aufgetreten, bitte versuche es nochmal erneut oder kontaktiere einen Administrator.").setColor(Color.RED).build()).queue();
			}
			return true;
		}
		return false;
	}
	
	@RequireUserPermission(guildPermission = Permission.KICK_MEMBERS)
	@Command(name = "mixerid", description = "MIXERID_DESC", usage = "username")
	public boolean mixerLookupId(CommandInfo info) {
		if(info.getArguments().length == 1) {
			String loginName = info.getArguments()[0];
			try {
				UserSearchResponse search = mixer.use(UsersService.class).search(loginName).get(2L, TimeUnit.SECONDS);
				
				if(search.size() > 0) {
					MixerChannel channel = search.get(0).channel;
					
					info.getChannel().sendMessage(new EmbedBuilder().setDescription("Die Mixer-ID von " + info.getArguments()[0] + " ist: " + channel.id).setColor(Color.getHSBColor(259, 55, 47)).build()).queue();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} catch (TimeoutException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
	
	@RequireOwner
	@RequireGuild
	@Command(name = "addtwitchsubscription", description = "ADDTWITCHSUBSCRIPTION_DESC", usage = "name userid secret webhook")
	public boolean addTwitchSubscription(CommandInfo info) {
		if(info.getArguments().length == 4) {
			String name = info.getArguments()[0];
			String userIdString = info.getArguments()[1];
			String secret = info.getArguments()[2];
			String webhookURL = info.getArguments()[3];
			
			try {
				int userId = Integer.parseInt(userIdString);
				
				ConnectedGuild guild = this.getBot().getConnectedGuild(info.getGuild().get());
				guild.addTwitchSubscription(name, userId, secret, SubscriptionType.TWITCH, webhookURL);
				
				this.getBot().getDatabase().insertTwitchSubscription(info.getGuild().get().getIdLong(), guild.getTwitchSubscription(name, SubscriptionType.TWITCH));
				
				this.unsubscribe(guild);
				this.refreshSubscriptions(guild);
				
				info.getChannel().sendMessage("Subscribed!").queue();
			} catch(NumberFormatException ex) {
				info.getChannel().sendMessage("Please write a number!").queue();
			}
			
			return true;
		}
		
		return false;
	}
	
	@RequireOwner
	@RequireGuild
	@Command(name = "removetwitchsubscription", description = "REMOVETWITCHSUBSCRIPTION_DESC", usage = "name")
	public boolean removeTwitchSubscription(CommandInfo info) {
		if(info.getArguments().length == 1) {
			String name = info.getArguments()[0];
			
			ConnectedGuild guild = this.getBot().getConnectedGuild(info.getGuild().get());
			guild.removeTwitchSubscription(name, SubscriptionType.TWITCH);
			
			this.getBot().getDatabase().removeTwitchSubscriptions(info.getGuild().get().getIdLong(), name, SubscriptionType.TWITCH.name());
			
			this.unsubscribe(guild);
			this.refreshSubscriptions(guild);
			
			info.getChannel().sendMessage("Unsubscribed!").queue();
			
			return true;
		}
		
		return false;
	}
	
	@RequireOwner
	@RequireGuild
	@Command(name = "showtwitchsubscriptions", description = "SHOWTWITCHSUBSCRIPTIONS_DESC", usage = "")
	public boolean showTwitchSubscriptions(CommandInfo info) {
		ConnectedGuild guild = this.getBot().getConnectedGuild(info.getGuild().get());
		
		BotEmbedBuilder builder = new BotEmbedBuilder().setTitle("Twitch Subscriptions:");
		
		for(TwitchSubscription subscription : guild.getTwitchSubscriptions()) {
			if(subscription.getType() == SubscriptionType.TWITCH) {
				builder.addField(subscription.getName(), "ID:" + subscription.getUserId() + ", Webhook: " + subscription.getWebhookURL(), false);
			}
		}
		
		builder.sendMessage(info.getChannel());
		
		return true;
	}
	
	@RequireOwner
	@RequireGuild
	@Command(name = "addmixersubscription", description = "ADDMIXERSUBSCRIPTION_DESC", usage = "name userid secret webhook")
	public boolean addMixerSubscription(CommandInfo info) {
		if(info.getArguments().length == 4) {
			String name = info.getArguments()[0];
			String userIdString = info.getArguments()[1];
			String secret = info.getArguments()[2];
			String webhookURL = info.getArguments()[3];
			
			try {
				int userId = Integer.parseInt(userIdString);
				
				ConnectedGuild guild = this.getBot().getConnectedGuild(info.getGuild().get());
				guild.addTwitchSubscription(name, userId, secret, SubscriptionType.MIXER, webhookURL);
				
				this.getBot().getDatabase().insertTwitchSubscription(info.getGuild().get().getIdLong(), guild.getTwitchSubscription(name, SubscriptionType.MIXER));
				
				this.refreshSubscriptionMixer(guild.getTwitchSubscription(name, SubscriptionType.MIXER), guild);
				
				info.getChannel().sendMessage("Subscribed!").queue();
			} catch(NumberFormatException ex) {
				info.getChannel().sendMessage("Please write a number!").queue();
			}
			
			return true;
		}
		
		return false;
	}
	
	@RequireOwner
	@RequireGuild
	@Command(name = "removemixersubscription", description = "REMOVEMIXERSUBSCRIPTION_DESC", usage = "name")
	public boolean removeMixerSubscription(CommandInfo info) {
		if(info.getArguments().length == 1) {
			String name = info.getArguments()[0];
			
			ConnectedGuild guild = this.getBot().getConnectedGuild(info.getGuild().get());
			guild.removeTwitchSubscription(name, SubscriptionType.MIXER);
			
			this.getBot().getDatabase().removeTwitchSubscriptions(info.getGuild().get().getIdLong(), name, SubscriptionType.MIXER.name());
			
			info.getChannel().sendMessage("Unsubscribed!").queue();
			
			return true;
		}
		
		return false;
	}
	
	@RequireOwner
	@RequireGuild
	@Command(name = "showmixersubscriptions", description = "SHOWMIXERSUBSCRIPTIONS_DESC", usage = "")
	public boolean showMixerSubscriptions(CommandInfo info) {
		ConnectedGuild guild = this.getBot().getConnectedGuild(info.getGuild().get());
		
		BotEmbedBuilder builder = new BotEmbedBuilder().setTitle("Mixer Subscriptions:");
		
		for(TwitchSubscription subscription : guild.getTwitchSubscriptions()) {
			if(subscription.getType() == SubscriptionType.MIXER) {
				builder.addField(subscription.getName(), "ID:" + subscription.getUserId() + ", Webhook: " + subscription.getWebhookURL() + ", Id: " + subscription.getId(), false);
			}
		}
		
		builder.sendMessage(info.getChannel());
		
		return true;
	}
	
	public static class TwitchResponseServlet extends HttpServlet {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			String pathInfo = req.getPathInfo();
			pathInfo = pathInfo.substring(1, pathInfo.length());
			
			Bot.getInstance().getLogger().error("Got request.");
			
			try {
				long guildId = Long.parseLong(pathInfo);
				
				if(Bot.getInstance().getJDA().getGuildById(guildId) != null) {
					Guild guild = Bot.getInstance().getJDA().getGuildById(guildId);
					
					if(Bot.getInstance().getConnectedGuild(guild) != null) {
						ConnectedGuild cGuild = Bot.getInstance().getConnectedGuild(guild);
						
						try(BufferedReader reader = new BufferedReader(new InputStreamReader(req.getInputStream()))) {
							String line = reader.readLine();
							
							JSONObject object = new JSONObject(line);
							
							if(object.has("data")) {
								JSONArray array = object.getJSONArray("data");
								
								if(array.length() > 0) {
									JSONObject data = array.getJSONObject(0);
									
									int userId = data.getInt("user_id");
									
									if(data.getString("type").equalsIgnoreCase("live")) {
										for(TwitchSubscription subscription : cGuild.getTwitchSubscriptions()) {
											if(subscription.getType() == SubscriptionType.TWITCH) {
												if(subscription.getUserId() == userId) {
													String webhookURL = subscription.getWebhookURL();
													
													WebhookClientBuilder builder = new WebhookClientBuilder(webhookURL);
													WebhookClient client = builder.build();
													
													String game = this.getGame(data.getInt("game_id"));
													
													if(game != null) {
														MessageEmbed embed = new BotEmbedBuilder()
																.setColor(Color.MAGENTA)
																.setThumbnail(data.getString("thumbnail_url"))
																.setTitle(data.getString("title"), "http://twitch.tv/" + subscription.getName())
																.setAuthor(subscription.getName(), "http://twitch.tv/" + subscription.getName())
																.setDescription(subscription.getName() + " is live with '" + game + "'").build();
														
														client.send(embed);
														client.close();
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			} catch(NumberFormatException ex) {
				ex.printStackTrace();
			}
		}
		
		public String getGame(int gameId) {
			try {
				URL url = new URL("https://api.twitch.tv/helix/games?id=" + gameId);
				
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestProperty("User-Agent", "DiscordBot");
				conn.setRequestProperty("Client-ID", Bot.getInstance().getConfig().getTwitchClientId());
				conn.setRequestProperty("Authorization", "Bearer " + Bot.getInstance().getConfig().getTwitchClientSecret());
				
				BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				
				StringBuilder builder = new StringBuilder();
				
				String line = "";
				
				while((line = reader.readLine()) != null) {
					builder.append(line);
				}
				
				String response = builder.toString();
				
				JsonElement jElement = new JsonParser().parse(response);
				JsonObject jObject = jElement.getAsJsonObject();
				
				JsonArray array = jObject.getAsJsonArray("data");
				jObject = array.get(0).getAsJsonObject();
				
				String name = jObject.get("name").getAsString();
				
				return name;
			} catch(Exception e) {
				e.printStackTrace();
				
				return null;
			}
		}
		
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			if(req.getParameter("hub.mode") != null) {
				if(req.getParameter("hub.mode").equalsIgnoreCase("subscribe")) {
					Bot.getInstance().getLogger().error("Accepted subscription request: " + req.getParameter("hub.topic"));
					Bot.getInstance().getLogger().error("Subscription challenge: " + req.getParameter("hub.challenge"));
					
					resp.getWriter().write(req.getParameter("hub.challenge"));
					resp.setStatus(HttpServletResponse.SC_OK);
				} else if(req.getParameter("hub.mode").equalsIgnoreCase("denied")) {
					Bot.getInstance().getLogger().error("Denied subscription request: " + req.getParameter("hub.topic"));
				}
			}
		}
	}
	
	public static class MixerResponseServlet extends HttpServlet {
		
		private MixerAPI mixer = new MixerAPI(Bot.getInstance().getConfig().getMixerClientSecret());
		
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			String pathInfo = req.getPathInfo();
			pathInfo = pathInfo.substring(1, pathInfo.length());
			
			Bot.getInstance().getLogger().error("Got request.");
			
			try {
				long guildId = Long.parseLong(pathInfo);
				
				if(Bot.getInstance().getJDA().getGuildById(guildId) != null) {
					Guild guild = Bot.getInstance().getJDA().getGuildById(guildId);
					
					if(Bot.getInstance().getConnectedGuild(guild) != null) {
						ConnectedGuild cGuild = Bot.getInstance().getConnectedGuild(guild);
						
						try(BufferedReader reader = new BufferedReader(new InputStreamReader(req.getInputStream()))) {
							String line = reader.readLine();
							
							JSONObject object = new JSONObject(line);
							
							if(object.has("event")) {
								String[] data = object.getString("event").split(":");
								
								if(data.length == 3 && data[2].equalsIgnoreCase("broadcast")) {
									for(TwitchSubscription subscription : cGuild.getTwitchSubscriptions()) {
										if(subscription.getType() == SubscriptionType.MIXER) {
											if(subscription.getUserId() == Long.parseLong(data[1])) {
												MixerChannel channel = mixer.use(ChannelsService.class).findOne(Integer.parseInt(data[1])).get(2L, TimeUnit.SECONDS);
												
												Bot.getInstance().getLogger().error("TEST");
												
												if(channel.online) {
													
													String webhookURL = subscription.getWebhookURL();
													
													WebhookClientBuilder builder = new WebhookClientBuilder(webhookURL);
													WebhookClient client = builder.build();
													
													MessageEmbed embed = null;
													
													if(channel.interactive) {
														InteractiveGame game = mixer.use(InteractiveService.class).getInteractiveGame(channel.interactiveGameId).get(2L, TimeUnit.SECONDS);
														
														BotEmbedBuilder embedBuilder = (BotEmbedBuilder) new BotEmbedBuilder()
														.setColor(Color.MAGENTA)
														.setTitle(channel.token, "http://mixer.com/" + subscription.getName())
														.setAuthor(subscription.getName(), "http://mixer.com/" + subscription.getName())
														.setDescription(subscription.getName() + " is live with '" + game.name + "'");
														
														if(channel.thumbnail != null) {
															embedBuilder.setThumbnail(channel.thumbnail.url);
														}
														
														embed = embedBuilder.build();
													} else {
														BotEmbedBuilder embedBuilder = (BotEmbedBuilder) new BotEmbedBuilder()
														.setColor(Color.MAGENTA)
														.setTitle(channel.token, "http://mixer.com/" + subscription.getName())
														.setAuthor(subscription.getName(), "http://mixer.com/" + subscription.getName())
														.setDescription(subscription.getName() + " is live with '" + channel.type.name + "'");
														
														if(channel.thumbnail != null) {
															embedBuilder.setThumbnail(channel.thumbnail.url);
														}
														
														embed = embedBuilder.build();
													}
													
													client.send(embed);
													client.close();
												}
											}
										}
									}
								}
							}
						} catch (InterruptedException e) {
							e.printStackTrace();
						} catch (ExecutionException e) {
							e.printStackTrace();
						} catch (TimeoutException e) {
							e.printStackTrace();
						}
					}
				}
			} catch(NumberFormatException ex) {
				ex.printStackTrace();
			}
		}
		
	}

}
