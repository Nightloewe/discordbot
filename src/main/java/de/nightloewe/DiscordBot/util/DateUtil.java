package de.nightloewe.DiscordBot.util;

import java.util.Date;

public class DateUtil {

	public static final long SECOND = 1000L;
	public static final long MINUTE = 60000L;
	public static final long HOUR = 3600000L;
	public static final long DAY = 86400000L;
	public static final long MONTH = 2628000000L;
	public static final long YEAR = 31536000000L;
	
	public static Date stringToDate(String timeDiff) {
		String[] durations = timeDiff.split("(?<=[yMhms])");
		
		long time = 0L;
		int length = durations.length; 
		
		while(length > 0) {
			String dur = durations[length- 1];
			
			if(dur.matches("(\\d+)y")) {
				int duration = Integer.parseInt(dur.substring(0, dur.length() - 1));
				time = time + duration * YEAR;
			} else if(dur.matches("(\\d+)M")) {
				int duration = Integer.parseInt(dur.substring(0, dur.length() - 1));
				time = time + duration * MONTH;
			} else if(dur.matches("(\\d+)d")) {
				int duration = Integer.parseInt(dur.substring(0, dur.length() - 1));
				time = time + duration * DAY;
			} else if(dur.matches("(\\d+)h")) {
				int duration = Integer.parseInt(dur.substring(0, dur.length() - 1));
				time = time + duration * HOUR;
			} else if(dur.matches("(\\d+)m")) {
				int duration = Integer.parseInt(dur.substring(0, dur.length() - 1));
				time = time + duration * MINUTE;
			} else if(dur.matches("(\\d+)s")) {
				int duration = Integer.parseInt(dur.substring(0, dur.length() - 1));
				time = time + duration * SECOND;
			}
			
			length--;
		}
		
		return new Date(time);
	}
	
	public static int milliSecondsToDays(long time) {
		return (int) (time / 86400000L);
	}
	
	public static String getRemainingTimeString(long time) {
		StringBuilder builder = new StringBuilder();
		
		int years = (int) (time / YEAR);
		int days = (int) ((time % YEAR) / DAY);
		int hours = (int) ((time % DAY) / HOUR);
		int minutes = (int) ((time % HOUR) / MINUTE);
		int seconds = (int) ((time % MINUTE) / SECOND);
		
		if(years > 0)
			builder.append(years + " Jahre ");
		if(days > 0)
			builder.append(days + " Tage ");
		if(hours > 0)
			builder.append(hours + " Stunde(n) ");
		if(minutes > 0)
			builder.append(minutes + " Minute(n) ");
		if(seconds > 0)
			builder.append(seconds + " Sekunde(n)");
		
		if(time == 0) {
			builder.append("nicht");
		}
		
		return builder.toString();
	}
	
	
}
