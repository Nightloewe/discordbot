package de.nightloewe.DiscordBot.web;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

public class TemplateBuilder {

	private Template temp = null;
	
	private Configuration cfg;
	private HashMap<String, Object> replacements = new HashMap<String, Object>();
	
	public TemplateBuilder(Configuration cfg, String templateName) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException {
		this.cfg = cfg;
		this.temp = this.cfg.getTemplate(templateName);
	}
	
	public TemplateBuilder assign(String var, Object obj) {
		this.replacements.put(var, obj);
		
		return this;
	}
	
	public TemplateBuilder remove(String var) {
		this.replacements.remove(var);
		
		return this;
	}
	
	public Template getTemplate() {
		return this.temp;
	}
	
	public String build() throws TemplateProcessException {
		if(this.temp != null) {
			Template temp = this.getTemplate();
			StringWriter out = new StringWriter();
			
			try {
				temp.process(this.replacements, out);
			} catch (TemplateException e) {
				throw new TemplateProcessException(e);
			} catch (IOException e) {
				throw new TemplateProcessException(e);
			}
			
			String processedString = out.toString();
			
			if(this.replacements.containsKey("navbarActive") && processedString.contains("")) {
				
			}
			
			return out.toString();
		} else {
			return null;
		}
	}

}
