package de.nightloewe.DiscordBot.web;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

public class TemplateManager {

	private Configuration cfg;
	
	public TemplateManager() {
		this.cfg = new Configuration(Configuration.VERSION_2_3_23);
		try {
			this.cfg.setDirectoryForTemplateLoading(new File("./web/", "templates"));
			this.cfg.setDefaultEncoding("UTF-8");
			this.cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
			this.cfg.setLogTemplateExceptions(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public TemplateBuilder newBuilder(String templateName) throws TemplateProcessException {
		try {
			TemplateBuilder builder = new TemplateBuilder(this.cfg, templateName);
			return builder;
		} catch (IOException e) {
			throw new TemplateProcessException(e);
		}
	}
	
	public void sendPermissionError(HttpServletResponse resp) throws IOException {
		TemplateBuilder builder = this.newBuilder("errors/no_permission.tpl");
		String temp = builder.build();
		
		resp.getWriter().write(temp);
	}
	
}
