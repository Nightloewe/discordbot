package de.nightloewe.DiscordBot.web;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.nightloewe.DiscordBot.Bot;
import de.nightloewe.DiscordBot.modules.TwitchModule;

public class WebService {

	private Gson gson;
	
	private Server webServer;
	
	private TemplateManager tmpManager;

	private Bot bot;
	
	public Gson getGson() {
		return gson;
	}
	
	public TemplateManager getTmpManager() {
		return tmpManager;
	}
	
	public Server getWebServer() {
		return webServer;
	}
	
	public WebService(Bot bot) {
		this.gson = new GsonBuilder().setPrettyPrinting().create();
		this.tmpManager = new TemplateManager();
		this.bot = bot;
	}
	
	public void start() {
		QueuedThreadPool threadPool = new QueuedThreadPool(this.bot.getConfig().getWebMaxThreads(), this.bot.getConfig().getWebMinThreads());
		
		this.webServer = new Server(threadPool);
		
		ServerConnector connector = new ServerConnector(this.webServer);
		connector.setPort(this.bot.getConfig().getWebPort());
		
		this.webServer.addConnector(connector);
		
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		
		ServletHolder staticHolder = new ServletHolder("static", DefaultServlet.class);
		staticHolder.setInitParameter("resourceBase", "./web/static/");
		staticHolder.setInitParameter("dirAllowed", "false");
		staticHolder.setInitParameter("pathInfoOnly", "true");
		context.addServlet(staticHolder, "/static/*");
		
		this.registerServlets(context);
		
		this.webServer.setHandler(context);
		try {
			this.webServer.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registerServlets(ServletContextHandler context) {
		context.addServlet(TwitchModule.TwitchResponseServlet.class, "/twitch_callback/*");
		context.addServlet(TwitchModule.MixerResponseServlet.class, "/mixer_callback/*");
	}
	
}
